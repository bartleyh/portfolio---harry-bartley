### Accelerator 2018-2019 Personal Exercises

## Exercises undertaken to suppliment accelerator material

• Problems taken from internet sources (as credited in the respective files)

• Particular problem sets may have been attempted in multiple languages for practise

• Where possible test cases have been written

