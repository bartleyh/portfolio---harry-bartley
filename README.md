## Accelerator 2018-2019 Harry Bartley

### All folders contain completed or pending work, differentiating between recommended accelerator work and personal projects and interests

• For the projects and work conducted alongside others, credit has been given in the respective files

• Where possible work done using different languages has been segregated for ease of viewing