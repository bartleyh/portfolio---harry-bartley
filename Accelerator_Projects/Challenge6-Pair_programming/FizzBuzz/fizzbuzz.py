def fizz_buzz(number):
    if number % 15 == 0:
        return 'FizzBuzz'
    elif number % 21 == 0:
        return 'FizzPop'
    elif number % 5 == 0:
        return 'Buzz'
    elif number % 3 == 0:
        return 'Fizz'
    elif number % 7 == 0:
        return 'Pop'
    else:
        return number 
