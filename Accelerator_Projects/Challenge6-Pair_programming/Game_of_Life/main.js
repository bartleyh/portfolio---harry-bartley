function createEmptyBoard(width, height) {
    board = []
    for (let j =0; j < height; j++) {
        row =[]
        for (let i =0; i < width; i++){
            row.push(0)
        }
        board.push(row)
    }
    return board
}

function initialiseBoard(livecells, board) {
    for (let i =0; i < livecells.length; i++) {
        let livecell = livecells[i]
        board[livecell[0]][livecell[1]] = 1
    }
}

function countNeighbour(cell, board){
  count = 0
  if (cell[1] == 0){
    if (cell[0] == 0){
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
    }
    else if (cell[0] == board.length-1) {
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
    }
    else {
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]+1]
    }
  }

  else if (cell[1] == board[0].length-1){
    if (cell[0] == 0){
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]-1]
      count += board[cell[0]][cell[1]-1]
    }
    else if (cell[0] == board.length-1) {
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]-1]
      count += board[cell[0]][cell[1]-1]
    }
    else {
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]-1]
      count += board[cell[0]][cell[1]-1]
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]-1]
    }
  }

  else if (cell[0] == 0){
      count += board[cell[0]+1][cell[1]]
      count += board[cell[0]+1][cell[1]-1]
      count += board[cell[0]+1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
      count += board[cell[0]][cell[1]-1]
  }

  else if (cell[0] == board.length-1){
      count += board[cell[0]-1][cell[1]]
      count += board[cell[0]-1][cell[1]-1]
      count += board[cell[0]-1][cell[1]+1]
      count += board[cell[0]][cell[1]+1]
      count += board[cell[0]][cell[1]-1]
  }

  else {
    count += board[cell[0]-1][cell[1]]
    count += board[cell[0]-1][cell[1]-1]
    count += board[cell[0]-1][cell[1]+1]
    count += board[cell[0]][cell[1]+1]
    count += board[cell[0]][cell[1]-1]
    count += board[cell[0]+1][cell[1]]
    count += board[cell[0]+1][cell[1]-1]
    count += board[cell[0]+1][cell[1]+1]
  }
  return count
}

function neighbourCountBoard(board){
  countBoard = []
  for (let i=0; i < board.length; i++){
    countRow = []
    for (let j=0; j <board[0].length; j++){
      countRow.push(countNeighbour([i,j], board))
    }
    countBoard.push(countRow)
  }
  return countBoard
}

function isLivingInASecond(cell, countBoard, board){
  let alive
  if (board[cell[0]][cell[1]] == 1) {
    if (0 <= countBoard[cell[0]][cell[1]] &&  countBoard[cell[0]][cell[1]] < 2){
      alive = 0
    }
    else if (4 <= countBoard[cell[0]][cell[1]]){
      alive = 0
    }
    else if (countBoard[cell[0]][cell[1]] == 2 || countBoard[cell[0]][cell[1]] == 3) {
      alive = 1
    }
  }
  else if (board[cell[0]][cell[1]] == 0) {
    if (0 <= countBoard[cell[0]][cell[1]] && countBoard[cell[0]][cell[1]] <= 2){
      alive = 0
    }
    else if (4 <= countBoard[cell[0]][cell[1]] && countBoard[cell[0]][cell[1]] <= 8){
      alive = 0
    }
    else if (countBoard[cell[0]][cell[1]]==3){

      alive = 1
    }
  }
  return alive
}

function refreshBoard(board) {
  countBoard = neighbourCountBoard(board)
  for (let i=0; i < board.length; i++){
    newRow = []
    for (let j=0; j <board[0].length; j++){
      board[i][j] = isLivingInASecond([i,j], countBoard, board)
    }
  }
}

function refreshPrintBoard(board) {
  console.clear()
  console.log(board)
  console.log('\n')
  refreshBoard(board)
}


function playGame(){
  // const canvas = document.getElementById('gameoflife');
  // const ctx = canvas.getContext('2d');
  // ctx.scale(20, 20)
  // ctx.fillStyle = 'grey';
  // ctx.fillRect(0, 0, canvas.width, canvas.height)
  const width = 7;
  const height = 7;
  liveCellsData = [[1,1],[1,2],[1,3]]
  let game = createEmptyBoard(width, height)
  initialiseBoard(liveCellsData, game)
  printhtml(game)
  setInterval(printhtml, 400, game)
}

// function printhtml(board) {
//   out = ''
//   for (let i=0; i < board.length; i++){
//     for (let j=0; j <board[0].length; j++){
//       if (board[i][j] == 0) {
//         out += "<div style='margin-right: 15px; display: inline-block; height: 10px; width: 10px; background-color: white'></div>"
//       }
//       else if (board[i][j] == 1) {
//         out += "<div style='margin-right: 15px; display: inline-block; height: 10px; width: 10px; background-color: black'></div>"
//       }
//     }
//     out += "<br></br>"
//   }
// console.log(out)

refreshBoard(board)
render(out, document.getElementById('gol'))

let render = function (template, node) {
  node.innerHTML = template;
}


playGame()