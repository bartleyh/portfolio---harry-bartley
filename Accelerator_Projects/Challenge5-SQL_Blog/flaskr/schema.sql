DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS post;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE post (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author_id INTEGER NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  title TEXT NOT NULL,
  body TEXT NOT NULL,
  FOREIGN KEY (author_id) REFERENCES user (id)
);

CREATE TABLE science (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  author TEXT NOT NULL,
  title TEXT NOT NULL,
  abstract TEXT NOT NULL,
  link TEXT NOT NULL,
  category TEXT NOT NULL,
  likes INTEGER
)

CREATE TABLE comments (
 user_id TEXT,
 article_id TEXT,
 content TEXT,
 FOREIGN KEY(user_id) REFERENCES user(id),
 FOREIGN KEY(article_id) REFERENCES science(id)
 )

CREATE TABLE likes (
 user_id TEXT,
 article_id TEXT,
 FOREIGN KEY(user_id) REFERENCES user(id),
 FOREIGN KEY(article_id) REFERENCES science(id)
 )
