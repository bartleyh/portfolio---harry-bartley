const canvas = document.getElementById("myCanvas");
let ctx = canvas.getContext("2d");

let img1 = new Image();
let img2 = new Image();

img1.src = "image/ship.jpeg";
img2.src = "image/invader.jpg";

let BulletFiredPos = null;
let lives = 30;
let invaderAlive = true;
let shooting = false;
let Y = 0;
let Up = 0;
let dx = 0;
let key = 0;
let score = 0;
let playgame = false;
let shipWidth = range(-18, 18, 1);

let possiblePositions = range(20, 230, 10);

let shipPosInitial = {
  X: 150,
  Y: 120
};

let invaderPosInitial = {
  X: possiblePositions[Math.round(Math.random() * 20)],
  Y: 0
};

let invaderPosXRange = {
  a: invaderPosInitial.X,
  b: invaderPosInitial.X + 25
};

let collision;

let dy = 0;
let invaderPos = 0;
let bulletY = shipPosInitial.Y - 5;

function range(start, end, step) {
  let array = [];
  for (let k = start; k < end + 1; k += step) {
    array.push(k);
  }
  return array;
}

invaderCollision = function(invaderAlive, ship) {
  if (invaderAlive) {
    if (invaderPos > 110) {
      switch (invaderPosInitial.X) {
        default:
          collision = false;
          break;
        case ship[0]:
          collision = true;
          break;
        case ship[4]:
          collision = true;
          break;
        case ship[8]:
          collision = true;
          break;
        case ship[12]:
          collision = true;
          break;
        case ship[16]:
          collision = true;
          break;
        case ship[20]:
          collision = true;
          break;
        case ship[2]:
          collision = true;
          break;
        case ship[6]:
          collision = true;
          break;
        case ship[10]:
          collision = true;
          break;
        case ship[14]:
          collision = true;
          break;
        case ship[18]:
          collision = true;
          break;
        case ship[22]:
          collision = true;
          break;
      }
      if (collision == true) {
        return true;
      } else if (invaderPos > 140) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  } else {
    return false;
  }
};

function bulletCollision(bullet, invader) {
  switch (bullet) {
    default:
      collision = false;
      break;
    case invader[0]:
      collision = true;
      break;
    case invader[1]:
      collision = true;
      break;
    case invader[2]:
      collision = true;
      break;
    case invader[3]:
      collision = true;
      break;
    case invader[4]:
      collision = true;
      break;
    case invader[5]:
      collision = true;
      break;
    case invader[6]:
      collision = true;
      break;
    case invader[7]:
      collision = true;
      break;
    case invader[8]:
      collision = true;
      break;
    case invader[9]:
      collision = true;
      break;
  }
  if (collision == true) {
    if (
      BulletFiredPos == invaderPosInitial.X ||
      BulletFiredPos == invaderPosInitial.X - 2 ||
      BulletFiredPos == invaderPosInitial.X - 4 ||
      BulletFiredPos == invaderPosInitial.X - 8 ||
      BulletFiredPos == invaderPosInitial.X - 10 ||
      BulletFiredPos == invaderPosInitial.X - 12 ||
      BulletFiredPos == invaderPosInitial.X - 14 ||
      BulletFiredPos == invaderPosInitial.X - 16 ||
      BulletFiredPos == invaderPosInitial.X + 2 ||
      BulletFiredPos == invaderPosInitial.X + 4 ||
      BulletFiredPos == invaderPosInitial.X + 8 ||
      BulletFiredPos == invaderPosInitial.X + 10 ||
      BulletFiredPos == invaderPosInitial.X + 12 ||
      BulletFiredPos == invaderPosInitial.X + 14 ||
      BulletFiredPos == invaderPosInitial.X + 16 ||
      BulletFiredPos == invaderPosInitial.X + 18 ||
      BulletFiredPos == invaderPosInitial.X - 1 ||
      BulletFiredPos == invaderPosInitial.X - 3 ||
      BulletFiredPos == invaderPosInitial.X - 5 ||
      BulletFiredPos == invaderPosInitial.X - 7 ||
      BulletFiredPos == invaderPosInitial.X - 9 ||
      BulletFiredPos == invaderPosInitial.X - 11 ||
      BulletFiredPos == invaderPosInitial.X - 13 ||
      BulletFiredPos == invaderPosInitial.X + 1 ||
      BulletFiredPos == invaderPosInitial.X + 3 ||
      BulletFiredPos == invaderPosInitial.X + 5 ||
      BulletFiredPos == invaderPosInitial.X + 7 ||
      BulletFiredPos == invaderPosInitial.X + 9 ||
      BulletFiredPos == invaderPosInitial.X + 11 ||
      BulletFiredPos == invaderPosInitial.X + 13 ||
      BulletFiredPos == invaderPosInitial.X + 15
    ) {
      shooting = false;
      invaderAlive = false;
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function invaderUpdate() {
  if (invaderAlive == false) {
    invaderAlive = true;
    return (invaderPosInitial.X =
      possiblePositions[Math.round(Math.random() * 27)]);
  }
  return invaderAlive;
}

function invaderReset() {
  if (invaderAlive == false) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    invaderPos = 0;
    return invaderPos;
  } else return;
}

function invaderMove(Collision, invaderAlive) {
  if (Collision == false && invaderAlive == true) {
    invaderPos += 1;
    return invaderPos;
  } else if (Collision == true) {
    invaderAlive = false;
  } else {
    return (invaderAlive = false);
  }
}

function moveBullet(collide) {
  if (collide == false) {
    Y = 2;
    return Y;
  } else {
    return (Y = 0);
  }
}

function PositionBullet(Up) {
  if (Up != 0) {
    if (Up + bulletY <= 0) {
      shooting = false;
    } else {
      ctx.fillStyle = "red";
      ctx.fillRect(BulletFiredPos + 10, bulletY + Up, 5, 5);
      shooting = true;
    }
  } else shooting = false;
}

function moveShip(key) {
  if (key == 37 && shipPosInitial.X + dx > 0) {
    dx -= 2;
    return dx;
  } else if (key == 39 && shipPosInitial.X + dx < 270) {
    dx += 2;
    return dx;
  } else {
    return dx;
  }
}

function update() {
  document.getElementById("score").innerHTML = "Total Score: " + score;
  document.getElementById("lives").innerHTML = "Lives: " + lives;

  let shipPosXRange = {
    a: shipPosInitial.X + dx,
    b: shipPosInitial.X + 30 + dx
  };

  let invaderPosY = invaderPos;
  invader = range(invaderPosY, invaderPosY + 5, 0.5);

  moveShip(key);
  ship = range(shipPosXRange.a, shipPosXRange.b, 1);
  collide = invaderCollision(invaderAlive, ship);

  if (collide == true) {
    invaderAlive = false;
    invaderReset();
    lives -= 1;
  }

  if (lives > 0) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (invaderAlive === true) {
      ctx.drawImage(img2, invaderPosInitial.X, invaderPos, 25, 13);
      invaderPos = invaderMove(collide, invaderAlive);
    }
    if (shooting == true) {
      Up -= moveBullet(collide);
      PositionBullet(Up);
      hitcol = bulletCollision(Up + bulletY - 0.5, invader);
      if (hitcol == true) {
        score += 10;
        invaderReset();
      }
    } else {
      Up = 0;
    }
    ctx.drawImage(img1, shipPosInitial.X + dx, shipPosInitial.Y, 30, 15);
  } else gameOver();
}

setInterval(function() {
  if (playgame == true) {
    update();
  } else {
    document.getElementById("score").innerHTML = "Total Score: " + score;
    document.getElementById("lives").innerHTML = "Lives: " + lives;
    return;
  }
}, 20);

setInterval(function() {
  invaderUpdate();
}, 4000);

function gameOver() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  playgame = false;
  let text = document.getElementById("GameOver");
  let button = document.getElementById("reset");
  text.style.display = "block";
  button.style.display = "block";
}

function startGame() {
  playgame = true;
  lives = 30;
  score = 0;
  dx = 0;
  key = 0;
  let Intro = document.getElementById("Intro");
  let Playbutton = document.getElementById("Play");
  let text = document.getElementById("GameOver");
  let button = document.getElementById("reset");
  text.style.display = "none";
  button.style.display = "none";
  Intro.style.display = "none";
  Playbutton.style.display = "none";
}

document.getElementById("fire").onclick = function() {
  shooting = true;
  if (bulletY + Up == shipPosInitial.Y - 5) {
    BulletFiredPos = shipPosInitial.X + dx;
  }
};

window.addEventListener("touchstart", event => {
  if (event.touches[0].screenX > 240 && event.touches[0].screenX < 400) {
    if (event.touches[0].screenY > 415 && event.touches[0].screenY < 465) {
      startGame();
    }
  } else if (event.touches[0].screenX > 390 && event.touches[0].screenX < 445) {
    if (event.touches[0].screenY > 615 && event.touches[0].screenY < 655) {
      key = 37;
      moveShip(key);
    }
  } else if (event.touches[0].screenX > 465 && event.touches[0].screenX < 530) {
    if (event.touches[0].screenY > 615 && event.touches[0].screenY < 655) {
      key = 39;
      moveShip(key);
    }
  } else if (event.touches[0].screenX > 320 && event.touches[0].screenX < 345) {
    if (event.touches[0].screenY > 515 && event.touches[0].screenY < 540) {
      shooting = true;
      if (bulletY + Up == shipPosInitial.Y - 5) {
        BulletFiredPos = shipPosInitial.X + dx;
      }
    }
  }
});

document.addEventListener("keydown", event => {
  if (event.keyCode === 32) {
    shooting = true;
    if (bulletY + Up == shipPosInitial.Y - 5) {
      BulletFiredPos = shipPosInitial.X + dx;
    }
  } else if (event.keyCode == 37) {
    key = 37;
    moveShip(key);
  } else if (event.keyCode == 39) {
    key = 39;
    moveShip(key);
  }
});

document.addEventListener("keyup", function() {
  key = 0;
});
