## Space Invaders (With Less Features)

### A Javascript Game - (Accelerator 18-19)

#### Project Start

• Decided to use pure javascript and html5 canvas with no framework, the game will be initially run locally using Visual Studio Code Live server

• Testing would be done using jasmine framework downloaded from nodejs using the visual html package to allow errord and test code to be seen and compared easier

• The idea is to create a game that will work using, keyboard or mouse clicks or have touch screen capability

• The @media css extension will be used to deploy the app in order for the canvas to be sized for both a widescreen and small screen

• The game will run on chrome browser with both android and ios capabilities

#### Design

• Layout is a single js file and css stylsheet linked to a html file deploying a canvas

• Script was initially written a function at a time within the jasmine/index.html file in order to be fully tested - any canvas notation has had to be commented out in order for functions to run properly

• The occasional function had to be changed in order to be tested as the result would be to test a return value rather than a change in global variable

• SetInterval() has been used twice in order to continuously update the page as well as ensure the invader is continuously coming down - more of a natural feel

#### Testing

• A combination of different expect() functions were used to test

• Where possible a function should return a single value and only attempt one operation

• Testing has ensured a large majority of the code should not break and will return an operation under a wide range of conditions

#### Deployment

• As previously mentioned the main deployment will be using live server from visual studio fixing the canvas into chrome browser

• In browser the game can be operated using the keyboard or the buttons on the interface

• Using the developer tools when switched to mobile mode the game can still be tested with "touchscreen events" however for a more accurate mobile app simulator, cordova was used

##### Cordova

• [Apache Cordova](https://cordova.apache.org/) runs mobile phone emulators to run apps written in HTML, CSS & JS - an open source project

• It can used to determine how the app will run, look and whether it will run on all platforms

###### The requirements to create a cordova project for an android platform are:

• [Gradle](https://gradle.org/) build system (takes best build features from other systems and combines them)

• [Java JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java development kit

• [Android SDK](https://developer.android.com/studio/) - Android studio download for AVD development tools

• This project is only set up for android currently and will run off a samsung galaxy nexus

• Cordova was downloaded through node and runs via command line or Android Visual Studio

#### Errors and Improvements

• The @media function doesnt work as accuratley as it should and so needs to be tweeked to properly run on all devices

• The code needs a bit of refactoring as there is repeated sections and the testing could cover a larger percentage

• Rather than draw using canvas it would have been better to simply construct all the visual aspects using javascript and keep the HTML to a bare minimum to avoid positioning errors

• Emulate using both an android and a ios platform - even windows (slightly different to android phones)
