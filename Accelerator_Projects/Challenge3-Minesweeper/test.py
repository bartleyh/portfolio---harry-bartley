import ast
from operator import itemgetter


minesweeper = open('map_array.txt', 'r')

grid = []
for row in minesweeper:
    grid.append(ast.literal_eval(row))

numbered_squares = []
for row_index in range(len(grid)):
    for column_index in range(len(grid)):
        if grid[row_index][column_index] != '0' and grid[row_index][column_index] != ' ':
            print(row_index, column_index)
            numbered_squares.append([row_index, column_index, grid[row_index][column_index]])


def surrounding_squares(numbered_squares, grid):
    empty_space = []
    for array in numbered_squares:
        counter = 0
        row_index = array[0]
        column_index = array[1]
        print(row_index,column_index, 'indexes')

        for i in range(row_index-1, row_index+2):
            for j in range(column_index-1, column_index+2):
                if j >= 0 and i >= 0 and j < 9 and i < 9:
                    if grid[i][j] == ' ':
                        empty_space.append([i,j])f
                        counter += 1
        array.append(counter)
    return empty_space


def compute_empty_space_probability(numbered_squares, empty_space_numbered):
    for array in numbered_squares:
        for list in empty_space_numbered:

            numerator = int(array[2])
            denominator = list[1]
            probability = numerator / denominator
            list.append(probability)


def count_references_empty(empty_space):
    empty_space_numbered = []
    for array in empty_space:
        count = empty_space.count(array)
        empty_space_numbered.append([array, count])
    return empty_space_numbered


def minimize_probability(empty_space_numbered):
    empty_space_numbered.sort(key=itemgetter(1))
    a = min(x[1] for x in empty_space_numbered)
    final = []
    for array in empty_space_numbered:
        if array[1] == a:
            final.append(array)
    return final



print(numbered_squares)
empties = surrounding_squares(numbered_squares,grid)
print(numbered_squares, 'empties')
numbered_empties = count_references_empty(empties)
compute_empty_space_probability(numbered_squares, numbered_empties)
print(numbered_empties)
final = minimize_probability(numbered_empties)

print(numbered_empties, 'numbered empties')
print(numbered_squares, 'numbered squares')
dictionary = {0:'a', 1:'b', 2:'c', 3:'d', 4:'e', 5:'f', 6:'g', 7:'h', 8:'i'}
    #for array in final:
    #print(dictionary[array[0][1]],array[0][0]+1, 'these are empties! PICK any of these!!')
