Number	 Title-The_title_of_the_film	  Rating-User_rating_for_the_movie_0-10	 Votes-Number_of_votes	 Revenue-(Millions)-Movie_revenue_in_millions	 Metascore-An_aggregated_average_of_critic_scores._Values_are_between_0_and_100._Higher_scores_represent_positive_reviews.

1	Guardians of the Galaxy	8.1	757074	333.13	76
2	Prometheus	7	485820	126.46	65
3	Split	7.3	157606	138.12	62
4	Sing	7.2	60545	270.32	59
5	Suicide Squad	6.2	393727	325.02	40
6	The Great Wall	6.1	56036	45.13	42
7	La La Land	8.3	258682	151.06	93
8	Mindhorn	6.4	2490		71
9	The Lost City of Z	7.1	7188	8.01	78
10	Passengers	7	192177	100.01	41
11	Fantastic Beasts and Where to Find Them	7.5	232072	234.02	66
12	Hidden Figures	7.8	93103	169.27	74
13	Rogue One	7.9	323118	532.17	65
14	Moana	7.7	118151	248.75	81
15	Colossal	6.4	8612	2.87	70
16	The Secret Life of Pets	6.6	120259	368.31	61
17	Hacksaw Ridge	8.2	211760	67.12	71
18	Jason Bourne	6.7	150823	162.16	58
19	Lion	8.1	102061	51.69	69
20	Arrival	8	340798	100.5	81
21	Gold	6.7	19053	7.22	49
22	Manchester by the Sea	7.9	134213	47.7	96
23	Hounds of Love	6.7	1115		72
24	Trolls	6.5	38552	153.69	56
25	Independence Day: Resurgence	5.3	127553	103.14	32
26	Paris pieds nus	6.8	222		
27	Bahubali: The Beginning	8.3	76193	6.5	
28	Dead Awake	4.7	523	0.01	
29	Bad Moms	6.2	66540	113.08	60
30	Assassin's Creed	5.9	112813	54.65	36
31	Why Him?	6.3	48123	60.31	39
32	Nocturnal Animals	7.5	126030	10.64	67
33	X-Men: Apocalypse	7.1	275510	155.33	52
34	Deadpool	8	627797	363.02	65
35	Resident Evil: The Final Chapter	5.6	46165	26.84	49
36	Captain America: Civil War	7.9	411656	408.08	75
37	Interstellar	8.6	1047747	187.99	74
38	Doctor Strange	7.6	293732	232.6	72
39	The Magnificent Seven	6.9	122853	93.38	54
40	5- 25- 77	7.1	241		
41	Sausage Party	6.3	120690	97.66	66
42	Moonlight	7.5	135095	27.85	99
43	Don't Mess in the Woods	2.7	496		
44	The Founder	7.2	37033	12.79	66
45	Lowriders	6.3	279	4.21	57
46	Pirates of the Caribbean: On Stranger Tides	6.7	395025	241.06	45
47	Miss Sloane	7.3	17818	3.44	64
48	Fallen	5.6	5103		
49	Star Trek Beyond	7.1	164567	158.8	68
50	The Last Face	3.7	987		16
51	Star Wars: Episode VII - The Force Awakens	8.1	661608	936.63	81
52	Underworld: Blood Wars	5.8	41362	30.35	23
53	Mother's Day	5.6	20221	32.46	18
54	John Wick	7.2	321933	43	68
55	The Dark Knight	9	1791916	533.32	82
56	Silence	7.3	49190	7.08	79
57	Don't Breathe	7.2	121103	89.21	71
58	Me Before You	7.4	113322	56.23	51
59	Their Finest	7	3739	3.18	76
60	Sully	7.5	137608	125.07	74
61	Batman v Superman: Dawn of Justice	6.7	472307	330.25	44
62	The Autopsy of Jane Doe	6.8	35870		65
63	The Girl on the Train	6.5	102177	75.31	48
64	Fifty Shades of Grey	4.1	244474	166.15	46
65	The Prestige	8.5	913152	53.08	66
66	Kingsman: The Secret Service	7.7	440209	128.25	58
67	Patriots Day	7.4	39784	31.86	69
68	Mad Max: Fury Road	8.1	632842	153.63	90
69	Wakefield	7.5	291	0.01	61
70	Deepwater Horizon	7.2	89849	61.28	68
71	The Promise	5.9	149791		49
72	Allied	7.1	78079	40.07	60
73	A Monster Calls	7.5	39134	3.73	76
74	Collateral Beauty	6.8	43977	30.98	23
75	Zootopia	8.1	296853	341.26	78
76	Pirates of the Caribbean: At World's End	7.1	498821	309.4	50
77	The Avengers	8.1	1045588	623.28	69
78	Inglourious Basterds	8.3	959065	120.52	69
79	Pirates of the Caribbean: Dead Man's Chest	7.3	552027	423.03	53
80	Ghostbusters	5.3	147717	128.34	60
81	Inception	8.8	1583625	292.57	74
82	Captain Fantastic	7.9	105081	5.88	72
83	The Wolf of Wall Street	8.2	865134	116.87	75
84	Gone Girl	8.1	636243	167.74	79
85	Furious Seven	7.2	301249	350.03	67
86	Jurassic World	7	455169	652.18	59
87	Live by Night	6.4	27869	10.38	49
88	Avatar	7.8	935408	760.51	83
89	The Hateful Eight	7.8	341170	54.12	68
90	The Accountant	7.4	162122	86.2	51
91	Prisoners	8.1	431185	60.96	74
92	Warcraft	7	187547	47.17	32
93	The Help	8.1	342429	169.71	62
94	War Dogs	7.1	106463	43.02	57
95	Avengers: Age of Ultron	7.4	516895	458.99	66
96	The Nice Guys	7.4	175067	36.25	70
97	Kimi no na wa	8.6	34110	4.68	79
98	The Void	5.8	9247	0.15	62
99	Personal Shopper	6.3	10181	1.29	77
100	The Departed	8.5	937414	132.37	85
101	Legend	7	108836	1.87	55
102	Thor	7	570814	181.02	57
103	The Martian	8	556097	228.43	80
104	Contratiempo	7.9	7204		
105	The Man from U.N.C.L.E.	7.3	202973	45.43	56
106	Hell or High Water	7.7	115546	26.86	88
107	The Comedian	5.4	1954	1.66	40
108	The Legend of Tarzan	6.3	117590	126.59	44
109	All We Had	5.8	1004		48
110	Ex Machina	7.7	339797	25.44	78
111	The Belko Experiment	6.3	3712	10.16	44
112	12 Years a Slave	8.1	486338	56.67	96
113	The Bad Batch	6.1	512		65
114	300	7.7	637104	210.59	52
115	Harry Potter and the Deathly Hallows: Part 2	8.1	590595	380.96	87
116	Office Christmas Party	5.8	30761	54.73	42
117	The Neon Demon	6.2	50359	1.33	51
118	Dangal	8.8	48969	11.15	
119	10 Cloverfield Lane	7.2	192968	71.9	76
120	Finding Dory	7.4	157026	486.29	77
121	Miss Peregrine's Home for Peculiar Children	6.7	101058	87.24	57
122	Divergent	6.7	362093	150.83	48
123	Mike and Dave Need Wedding Dates	6	53183	46.01	51
124	Boyka: Undisputed IV	7.4	10428		
125	The Dark Knight Rises	8.5	1222645	448.13	78
126	The Jungle Book	7.5	198243	364	77
127	Transformers: Age of Extinction	5.7	255483	245.43	32
128	Nerve	6.6	69651	38.56	58
129	Mamma Mia!	6.4	153481	143.7	51
130	The Revenant	8	499424	183.64	76
131	Fences	7.3	50953	57.64	79
132	Into the Woods	6	109756	128	69
133	The Shallows	6.4	78328	55.12	59
134	Whiplash	8.5	477276	13.09	88
135	Furious 6	7.1	318051	238.67	61
136	The Place Beyond the Pines	7.3	200090	21.38	68
137	No Country for Old Men	8.1	660286	74.27	91
138	The Great Gatsby	7.3	386102	144.81	55
139	Shutter Island	8.1	855604	127.97	63
140	Brimstone	7.1	13004		44
141	Star Trek	8	526324	257.7	82
142	Diary of a Wimpy Kid	6.2	34184	64	56
143	The Big Short	7.8	246360	70.24	81
144	Room	8.2	224132	14.68	86
145	Django Unchained	8.4	1039115	162.8	81
146	Ah-ga-ssi	8.1	33418	2.01	84
147	The Edge of Seventeen	7.4	47694	14.26	77
148	Watchmen	7.6	410249	107.5	56
149	Superbad	7.6	442082	121.46	76
150	Inferno	6.2	97623	34.26	42
151	The BFG	6.4	50853	55.47	66
152	The Hunger Games	7.2	735604	408	68
153	White Girl	5.8	4299	0.2	65
154	Sicario	7.6	243230	46.88	82
155	Twin Peaks: The Missing Pieces	8.1	1973		
156	Aliens vs Predator - Requiem	4.7	97618	41.8	29
157	Pacific Rim	7	400519	101.79	64
158	Crazy, Stupid, Love.	7.4	396714	84.24	68
159	Scott Pilgrim vs. the World	7.5	291457	31.49	69
160	Hot Fuzz	7.9	373244	23.62	81
161	Mine	6	5926		40
162	Free Fire	7	6946	1.8	63
163	X-Men: Days of Future Past	8	552298	233.91	74
164	Jack Reacher: Never Go Back	6.1	78043	58.4	47
165	Casino Royale	8	495106	167.01	80
166	Twilight	5.2	361449	191.45	56
167	Now You See Me 2	6.5	156567	65.03	46
168	Woman in Gold	7.3	39723	33.31	51
169	13 Hours	7.3	76935	52.82	48
170	Spectre	6.8	308981	200.07	60
171	Nightcrawler	7.9	332476	32.28	76
172	Kubo and the Two Strings	7.9	72778	48.02	84
173	Beyond the Gates	5.2	2127		59
174	Her	8	390531	25.56	90
175	Frozen	7.5	451894	400.74	74
176	Tomorrowland	6.5	143069	93.42	60
177	Dawn of the Planet of the Apes	7.6	337777	208.54	79
178	Tropic Thunder	7	321442	110.42	71
179	The Conjuring 2	7.4	137203	102.46	65
180	Ant-Man	7.3	368912	180.19	64
181	Bridget Jones's Baby	6.7	43086	24.09	59
182	The VVitch: A New-England Folktale	6.8	101781	25.14	83
183	Cinderella	7	117018	201.15	67
184	Realive	5.9	1176		
185	Forushande	8	22389	3.4	85
186	Love	6	24003		51
187	Billy Lynn's Long Halftime Walk	6.3	11944	1.72	53
188	Crimson Peak	6.6	97454	31.06	66
189	Drive	7.8	461509	35.05	78
190	Trainwreck	6.3	106364	110.01	75
191	The Light Between Oceans	7.2	27382	12.53	60
192	Below Her Mouth	5.6	1445		42
193	Spotlight	8.1	268282	44.99	93
194	Morgan	5.8	22107	3.91	48
195	Warrior	8.2	355722	13.65	71
196	Captain America: The First Avenger	6.9	547368	176.64	66
197	Hacker	6.3	3799		
198	Into the Wild	8.1	459304	18.35	73
199	The Imitation Game	8.1	532353	91.12	73
200	Central Intelligence	6.3	97082	127.38	52
201	Edge of Tomorrow	7.9	471815	100.19	71
202	A Cure for Wellness	6.5	12193	8.1	47
203	Snowden	7.3	79855	21.48	58
204	Iron Man	7.9	737719	318.3	79
205	Allegiant	5.7	70504	66	33
206	X: First Class	7.8	550011	146.41	65
207	Raw (II)	7.5	5435	0.51	81
208	Paterson	7.5	26089	2.14	90
209	Bridesmaids	6.8	227912	169.08	75
210	The Girl with All the Gifts	6.7	23713		67
211	San Andreas	6.1	161396	155.18	43
212	Spring Breakers	5.3	114290	14.12	63
213	Transformers	7.1	531112	318.76	61
214	Old Boy	5.8	54679		49
215	Thor: The Dark World	7	443584	206.36	54
216	Gods of Egypt	5.5	73568	31.14	25
217	Captain America: The Winter Soldier	7.8	542362	259.75	70
218	Monster Trucks	5.7	7044	33.04	41
219	A Dark Song	6.1	1703		67
220	Kick-Ass	7.7	456749	48.04	66
221	Hardcore Henry	6.7	61098	9.24	51
222	Cars	7.1	283445	244.05	73
223	It Follows	6.9	136399	14.67	83
224	The Girl with the Dragon Tattoo	7.8	348551	102.52	71
225	We're the Millers	7	334867	150.37	44
226	American Honey	7	19660	0.66	79
227	The Lobster	7.1	121313	8.7	82
228	Predators	6.4	179450	52	51
229	Maleficent	7	268877	241.41	56
230	Rupture	4.8	2382		35
231	Pan's Labyrinth	8.2	498879	37.62	98
232	A Kind of Murder	5.2	3305	0	50
233	Apocalypto	7.8	247926	50.86	68
234	Mission: Impossible - Rogue Nation	7.4	257472	195	75
235	The Huntsman: Winter's War	6.1	66766	47.95	35
236	The Perks of Being a Wallflower	8	377336	17.74	67
237	Jackie	6.8	41446	13.96	81
238	The Disappointments Room	3.9	4895	2.41	31
239	The Grand Budapest Hotel	8.1	530881	59.07	88
240	The Host	5.9	96852	26.62	35
241	Fury	7.6	332234	85.71	64
242	Inside Out	8.2	416689	356.45	94
243	Rock Dog	5.8	1109	9.4	48
244	Terminator Genisys	6.5	205365	89.73	38
245	Percy Jackson & the Olympians: The Lightning Thief	5.9	148949	88.76	47
246	Les Misérables	7.6	257426	148.78	63
247	Children of Men	7.9	382910	35.29	84
248	20th Century Women	7.4	14708	5.66	83
249	Spy	7.1	188017	110.82	75
250	The Intouchables	8.6	557965	13.18	57
251	Bonjour Anne	4.9	178	0.32	50
252	Kynodontas	7.3	50946	0.11	73
253	Straight Outta Compton	7.9	139831	161.03	72
254	The Amazing Spider-Man 2	6.7	342183	202.85	53
255	The Conjuring	7.5	330305	137.39	68
256	The Hangover	7.8	611563	277.31	73
257	Battleship	5.8	210349	65.17	41
258	Rise of the Planet of the Apes	7.6	422290	176.74	68
259	Lights Out	6.4	69823	67.24	58
260	Norman: The Moderate Rise and Tragic Fall of a New York Fixer	7.1	664	2.27	76
261	Birdman or (The Unexpected Virtue of Ignorance)	7.8	440299	42.34	88
262	Black Swan	8	581518	106.95	79
263	Dear White People	6.2	21715	4.4	79
264	Nymphomaniac: Vol. I	7	90556	0.79	64
265	Teenage Mutant Ninja Turtles: Out of the Shadows	6	59312	0.54	40
266	Knock Knock	4.9	53441	0.03	53
267	Dirty Grandpa	6	75137	35.54	18
268	Cloud Atlas	7.5	298651	27.1	55
269	X-Men Origins: Wolverine	6.7	388447	179.88	40
270	Satanic	3.7	2384		
271	Skyfall	7.8	547386	304.36	81
272	The Hobbit: An Unexpected Journey	7.9	668651	303	58
273	21 Jump Street	7.2	432046	138.45	69
274	Sing Street	8	52144	3.23	79
275	Ballerina	6.8	4729		
276	Oblivion	7	410125	89.02	54
277	22 Jump Street	7.1	280110	191.62	71
278	Zodiac	7.7	329683	33.05	78
279	Everybody Wants Some!!	7	36312	3.37	83
280	Iron Man Three	7.2	591023	408.99	62
281	Now You See Me	7.3	492324	117.7	50
282	Sherlock Holmes	7.6	501769	209.02	57
283	Death Proof	7.1	220236		
284	The Danish Girl	7	110773	12.71	66
285	Hercules	6	122838	72.66	47
286	Sucker Punch	6.1	204874	36.38	33
287	Keeping Up with the Joneses	5.8	30405	14.9	34
288	Jupiter Ascending	5.3	150121	47.38	40
289	Masterminds	5.8	26508	17.36	47
290	Iris	6.1	726		
291	Busanhaeng	7.5	58782	2.13	72
292	Pitch Perfect	7.2	226631	65	66
293	Neighbors 2: Sorority Rising	5.7	76327	55.29	58
294	The Exception	7.7	96		
295	Man of Steel	7.1	577010	291.02	55
296	The Choice	6.6	20514	18.71	26
297	Ice Age: Collision Course	5.7	34523	64.06	34
298	The Devil Wears Prada	6.8	302268	124.73	62
299	The Infiltrator	7.1	43929	15.43	66
300	There Will Be Blood	8.1	400682	40.22	92
301	The Equalizer	7.2	249425	101.53	57
302	Lone Survivor	7.5	218996	125.07	60
303	The Cabin in the Woods	7	295554	42.04	72
304	The House Bunny	5.5	67033	48.24	55
305	She's Out of My League	6.4	105619	31.58	46
306	Inherent Vice	6.7	69509	8.09	81
307	Alice Through the Looking Glass	6.2	57207	77.04	34
308	Vincent N Roxxy	5.5	403		
309	The Fast and the Furious: Tokyo Drift	6	193479	62.49	45
310	How to Be Single	6.1	59886	46.81	51
311	The Blind Side	7.7	237221	255.95	53
312	La vie d'Adèle	7.8	103150	2.2	88
313	The Babadook	6.8	132580	0.92	86
314	The Hobbit: The Battle of the Five Armies	7.4	385598	255.11	59
315	Harry Potter and the Order of the Phoenix	7.5	385325	292	71
316	Snowpiercer	7	199048	4.56	84
317	The 5th Wave	5.2	73093	34.91	33
318	The Stakelander	5.3	1263		
319	The Visit	6.2	81429	65.07	55
320	Fast Five	7.3	300803	209.81	66
321	Step Up	6.5	95960	65.27	48
322	Lovesong	6.4	616	0.01	74
323	RocknRolla	7.3	203096	5.69	53
324	In Time	6.7	319025	37.55	53
325	The Social Network	7.7	510100	96.92	95
326	The Last Witch Hunter	6	71149	27.36	34
327	Victor Frankenstein	6	37975	5.77	36
328	A Street Cat Named Bob	7.4	12643	0.04	54
329	Green Room	7	62885	3.22	79
330	Blackhat	5.4	43085	7.1	51
331	Storks	6.9	34248	72.66	56
332	American Sniper	7.3	353305	350.12	72
333	Dallas Buyers Club	8	352801	27.3	84
334	Lincoln	7.4	207497	182.2	86
335	Rush	8.1	339722	26.9	75
336	Before I Wake	6.1	18201		
337	Silver Linings Playbook	7.8	564364	132.09	81
338	Tracktown	5.9	115		64
339	The Fault in Our Stars	7.8	271301	124.87	69
340	Blended	6.5	93764	46.28	31
341	Fast & Furious	6.6	217464	155.02	46
342	Looper	7.4	452369	66.47	84
343	White House Down	6.4	173320	73.1	52
344	Pete's Dragon	6.8	36322	76.2	71
345	Spider-Man 3	6.2	406219	336.53	59
346	The Three Musketeers	5.8	92329	20.32	35
347	Stardust	7.7	220664	38.35	66
348	American Hustle	7.3	379088	150.12	90
349	Jennifer's Body	5.1	96617	16.2	47
350	Midnight in Paris	7.7	320323	56.82	81
351	Lady Macbeth	7.3	1396		83
352	Joy	6.6	97679	56.44	56
353	The Dressmaker	7.1	33352	2.02	47
354	Café Society	6.7	45579	11.08	64
355	Insurgent	6.3	171970	130	42
356	Seventh Son	5.5	59958	17.18	30
357	Demain tout commence	7.4	5496		
358	The Theory of Everything	7.7	299718	35.89	72
359	This Is the End	6.6	327838	101.47	67
360	About Time	7.8	221600	15.29	55
361	Step Brothers	6.9	223065	100.47	51
362	Clown	5.7	14248	0.05	42
363	Star Trek Into Darkness	7.8	417663	228.76	72
364	Zombieland	7.7	409403	75.59	73
365	Hail, Caesar!	6.3	89059	30	72
366	Slumdog Millionaire	8	677044	141.32	86
367	The Twilight Saga: Breaking Dawn - Part 2	5.5	194329	292.3	52
368	American Wrestler: The Wizard	6.9	286		
369	The Amazing Spider-Man	7	474320	262.03	66
370	Ben-Hur	5.7	28326	26.38	38
371	Sleight	6	702	3.85	62
372	The Maze Runner	6.8	335531	102.41	57
373	Criminal	6.3	38430	14.27	36
374	Wanted	6.7	312495	134.57	64
375	Florence Foster Jenkins	6.9	31776	27.37	71
376	Collide	5.7	7583	2.2	33
377	Black Mass	6.9	135706	62.56	68
378	Creed	7.6	175673	109.71	82
379	Swiss Army Man	7.1	61812	4.21	64
380	The Expendables 3	6.1	137568	39.29	35
381	What We Do in the Shadows	7.6	84016	3.33	76
382	Southpaw	7.4	169083	52.42	57
383	Hush	6.6	45867		67
384	Bridge of Spies	7.6	217938	72.31	81
385	The Lego Movie	7.8	266508	257.76	83
386	Everest	7.1	154647	43.25	64
387	Pixels	5.6	101092	78.75	27
388	Robin Hood	6.7	221117	105.22	53
389	The Wolverine	6.7	355362	132.55	60
390	John Carter	6.6	220667	73.06	51
391	Keanu	6.3	31913	20.57	63
392	The Gunman	5.8	31194	10.64	39
393	Steve Jobs	7.2	116112	17.75	82
394	Whisky Galore	5	102		43
395	Grown Ups 2	5.4	114482	133.67	19
396	The Age of Adaline	7.2	112288	42.48	51
397	The Incredible Hulk	6.8	342355	134.52	61
398	Couples Retreat	5.5	86417	109.18	23
399	Absolutely Anything	6	26587		31
400	Magic Mike	6.1	113686	113.71	72
