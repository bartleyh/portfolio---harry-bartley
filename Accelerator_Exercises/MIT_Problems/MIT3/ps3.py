# 6.0001 Problem Set 3
#
# The 6.0001 Word Game
# Created by: Kevin Luu <luuk> and Jenna Wiens <jwiens>
#
# Name          : <your name>
# Collaborators : <your collaborators>
# Time spent    : <total time>

import math
import random
import string

VOWELS = 'aeiou'
CONSONANTS = 'bcdfghjklmnpqrstvwxyz'
HAND_SIZE = 7

SCRABBLE_LETTER_VALUES = {
    'a': 1, 'b': 3, 'c': 3, 'd': 2, 'e': 1, 'f': 4, 'g': 2, 'h': 4, 'i': 1, 'j': 8, 'k': 5, 'l': 1, 'm': 3, 'n': 1, 'o': 1, 'p': 3, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 1, 'v': 4, 'w': 4, 'x': 8, 'y': 4, 'z': 10
}

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # wordlist: list of strings
    wordlist = []
    for line in inFile:
        wordlist.append(line.strip().lower())
    return wordlist


def get_frequency_dict(sequence):
    """
    Returns a dictionary where the keys are elements of the sequence
    and the values are integer counts, for the number of times that
    an element is repeated in the sequence.

    sequence: string or list
    return: dictionary
    """
    
    # freqs: dictionary (element_type -> int)
    freq = {}
    for x in sequence:
        freq[x] = freq.get(x,0) + 1
    return freq


# (end of helper code)
# -----------------------------------

#
# Problem #1: Scoring a word
#

def get_word_score(word, n):
    """
    Returns the score for a word. Assumes the word is a
    valid word.

    word: string
    n: int >= 0
    returns: int >= 0
    """

    if "*" in word:
        word = word.replace("*", "")

    word =str(word.lower())
    # First component, sum of the points for letters in the word
    letter_values = []
    for chars in word:
        letter_values.append(SCRABBLE_LETTER_VALUES.get(chars))
    component_one = sum(letter_values)

    # Second component, [7 * ​word_le​ngth - 3 * (​n-​word_length)] or 1, whichever value is greater
    word_length = len(word)
    component_two = ((7*word_length)-(3*(n - word_length)))

    if component_two < 1:
        component_two = 1

    return component_one * component_two


def display_hand(hand):
    """
    Displays the letters currently in the hand.

    For example:
       display_hand({'a':1, 'x':2, 'l':3, 'e':1})
    Should print out something like:
       a x x l l l e
    The order of the letters is unimportant.

    hand: dictionary (string -> int)
    """
    for letter in hand.keys():
       for j in range(hand[letter]):
           print(letter,end=" ")
    print()

            
#
# Make sure you understand how this function works and what it does!
# You will need to modify this for Problem #4.
#


def deal_hand(n):
    """
    Returns a random hand containing n lowercase letters.
    ceil(n/3) letters in the hand should be VOWELS (note,
    ceil(n/3) means the smallest integer not less than n/3).

    Hands are represented as dictionaries. The keys are
    letters and the values are the number of times the
    particular letter is repeated in that hand.

    n: int >= 0
    returns: dictionary (string -> int)
    """
    
    hand={}
    num_vowels = int(math.ceil(n / 3))

    for i in range(num_vowels - 1):
        x = random.choice(VOWELS)
        hand[x] = hand.get(x, 0) + 1
    
    for i in range(num_vowels, n):    
        x = random.choice(CONSONANTS)
        hand[x] = hand.get(x, 0) + 1

    hand["*"] = 1

    return hand


def update_hand(hand, word):
    """
    Updates the hand: uses up the letters in the given word
    and returns the new hand, without those letters in it.

    Has no side effects: does not modify hand.

    word: string
    hand: dictionary (string -> int)    
    returns: dictionary (string -> int)
    """

    new_hand = hand.copy()

    for i in range(len(word)):
        try:
            new_hand[word[i]] -= 1
        except KeyError:
            continue

    return new_hand


def is_valid_word(word, hand, word_list):
    """
    Returns True if word is in the word_list and is entirely
    composed of letters in the hand. Otherwise, returns False.
    Does not mutate hand or word_list.
   
    word: string
    hand: dictionary (string -> int)
    word_list: list of lowercase strings
    returns: boolean
    """

    def replace_wildcard(word, word_list):
            for letters in VOWELS:
                x = word.replace("*", letters)
                if x in word_list:
                    return True

    if word in word_list or replace_wildcard(word, word_list):
        freq = get_frequency_dict(word)
        for letter in freq:
            try:
                if hand.get(letter, ) < freq[letter]:
                    return False
                else:
                    return True
            except TypeError:
                return False
    else:
        return False


def calculate_handlen(hand):
    """ 
    Returns the length (number of letters) in the current hand.
    
    hand: dictionary (string-> int)
    returns: integer
    """
    handlen = 0
    for letter in hand:
        handlen += hand[letter]
    return handlen


def play_hand(hand, word_list):

    """
    Allows the user to play the given hand, as follows:
    * The hand is displayed.
    * The user may input a word or a single period (the string ".")
      to indicate they're done playing
    * Invalid words are rejected, and a message is displayed asking
      the user to choose another word until they enter a valid word or "."
    * When a valid word is entered, it uses up letters from the hand.
    * After every valid word: the score for that word is displayed,
      the remaining letters in the hand are displayed, and the user
      is asked to input another word.
    * The sum of the word scores is displayed when the hand finishes.
    * The hand finishes when there are no more unused letters or the user
      inputs a "."
      hand: dictionary (string -> int)
      wordList: list of lowercase strings
      n: integer (HAND_SIZE; i.e., hand size required for additional points)

    """
    print()
    # Keep track of the total scores
    total_score = 0
    n = HAND_SIZE
    
    # As long as there are still letters left in the hand:
    while calculate_handlen(hand) > 0:
        # Display the hand
        print("Current Hand is :  ") 
        display_hand(hand)
        print()
        letter = ""
        substitute_hand(hand, letter)
        # Ask user for input
        word = input("Please enter your word, made up of the letters given, if you are finished insert !! : ")

        # If the input is two exclamation points:
        if word == "!!":
            break
            # End the game (break out of the loop)
            
        # Otherwise (the input is not two exclamation points):
        else:
            # If the word is valid:
            if is_valid_word(word, hand, word_list):
                # Tell the user how many points the word earned,
                total_score += get_word_score(word, n)
                # and the updated total score
                print("The word : ", word, ", earned", get_word_score(word, n), "points. You have a total of", total_score, "points")
                print()

            # Otherwise (the word is not valid):
                # Reject invalid word (print a message)
            else:
                print("That is not a word made up from your hand, please try again")
                print()
            # update the user's hand by removing the letters of their inputted word
            hand = update_hand(hand, word)
    

    # Game is over (user entered '!!' or ran out of letters),
    # so tell user the total score
    if calculate_handlen(hand) == 0:
        print("There are no letters left, total score was", total_score)
    # Return the total score as result of function
    else:
        print("Hand over, total score was", total_score)
    return

    
#
# Problem #6: Playing a game
# 


#
# procedure you will use to substitute a letter in a hand
#

def substitute_hand(hand, letter):
    """ 
    Allow the user to replace all copies of one letter in the hand (chosen by user)
    with a new letter chosen from the VOWELS and CONSONANTS at random. The new letter
    should be different from user's choice, and should not be any of the letters
    already in the hand.

    If user provide a letter not in the hand, the hand should be the same.

    Has no side effects: does not mutate hand.

    For example:
        substitute_hand({'h':1, 'e':1, 'l':2, 'o':1}, 'l')
    might return:
        {'h':1, 'e':1, 'o':1, 'x':2} -> if the new letter is 'x'
    The new letter should not be 'h', 'e', 'l', or 'o' since those letters were
    already in the hand.
    
    hand: dictionary (string -> int)
    letter: string
    returns: dictionary (string -> int)
    """
    sub = input("If you would like to substitute one letter (or a set of) type 'y' if not type 'n' : ")
    sub = sub.lower()
    if sub == "y":
        print()
        try:
            letter = input("Which letter would you like to substitute? : ")
            if letter in hand.keys():

                if letter in VOWELS:
                    new_vowel = letter
                    while new_vowel == letter:
                        new_vowel = random.choice(VOWELS)
                    hand[new_vowel] = hand[letter]
                    del hand[letter]

                elif letter in CONSONANTS:
                    new_consonants = letter
                    while new_consonants == letter:
                        new_consonants= random.choice(CONSONANTS)
                    hand[new_consonants] = hand[letter]
                    del hand[letter]

                display_hand(hand)

            else:
                print("That letter is not in your hand, please choose a different substitution")
                substitute_hand(hand, letter)

        except ValueError:
            print("You have not entered a valid character, please choose a letter in your hand")
            substitute_hand(hand, letter)

    else: 
        return hand

def play_game(word_list):
    """
    Allow the user to play a series of hands

    * Asks the user to input a total number of hands

    * Accumulates the score for each hand into a total score for the 
      entire series
 
    * For each hand, before playing, ask the user if they want to substitute
      one letter for another. If the user inputs 'yes', prompt them for their
      desired letter. This can only be done once during the game. Once the
      substitue option is used, the user should not be asked if they want to
      substitute letters in the future.

    * For each hand, ask the user if they would like to replay the hand.
      If the user inputs 'yes', they will replay the hand and keep 
      the better of the two scores for that hand.  This can only be done  once 
      during the game. Once the replay option is used, the user should not
      be asked if they want to replay future hands. Replaying the hand does
      not count as one of the total number of hands the user initially
      wanted to play.

            * Note: if you replay a hand, you do not get the option to substitute
                    a letter - you must play whatever hand you just had.
      
    * Returns the total score for the series of hands

    word_list: list of lowercase strings
    """
    total_number_hands = int(input("Welcome! How many hands would you like to play? : "))

    while total_number_hands > 0:
        play_hand(deal_hand(HAND_SIZE), word_list)


#
# Build data structures used for entire session and play game
# Do not remove the "if __name__ == '__main__':" line - this code is executed
# when the program is run directly, instead of through an import statement
#

if __name__ == '__main__':
    word_list = load_words()
    play_game(word_list)