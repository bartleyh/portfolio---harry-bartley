## MIT problems Accelerator (2018 - 2019)

### Exercises taken from MIT course:

 https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-0001-introduction-to-computer-science-and-programming-in-python-fall-2016/assignments/

• All problem sheets are self contained with all files

• Test cases written as much as possible (ongoing)