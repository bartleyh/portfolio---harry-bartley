# Problem Set 4A
# Name: <your name here>
# Collaborators:
# Time Spent: x:xx

def get_permutations(sequence):
    '''
    Enumerate all permutations of a given string

    sequence (string): an arbitrary string to permute. Assume that it is a
    non-empty string.  

    You MUST use recursion for this part. Non-recursive solutions will not be
    accepted.

    Returns: a list of all permutations of sequence

    Example:
    >>> get_permutations('abc')
    ['abc', 'acb', 'bac', 'bca', 'cab', 'cba']

    Note: depending on your implementation, you may return the permutations in
    a different order than what is listed here.
    '''
    
    sequence=list(sequence)
    if len(sequence)==1:
        return [sequence]
    permutations=[]
    for i in range(len(sequence)):
        first_letter=sequence[i]          #Pick out an element in the sequence to put at the beginning
        for permute in get_permutations(sequence[:i]+sequence[i+1:]):    #Permute the rest of the sequence without the element 'first'
            permutations.append([first_letter]+permute)
    return permutations

def convert(permutations):
    permutations_new = [''.join(l) for l in permutations]
    return permutations_new


        

if __name__ == '__main__':
#    #EXAMPLE
#    example_input = 'abc'
#    print('Input:', example_input)
#    print('Expected Output:', ['abc', 'acb', 'bac', 'bca', 'cab', 'cba'])
#    print('Actual Output:', get_permutations(example_input))
    
#    # Put three example test cases here (for your sanity, limit your inputs
#    to be three characters or fewer as you will have n! permutations for a 
#    sequence of length n)

    print(get_permutations("abc"))
    print(convert(get_permutations("abc")))
