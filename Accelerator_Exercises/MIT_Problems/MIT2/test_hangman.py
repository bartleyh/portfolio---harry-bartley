import unittest
from hangman_copy import is_word_guessed

class Testhangman(unittest.TestCase):
    def test_is_word_guessed(self):
        self.assertTrue(is_word_guessed)

if __name__ == "__main__":
    unittest.main()