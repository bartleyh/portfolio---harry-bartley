import random
import time
import datetime


class TheBirthdayProblem(object):

    def __init__(self, iterations):
        self.iterations = iterations
        self.BirthdayMatch = 0
        self.NotaMatch = 0


    def ListBirthdays(self, NumPeople):
        BirthdayList = []
        while len(BirthdayList) < NumPeople:
            BirthdayList.append(random.randint(1, 366))
        return BirthdayList


    def MatchingBirthdays(self, BirthdayList):
        BirthdayList.sort()
        counts = [None] * (BirthdayList[25] +1)
        for i in BirthdayList:
            if counts[i] is None:
                counts[i] = i
            elif i == counts[i]:
                return True


    def run(self):
        start_time = time.time()
        iteration = 0
        while iteration <= self.iterations:
            BirthdayList = self.ListBirthdays(26)
            if self.MatchingBirthdays(BirthdayList):
                self.BirthdayMatch += 1
            else:
                self.NotaMatch += 1
            iteration += 1

        self.percentage_calculation(self.BirthdayMatch, self.NotaMatch)
        print("--- Completed in %s seconds ---" % (time.time() - start_time))
    

    def percentage_calculation(self, sucess, failure):
        total_percentage = sucess + failure
        total_sucesses = (sucess / total_percentage)*100
        total_failures = (failure / total_percentage)*100
        print("Total times Birthdays were the same is at", total_sucesses, '%')
        print("Total times Birthdays were different is at", total_failures, '%')


if __name__ == "__main__":
    Problem = TheBirthdayProblem(1000)
    Problem.run()