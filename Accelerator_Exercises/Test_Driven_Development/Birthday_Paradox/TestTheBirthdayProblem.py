import unittest
from TheBirthdayProblem import *

class TestTheBirthdayProblem(unittest.TestCase):

    def test_ListBirthday(self):
        Problem = TheBirthdayProblem(100)
        self.assertEqual(len(Problem.ListBirthdays(26)), 26)

    def test_MatchingBirthdays1(self):
        Problem = TheBirthdayProblem(100)
        BirthdayList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 1]
        self.assertTrue(Problem.MatchingBirthdays(BirthdayList))

    def test_MatchingBirthdays2(self):
        Problem = TheBirthdayProblem(100)
        BirthdayList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]
        self.assertFalse(Problem.MatchingBirthdays(BirthdayList))


if __name__ == "__main__":
    unittest.main()
