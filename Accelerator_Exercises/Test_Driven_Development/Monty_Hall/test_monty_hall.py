import unittest
from monty_hall import *

class TestMontyHall(unittest.TestCase):

    def testPrizedoor(self):
        num =[1, 2, 3]
        Problem = montyHall(1000, True)
        self.assertIn(Problem.prizedoor(), num)


    def testChoosedoor(self):
        num =[1, 2, 3]
        Problem = montyHall(1000, True)
        self.assertIn(Problem.choosedoor(), num)

    def testRemovedoor1(self):
        prize = 1
        choice = 1 
        remove = [2, 3]
        Problem = montyHall(1000, True)
        self.assertIn(Problem.removedoor(prize, choice), remove)

    def testRemovedoor2(self):
        prize = 2
        choice = 2 
        remove = [1, 3]
        Problem = montyHall(1000, True)
        self.assertIn(Problem.removedoor(prize, choice), remove)

    def testRemovedoor3(self):
        prize = 3
        choice = 3
        remove = [1, 2]
        Problem = montyHall(1000, True)
        self.assertIn(Problem.removedoor(prize, choice), remove)

    def testRemovedoor4(self):
        prize = 1
        choice = 2
        remove = 3
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.removedoor(prize, choice), remove)

    def testRemovedoor5(self):
        prize = 3
        choice = 1
        remove = 2
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.removedoor(prize, choice), remove)

    def testRemovedoor6(self):
        prize = 3
        choice = 2
        remove = 1
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.removedoor(prize, choice), remove)

    def testSwapTrue1(self):
        prize = 1
        choice = 1
        removed = 2
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.swap_true(prize, choice, removed), 3)

    def testSwapTrue2(self):
        prize = 2
        choice = 2
        removed = 1
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.swap_true(prize, choice, removed), 3)

    def testSwapTrue3(self):
        prize = 3
        choice = 3
        removed = 2
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.swap_true(prize, choice, removed), 1)

    def testSwapTrue4(self):
        prize = 2
        choice = 2
        removed = 3
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.swap_true(prize, choice, removed), 1)

    def testSwapTrue5(self):
        prize = 1
        choice = 2
        removed = 3
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.swap_true(prize, choice, removed), 1)

    def testSwapTrue6(self):
        prize = 2
        choice = 1
        removed = 3
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.swap_true(prize, choice, removed), 2)


    def testSwapTrue7(self):
        prize = 3
        choice = 1
        removed = 2
        Problem = montyHall(1000, True)
        self.assertEqual(Problem.swap_true(prize, choice, removed), 3)





if __name__ == "__main__":
    unittest.main()