## Monty-Hall Problem

### Written in python with a test case

Quick outline of the problem:

>>> If there are three doors, behind one of which is a prize and the other two are empty, after you randomly choose a door one of the 
other doors is revealed to be empty. If you switch are you more likely to get the prize?

• Yes, as once the original probability of 1/3 is chosen and another door removed, this probability is locked in

• Hence the other door has a probability of 2/3 of containing the prize

• The python program should represent this problem with options to play the game yourself testing its likelihood or running and representing probability