import random
import time

class montyHall(object):


    def __init__(self, iterations, swapping):
        self.swapping = swapping
        self.iterations = iterations
        self.NumSucesses = 0
        self.NumFailures = 0 
    

    def assigndoor(self):
        return random.randint(1,3)


    def prizedoor(self):
        prize = self.assigndoor()
        return prize


    def choosedoor(self):
        choice = self.assigndoor()
        return choice


    def removedoor(self, prize, choice):
        num = [1, 2, 3]

        if prize == choice:
            num.remove(prize)
            removedoor = num[random.randint(0,1)]
            return removedoor
        else:
            num.remove(prize)
            num.remove(choice)
            removedoor = num[0]
            return removedoor


    def swap_true(self, prize, choice, removed):
        if prize == choice:
            choice = 6 - removed - prize
            return choice
        else:
            choice = 6 - choice - removed
            return choice


    def run(self):
        start_time = time.time()
        while self.iterations >= 0:
            
            prize = self.prizedoor()
            choice = self.choosedoor()
            removed = self.removedoor(prize, choice)

            if self.swapping == True:
                final_choice = self.swap_true(prize, choice, removed)
            else:
                final_choice = choice

            if final_choice == prize:
                self.result_sucess()
            else:
                self.result_failure()
            
            self.iterations -= 1
        
        self.percentage_calculation(self.result_sucess(), self.result_failure())
        print("--- Completed in %s seconds ---" % (time.time() - start_time))


    def result_sucess(self):
        self.NumSucesses += 1
        return self.NumSucesses
    

    def result_failure(self):
        self.NumFailures += 1
        return self.NumFailures
    

    def percentage_calculation(self, sucess, failure):
        total_percentage = sucess + failure
        total_sucesses = (sucess / total_percentage)*100
        total_failures = (failure / total_percentage)*100
        print("Total sucesses is at", total_sucesses, '%')
        print("Total failures is at", total_failures, '%')

            
if __name__ == "__smain__":
    
    print("Welcome to the Monty-Hall problem")
    boolean = input("Do you want to initate swapping? Type T for true or F for false : ")
    if boolean.lower() == "t":
         boolean = True
    else:
        boolean = False

    Problem = montyHall(1000000, boolean)
    Problem.run()