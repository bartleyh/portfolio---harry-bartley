## Test Driven development practise Accelerator (2018 - 2019)

### Exercises conducted in a number of languages taken from https://gitlab.com/thgaccelerator/Core_Curriculum/tree/master/technical_content/tdd

• Monty Hall problem in python

• 100-prisoners

• Birthday Paradox

• Box Paradox

• Coconut Problem
