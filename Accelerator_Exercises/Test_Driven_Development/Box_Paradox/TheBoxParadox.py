import random
import time

class TheBoxParadox(object):

    def __init__(self, iterations):
        self.iterations = iterations
        self.box1 = ["Gold", "Gold"]
        self.box2 = ["Gold", "Silver"]
        self.box3 = ["Silver", "Silver"]
        self.boxes = [self.box1, self.box2, self.box3]
        self.GoldGold = 0
        self.GoldSilver = 0


    def assignNumBox(self):
        return random.randint(0,2)

    def assignNumCoin(self):
        return random.randint(0,1)


    def ChooseBox(self, num):
        return self.boxes[num]


    def ChooseCoin(self, box, num):
        return box[num]
        

    
    def run(self):
        start_time = time.time()
        iteration = 0
        while iteration < self.iterations:
            Box = self.ChooseBox(self.assignNumBox())
            if Box != self.box3:
                Num = self.assignNumCoin()
                FirstChoice = self.ChooseCoin(Box, Num)
                if FirstChoice != "Silver":
                    Numbers = [0, 1]
                    Numbers.remove(Num)
                    NewNum = Numbers[0]
                    SecondChoice = self.ChooseCoin(Box, NewNum)
                    if SecondChoice != "Silver":
                        self.GoldGold += 1
                    else:
                        self.GoldSilver +=1
                    
            iteration += 1

            
        self.percentage_calculation(self.GoldGold, self.GoldSilver)
        print("--- Completed in %s seconds ---" % (time.time() - start_time))


    def percentage_calculation(self, sucess, failure):
        total_percentage = sucess + failure
        total_sucesses = (sucess / total_percentage)*100
        total_failures = (failure / total_percentage)*100
        print("Total chance both coins are gold is at", total_sucesses, '%')
        print("Total chance both coins aren't gold is at", total_failures, '%')



        
if __name__ == "__main__":
    
    print("Welcome to the TheBox Paradox")
    Problem = TheBoxParadox(10000)
    Problem.run()
