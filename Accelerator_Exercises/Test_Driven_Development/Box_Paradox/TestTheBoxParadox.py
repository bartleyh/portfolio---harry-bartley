import unittest
from TheBoxParadox import *

class TestTheBoxParadox(unittest.TestCase):

    def testassignNumBox(self):
        num = [0, 1, 2]
        Problem = TheBoxParadox(10000)
        self.assertIn(Problem.assignNumBox(), num)

    
    def testassignNumCoin(self):
        num = [0, 1]
        Problem = TheBoxParadox(10000)
        self.assertIn(Problem.assignNumCoin(), num)


if __name__ == "__main__":
    unittest.main()