import random
import time

class HundredPrisoners(object):

    def __init__(self, PrisAmount):
        self.PrisAmount = PrisAmount
        self.dayNum = 0
        self.lightStatus = " "
        self.prisonersVisit = []
        self.prisoners = []
    
    def listPrisoners(self):
        for i in range(1, 101):
            self.prisoners.append(i)
        return self.prisoners


    def assignPrisoner(self):
        prisoner = random.randint(1, 100)
        return prisoner 

    def GetprisonerLeader(self):
        Leader = self.assignPrisoner()
        return Leader

    def EnterRoom(self, prisoner):
        self.dayNum += 1
        return prisoner


    def light_on(self):
        self.lightStatus = "on"
        return self.lightStatus


    def light_off(self):
        self.lightStatus = "off"
        return self.lightStatus

    def PrisonersVisited(self, visiter):
        if visiter not in self.prisonersVisit:
            self.prisonersVisit.append(visiter)
            return True
        else:
            return False

    def set_up(self, Leader):
        self.EnterRoom(Leader)
        self.light_on()
        self.PrisonersVisited(Leader)
        return self.lightStatus
        

    def run(self):
        start_time = time.time()
        Leader = self.GetprisonerLeader()
        self.set_up(Leader)
        
        Plist = len(self.listPrisoners())
        while (len(self.prisonersVisit)) != Plist:
            prisonerChosen = self.assignPrisoner()
            if self.EnterRoom(prisonerChosen) == Leader:
                if self.lightStatus != self.light_on():
                    self.light_on()
                else:
                    continue
                
            else:
                if self.lightStatus == self.light_on():
                    if self.PrisonersVisited(prisonerChosen):
                        self.light_off

                else:
                    continue
           
 

        print(self.dayNum)        
        print("--- Completed in %s seconds ---" % (time.time() - start_time))
        



 
if __name__ == "__main__":

    Problem = HundredPrisoners(100)
    Problem.run()
