const canvas = document.getElementById("myCanvas");
const ctx = canvas.getContext("2d");
const img = new Image();
// img.onload = function() {
//   ctx.drawImage(img, ghostx, ghosty, 45, 45);
// };
// img.src = "Images/ghost.png";

let rightPressed = false;
let leftPressed = false;
let upPressed = false;
let downPressed = false;
let x = canvas.width / 2;
let y = canvas.height - 250;
let radius = 10;
let ArrayEatenFood = [];
let TotalScore = 0;
let Lives = 3;
let ghostx = 0;
let ghosty = 0;

function range(start, end, step) {
  let array = [];
  for (let k = start; k < end + 1; k += step) {
    array.push(k);
  }
  return array;
}

function reload() {
  location.reload();
}

function drawGhost() {
  ctx.drawImage(img, ghostx, ghosty, 45, 45);
  img.src = "Images/ghost.png";
  GhostMove();
}

function drawPacman() {
  ctx.beginPath();
  ctx.arc(x, y - 20, radius, 0, Math.PI * 2);
  ctx.fillStyle = "yellow";
  ctx.fill();
  ctx.closePath();
}

function drawFood(posx, posy, State) {
  ctx.beginPath();
  ctx.arc(posx, posy, 5, 0, Math.PI * 2);
  if (State == "Alive") {
    ctx.fillStyle = "white";
  } else if (State == "Eaten") {
    ctx.fillStyle = "black";
  }
  ctx.fill();
  ctx.closePath();
}

function currentposition(xPos, yPos) {
  pacmanPos = [xPos, yPos - 20];
  return pacmanPos;
}

function positionFood() {
  let foodPos = [];
  down = 30;
  across = 25;
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 10; j++) {
      State = "Alive";

      if (ArrayEatenFood.length > 0) {
        for (let k = 0; k < ArrayEatenFood.length; k++) {
          if (down == ArrayEatenFood[k].downY) {
            if (across == ArrayEatenFood[k].acrossX) {
              State = "Eaten";
            }
          }
        }
      }
      drawFood(across, down, State);
      foodPos.push({ across, down });
      down += canvas.height / 10;
    }
    down = 30;
    across += canvas.width / 7;
  }
  return foodPos;
}

function touchFood(xPos, yPos, foodpos) {
  position = {
    across: xPos,
    down: yPos - 20
  };

  positionAcrossRange = range(xPos - 5, xPos + 5, 1);
  for (l = 0; l < positionAcrossRange.length; l++) {
    xPos = positionAcrossRange[l];
    position = {
      across: xPos,
      down: yPos - 20
    };
    for (let i = 0; i < foodpos.length; i++) {
      if (foodpos[i].down == position.down) {
        if (foodpos[i].across == position.across) {
          return true;
        } else {
        }
      } else {
        continue;
      }
    }
  }
}

function eatFood(x, y) {
  let foodPosition = {
    acrossX: x,
    downY: y
  };

  function checkfoodArray(food) {
    for (let i = 0; i < ArrayEatenFood.length; i++) {
      if (ArrayEatenFood[i].downY == food.downY) {
        if (ArrayEatenFood[i].acrossX == food.acrossX) {
          return false;
        }
      }
    }
    return true;
  }

  if (ArrayEatenFood.length == 0) {
    ArrayEatenFood.push(foodPosition);
    TotalScore += 10;
  } else {
    if (checkfoodArray(foodPosition) === true) {
      ArrayEatenFood.push(foodPosition);
      TotalScore += 10;
    }
  }
}

function GhostMove() {
  ghosty += 10;
}

function update() {
  let pacman = document.getElementById("lives");
  if (ArrayEatenFood.length == 70) {
    Won();
  } else if (Lives === 0) {
    pacman.parentNode.removeChild(pacman);
    GameOver();
  } else {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (Lives === 2) {
      pacman.parentNode.removeChild(pacman);
    } else if (Lives === 1) {
      pacman.parentNode.removeChild(pacman);
    }
    document.getElementById("score").innerHTML = "Total Score: " + TotalScore;
    document.getElementById("livesWords").innerHTML = "Lives: ";

    drawBoard();
    drawPacman();
    // drawGhost();
  }
}

function drawBoard() {
  positionFood();
  drawBoardHorizontal();
  drawBoardVerticle();
}

function checkPositionLeft() {
  if (x < 35) {
    return false;
  } else {
    return true;
  }
}

function checkPositionRight() {
  if (x > 315) {
    return false;
  } else {
    return true;
  }
}

function checkPositionTop() {
  let limit = radius + 50;
  if (y < limit) {
    return false;
  } else {
    return true;
  }
}

function checkPositionBottom() {
  if (y > 493) {
    return false;
  } else {
    return true;
  }
}

function drawBorderLineVerticle(across, down, width, length) {
  ctx.beginPath();
  ctx.rect(across, down, width, length);
  ctx.fillStyle = "blue";
  ctx.fill();
  ctx.closePath();
}

function drawBorderLineHorizontal(across, down, width, length) {
  ctx.beginPath();
  ctx.rect(across, down, width, length);
  ctx.fillStyle = "blue";
  ctx.fill();
  ctx.closePath();
}

function drawBoardVerticle() {
  let width = 5;
  let across = 50;
  let down = 50;
  let length = 50;
  for (let i = 0; i < 6; i++) {
    if (i == 0 || i == 5) {
      length = 150;
    } else if (i == 1 || i == 4) {
      length = 100;
    } else {
      length = 50;
    }
    drawBorderLineVerticle(across, down, width, length);
    across += 50;
  }
  across = across - 300;
  for (let j = 0; j < 6; j++) {
    if (j == 0 || j == 5) {
      down = 250;
      length = 150;
    } else if (j == 1 || j == 4) {
      down = 200;
      length = 100;
    } else {
      down = 200;
      length = 50;
    }
    drawBorderLineVerticle(across, down, width, length);
    across += 50;
  }
  across = across - 300;
  for (let j = 0; j < 6; j++) {
    if (j == 1 || j == 4) {
      down = 350;
      length = 100;
      drawBorderLineVerticle(across, down, width, length);
    }
    if (j == 2 || j == 3) {
      down = 400;
      length = 50;
      drawBorderLineVerticle(across, down, width, length);
    }
    across += 50;
  }
}

function drawBoardHorizontal() {
  let length = 5;
  let across = 50;
  let down = 100;
  let width = 50;
  for (let i = 0; i < 6; i++) {
    if (i == 1 || i == 3) {
      down = 150;
      for (let j = 0; j < 3; j++) {
        if (j == 2) {
          width = 50;
          down = 350;
        }
        drawBorderLineHorizontal(across, down, width, length);
        down += 150;
      }
    } else if (i == 2) {
      down = 100;
      for (let j = 0; j < 4; j++) {
        if (j == 2) {
          down = 300;
        } else if (j == 3) {
          down == 400;
        }
        width = 55;
        drawBorderLineHorizontal(across, down, width, length);
        down += 150;
      }
    } else if (i == 0 || i == 4) {
      down = 450;
      width = 55;
    } else if (i == 5) {
      break;
    }
    drawBorderLineHorizontal(across, down, width, length);
    across += 50;
  }
}

function Won() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.font = "Press Start 2P";
  let textString = "YOU WON!";

  textWidth = ctx.measureText(textString).width;
  ctx.fillText(textString, canvas.width / 2 - textWidth / 2, 100);
}

function GameOver() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.font = "Press Start 2P";
  let textString = "GAME OVER!";

  textWidth = ctx.measureText(textString).width;
  ctx.fillText(textString, canvas.width / 2 - textWidth / 2, 100);
}

function run(x, y) {
  currentposition(x, y);
  if (touchFood(x, y, positionFood()) === true) {
    eatFood(position.across, position.down);
  }
  update();
}

function Row(x, y) {
  let ThreeFirst = range(25, 115, 10);
  let ThreeSecond = range(125, 235, 10);
  let ThreeThird = range(275, 325, 10);
  let FourSecond = range(75, 275, 10);
  if (y == 50 || y == 500) {
    return "row1";
  } else if (
    y == 60 ||
    y == 70 ||
    y == 80 ||
    y == 90 ||
    y == 100 ||
    y == 110 ||
    y == 120 ||
    y == 130 ||
    y == 410 ||
    y == 420 ||
    y == 430 ||
    y == 440 ||
    y == 450 ||
    y == 460 ||
    y == 470 ||
    y == 480
  ) {
    return "row2";
  } else if (y == 140 || y == 150 || y == 390 || y == 400) {
    for (let i = 0; i < ThreeFirst.length; i++) {
      if (x == ThreeFirst[i]) {
        return "row3a";
      }
    }
    for (let j = 0; j < ThreeSecond.length; j++) {
      if (x == ThreeSecond[j]) {
        return "row3b";
      }
    }
    for (let k = 0; k < ThreeThird.length; k++) {
      if (x == ThreeThird[k]) {
        return "row3c";
      }
    }
  } else if (y == 200 || y == 350) {
    if (x == 25) {
      return "row4a";
    }

    for (let j = 0; j < FourSecond.length; j++) {
      if (x == FourSecond[j]) {
        return "row4b";
      }
    }
    if (x == 325) {
      return "row4c";
    }
  } else if (
    y == 210 ||
    y == 220 ||
    y == 230 ||
    y == 240 ||
    y == 250 ||
    y == 260 ||
    y == 270 ||
    y == 280
  ) {
    return "row5";
  } else if (y == 290 || y == 300) {
    for (let i = 0; i < ThreeFirst.length; i++) {
      if (x == ThreeFirst[i]) {
        return "row6a";
      }
    }
    for (let j = 0; j < ThreeSecond.length; j++) {
      if (x == ThreeSecond[j]) {
        return "row6b";
      }
    }
    for (let k = 0; k < ThreeThird.length; k++) {
      if (x == ThreeThird[k]) {
        return "row6c";
      }
    }
  }
}

document.addEventListener("keydown", event => {
  if (keyCode === 38) {
    setInterval(drawGhost(), 500);
  }
});

document.addEventListener("keydown", event => {
  if (event.keyCode === 37) {
    if (checkPositionLeft() === true) {
      if (Row(x, y) == "row1") {
        x -= 10;
      } else if (
        Row(x, y) == "row2" ||
        Row(x, y) == "row3a" ||
        Row(x, y) == "row3c" ||
        Row(x, y) == "row4a" ||
        Row(x, y) == "row4c" ||
        Row(x, y) == "row6a" ||
        Row(x, y) == "row6c"
      ) {
        if (y == 440 || y == 450) {
          if (x < 85 || x > 275) {
            x -= 10;
          }
        } else x = x;
      } else if (Row(x, y) == "row3b") {
        if (x > 125) {
          x -= 10;
        }
      } else if (Row(x, y) == "row4b") {
        if (x > 75) {
          x -= 10;
        }
      } else if (Row(x, y) == "row5") {
        if (y == 240 || y == 250) {
          if (x < 85 || x > 275) {
            x -= 10;
          }
        }
      } else if (Row(x, y) == "row6b") {
        if (x > 125) {
          x -= 10;
        }
      } else x = x;
      run(x, y);
    }
  } else if (event.keyCode === 39) {
    if (checkPositionRight() === true) {
      if (Row(x, y) == "row1") {
        x += 10;
      } else if (
        Row(x, y) == "row2" ||
        Row(x, y) == "row3a" ||
        Row(x, y) == "row3c" ||
        Row(x, y) == "row6c" ||
        Row(x, y) == "row6a"
      ) {
        if (y == 440 || y == 450) {
          if (x < 75 || x > 265) {
            x += 10;
          }
        } else x = x;
      } else if (Row(x, y) == "row3b") {
        if (x < 225) {
          x += 10;
        }
      } else if (Row(x, y) == "row4b") {
        if (x < 275) {
          x += 10;
        }
      } else if (Row(x, y) == "row5") {
        if (y == 240 || y == 250) {
          if (x < 75 || x > 265) {
            x += 10;
          }
        }
      } else if (Row(x, y) == "row6b") {
        if (x < 225) {
          x += 10;
        }
      } else x = x;
      run(x, y);
    }
  } else if (event.keyCode === 40) {
    if (checkPositionBottom() === true) {
      if (Row(x, y) == "row1" || Row(x, y) == "row2") {
        if (x == 25 || x == 125 || x == 225 || x == 325) {
          y += 10;
        } else if (x == 175) {
          if (y < 100 || y == 410 || y == 420 || y == 430 || y == 440) {
            y += 10;
          }
        } else if (x == 75 || x == 275) {
          if (y == 450) {
            y = y;
          } else y += 10;
        }
      } else if (Row(x, y) == "row3a" || Row(x, y) == "row3c") {
        y += 10;
      } else if (Row(x, y) == "row3b") {
        if (y == 140 || y == 150) {
          if (x == 175) {
            y += 10;
          }
        } else if (y == 400 || y == 390) {
          if (x == 125 || x == 175 || x == 225) {
            y += 10;
          }
        }
        if (y < 150) {
          y += 10;
        } else y = y;
      } else if (Row(x, y) == "row4b") {
        if (y == 200) {
          if (x == 75 || x == 125 || x == 175 || x == 225 || x == 275) {
            y += 10;
          }
        } else if (y == 350) {
          if (x == 75 || x == 175 || x == 275) {
            y += 10;
          }
        }
      } else if (Row(x, y) == "row5") {
        if (
          x == 25 ||
          x == 75 ||
          x == 125 ||
          x == 225 ||
          x == 275 ||
          x == 325
        ) {
          y += 10;
        } else if (x == 175)
          if (y < 250) {
            y += 10;
          }
      } else if (Row(x, y) == "row6b") {
        if (y == 290) {
          y += 10;
        } else y = y;
      } else y += 10;
      run(x, y);
    }
  } else if (event.keyCode === 38) {
    if (checkPositionTop() === true) {
      if (
        Row(x, y) == "row2" ||
        Row(x, y) == "row5" ||
        Row(x, y) == "row3a" ||
        Row(x, y) == "row3c" ||
        Row(x, y) == "row4a" ||
        Row(x, y) == "row4b"
      ) {
        if (
          x == 25 ||
          x == 75 ||
          x == 125 ||
          x == 175 ||
          x == 225 ||
          x == 275 ||
          x == 325
        ) {
          y -= 10;
        } else y = y;
      } else if (y == 190 || y == 200 || y == 180 || y == 170 || y == 160) {
        if (x == 25 || x == 75 || x == 175 || x == 275 || x == 325) {
          y -= 10;
        } else y = y;
      } else if (
        Row(x, y) == "row6a" ||
        Row(x, y) == "row6b" ||
        Row(x, y) == "row6c"
      ) {
        if (
          x == 25 ||
          x == 75 ||
          x == 25 ||
          x == 125 ||
          x == 225 ||
          x == 275 ||
          x == 325
        ) {
          y -= 10;
        }
      } else if (y == 500 || y == 490) {
        if (x == 25 || x == 125 || x == 225 || x == 325) {
          y -= 10;
        }
      } else if (y == 400 || y == 390 || y == 380 || y == 370 || y == 360) {
        if (x == 25 || x == 75 || x == 175 || x == 275 || x == 325) {
          y -= 10;
        }
      } else if (y == 340 || y == 330 || y == 320 || y == 310 || y == 300) {
        if (x == 25 || x == 75 || x == 275 || x == 325) {
          y -= 10;
        }
      } else if (Row(x, y) == "row3b") {
        if (x == 125 || x == 225) {
          y -= 10;
        }
      }
      run(x, y);
    }
  }
});

onload = drawGhost();
update();
