## React Syntax

see: https://reactjs.org/tutorial/tutorial.html

### What is React?

• Declarative, efficient and flexible Javascript library for building user interfaces

> Components : small, isolated pieces of code that tell React what we want to see on the screen, when our data changes, React willupdate and re-render components

• A component takes in props (properties) and returns a hierarchy of views to display via the render method


#### Render Method

• Returns a description of what you want to see on the screen

• Returns a React element, which is a lightweight description of what to render


#### Passing Data through Props

e.g. 

passing a prop called value through by using:

<button className = "square" >
    {this.props.value}
</button>

