from flask import Flask, render_template, url_for, flash, redirect
from forms import RegistrationForm, LoginForm
app = Flask(__name__)

app.config['SECRET_KEY'] = 'fd230aa8d539e7e5daa3d0ec47e439c2'

posts = [
    {
        "author" : "Harry Bartley",
        "title" : "Blog Post1",
        "content" : "First Blog Post",
        "date_posted" : "April 21, 2018"
    },
    {
        "author" : "Test Author",
        "title" : "Blog Post2",
        "content" : "Second Blog Post",
        "date_posted" : "April 22, 2018"
    }
]


@app.route("/")
@app.route("/home")
def home():
    return render_template("home.html", posts = posts)


@app.route("/about")
def about():
    return render_template("about.html", title="About")


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        flash(f'Account created for {form.username.data}!', 'success')
        return redirect(url_for('home'))
    return render_template("register.html", title="Register", form=form)


@app.route("/login")
def login():
    form = LoginForm()
    return render_template("login.html", title="Login", form=form)


if __name__ == "__main__":
    app.run(debug=True)