Harry Bartley
# Java course overview
Suggested book -- [Java: A Beginner's Guide, Eighth Edition (Schildt)](https://www.amazon.co.uk/Java-Beginners-Eighth-Herbert-Schildt/dp/1260440214/ref=dp_ob_title_bk)

## Week 1
- How to learn new languages -- classifying different aspects of programming languages, course objectives. Compiler overview.
- Java philosophy 
- Java build/execution model -- with and without an IDE -- bytecode theory and practice, JVM, JDK vs SDK etc. Interpreting compiler errors.
Basic syntax  -- classes, methods, objects, main

- Practical involving using javac/java without IDE; inspecting bytecode
- Types -- primitives, classes and overview of memory areas
- Control statements -- for, while, switch, if (logical short-circuit)
- Using your IDE effectively -- modules, navigation, debugging, import refactoring
- Basic Debugging

## Week 2
- OO week
- Visibility modifiers
- Interfaces, abstract classes
- Packages
- Inheritance
-Logical Short circuiting
- Abstract Classes, Interfaces, overriding and overloading
- Polymorphism, composition, aggregation
- junit Testing

## Week 3
- Data Structures: Lists, ArrayLists, Hashing, Queues, Maps
- GRASP and design principles for metrices like cohesion and coupling, open close principle, controller, MVC etc.
- Most used creational, structural and behavioural patterns and their Implementation (builder, factory, singleton etc)

## Week 4
- Java streams and method references
- Testing -- frameworks and designing for testability
- Generics; type variables
- Mockito

## Week 5
- Dependencies, build (gradle, maven, artifactory)
- Dependency injection

- Practical -- app with manual DI, then with a few sample frameworks


## Week 6
- Exceptions
- Logging
- Interpreting stack traces

- Practical -- find the bug in a legacy app

## Week 7
- Multi threading -- concurrency foundations
- Worker pools
- Synchronisation -- locks (first pass), synchronized keyword (second pass)

- Practical -- lifts in MC -- write your own allocation algo -- see whose is best

## Week 8
- REST week
- Theory -- recap of HTTP, motivation for REST by showing different ways of using verbs and the need for convention
- Definition of the Richardson maturity model with examples
- Spring boot (code + xml), tomcat examples -- relative pros and cons
- Auto-generating REST documentation e.g. swagger

- Practical -- make a toy REST app with a backing db

## Week 9
- Practical week
- Software workflows -- talk through a few examples and how jenkins can be used to implement
- Advanced testing -- Mockito
- Code coverage -- measuring with IDE and build


## Week 10
- Advanced topics
- auto boxing, annotations, javadoc
- Reflection, casting, transient, volatile keywords
- Dependency lattices and their management


## Week 11
- Memory week
- Garbage collection -- theory -- reference retention vs leaks; 
- Heap profiles and tools to diagnose issues
- GC practical -- find the leak or churn.


## Week 12
- Practical examples of elysium and the properties service map performance issue
- Java + Docker
- Metrics -- collecting and interpreting -- graphite + grafana


