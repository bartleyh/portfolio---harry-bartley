package com.thehutgroup.accelerator;

import com.thehutgroup.accelerator.Main;
import com.thehutgroup.accelerator.MySecondClass;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;


public class MySecondClassTest {

    @Test
    public void greaterThanTest() {
        MySecondClass myObject = new MySecondClass();
        assertEquals("10 is greater than 5", true , myObject.greaterThan(10, 5));

    }

}