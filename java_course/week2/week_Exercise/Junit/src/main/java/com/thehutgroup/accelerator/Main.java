package com.thehutgroup.accelerator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args)throws IOException {

        MySecondClass myObject = new MySecondClass();


        System.out.println(" Enter the first interger");
        BufferedReader myReader= new BufferedReader(new InputStreamReader(System.in));
        int firstNumber= Integer.parseInt(myReader.readLine());

        System.out.println(" Enter the second interger");
        BufferedReader mySecondReader= new BufferedReader(new InputStreamReader(System.in));
        int secondNumber= Integer.parseInt(mySecondReader.readLine());

        System.out.println(" Enter ==, !=, < OR > for required comparsion");
        BufferedReader myOperationalReader= new BufferedReader(new InputStreamReader(System.in));
        String operation= myOperationalReader.readLine();

        if (operation.equals("=="))
        {
            myObject.equalTo(firstNumber,secondNumber);
        }
        else  if (operation.equals("!="))
        {
            myObject.notEqualTo(firstNumber,secondNumber);
        }
        else  if (operation.equals(">"))
        {
            myObject.greaterThan(firstNumber,secondNumber);
        }
        else  if (operation.equals("<"))
        {
            myObject.lessThan(firstNumber,secondNumber);
        }
    }

}
