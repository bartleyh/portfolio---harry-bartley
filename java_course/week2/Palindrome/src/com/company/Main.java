package com.company;

public class Main {

    public static void main(String[] args) {
        try {
            int InputNumber = Integer.parseInt(args[0]);
            if (args.length == 1) {
                System.out.println(isPalindrome(InputNumber));
            }
        } catch (Exception e) {
            System.out.println("Invalid Input, please try again");
        }
    }

    private static boolean isPalindrome(int Input) {
        int original = Input;
        int reverse = 0;
        while (Input!= 0) {
            int remainder = Input % 10;
            reverse = reverse * 10 + remainder;
            Input = Input / 10;
        }
        return original == reverse;
    }
}
