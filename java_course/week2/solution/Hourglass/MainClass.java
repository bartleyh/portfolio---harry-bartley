import java.io.IOException;

public class MainClass {

    public static void main(String[] args)throws IOException {

	int rows = 0;
	int stars = 0;
	int start = 0;

	if (args.length == 1) {
	    rows = Integer.parseInt(args[0]);
	    for(int i = rows; i >= 2-rows; i = i -1) {
		if(i<1) {
		    stars = stars + 2;
		}
		else {
		    stars = (2 * i) - 1;
		}
         	for(int j = stars; j > 0; j = j -1) {
		    System.out.print("*");
		}
		System.out.printf("%n");
		start = start + 1;
                if(i<2) {
                    start = start - 2;
                }
		for(int k = start; k>0; k = k - 1) {
		    System.out.printf(" ");
		}
	    }
        }
	    else {
		System.err.println("Invalid Input: Please enter numbers.");	        
	    }

    }

}