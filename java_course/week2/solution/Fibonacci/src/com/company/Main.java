package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        try {
            if (args.length == 1){
                int number = Integer.parseInt(args[0]);
                System.out.println(Arrays.toString(FibonacciSequence(number)));
            } else {
                System.out.println("Invalid input, please try again");
            }
        }
        catch (Exception e){
            System.out.println("Invalid input, please try again");
        }

    }

    private static int[] FibonacciSequence(int Fibnumber) {

        int[] result = new int[Fibnumber];

        int index = 1;

    try {

        while(index <= Fibnumber) {
            if (index == 1) {
                result[0] = 1;
                index++;
            } else if (index == 2) {
                result[1] = 2;
                index++;
            } else {
                result[index-1] = result[index-2] + result[index-3];
                index++;
            }
        }
    }

    catch (Exception e){
        System.out.println("Invalid input, please try again");
        }

        return result;

    }


}
