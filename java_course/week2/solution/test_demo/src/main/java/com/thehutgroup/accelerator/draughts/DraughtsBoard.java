package com.thehutgroup.accelerator.draughts;

import com.thehutgroup.accelerator.draughts.piece.DraughtsPiece;

public class DraughtsBoard {
    private DraughtsPiece[][] board;

    public DraughtsBoard() {
        this.board = new DraughtsPiece[getSize()][getSize()];
    }

    public DraughtsBoard(DraughtsPiece[][] board) {
        this.board = board;
    }

    public boolean positionVacant(Position position) {
        return positionWithinBoard(position)
                && board[position.getY()][position.getX()] == null;
    }

    public boolean positionOccupied(Colour colour, Position position) {
        return positionWithinBoard(position)
                && board[position.getY()][position.getX()] != null
                && board[position.getY()][position.getX()].getColour() == colour;
    }

    public int getSize() {
        return 8;
    }

    public boolean positionWithinBoard(Position position) {
        return 0 <= position.getX() && position.getX() < getSize()
                && 0 <= position.getY() && position.getY() < getSize();
    }
}
