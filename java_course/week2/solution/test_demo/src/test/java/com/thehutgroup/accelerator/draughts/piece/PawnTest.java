package com.thehutgroup.accelerator.draughts.piece;

import com.thehutgroup.accelerator.draughts.DraughtsBoard;
import com.thehutgroup.accelerator.draughts.Colour;
import com.thehutgroup.accelerator.draughts.Position;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class PawnTest {
    private static Pawn BP = new Pawn(Colour.BLACK);
    private static Pawn WP = new Pawn(Colour.WHITE);

    @Test
    public void PawnMovementStartTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=7
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, BP  , null, BP  , null, BP  , null},
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {WP  , null, WP  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=7
        }));

        Set<Position> positions = WP.possibleMoves(new Position(1, 1), start);
        assertEquals("should have no options",
                new HashSet<>(),
                positions);

        positions = WP.possibleMoves(new Position(0, 2), start);
        assertEquals("should have one option",
                new HashSet<>(Arrays.asList(new Position(1, 3))),
                positions);


        positions = WP.possibleMoves(new Position(2, 2), start);
        assertEquals("should have two options",
                new HashSet<>(Arrays.asList(new Position(3, 3), new Position(1, 3))),
                positions);



    }

    @Test
    public void PawnMovementSingleCaptureTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=7
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, BP  , null, BP  , null, BP  , null},
                {null, BP  , null, BP  , null, null, null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, BP  , null, null, null, null},
                {WP  , null, WP  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=7
        }));


        Set<Position> positions = WP.possibleMoves(new Position(2, 2), start);
        assertEquals("should have one option to capture and one to move",
                new HashSet<>(Arrays.asList(new Position(4, 4), new Position(1, 3))),
                positions);


    }

    @Test
    public void PawnMovementDoubleCaptureTest() {
        DraughtsBoard start = new DraughtsBoard(reverse(new DraughtsPiece[][] {
                //y=7
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {BP  , null, null, null, BP  , null, null, null},
                {null, BP  , null, BP  , null, BP  , null, BP  },
                {null, null, null, null, null, null, null, null},
                {null, null, null, BP  , null, null, null, null},
                {WP  , null, WP  , null, WP  , null, WP  , null},
                {null, WP  , null, WP  , null, WP  , null, WP  },
                {WP  , null, WP  , null, WP  , null, WP  , null}
                //y=0,x=0                                 //x=7
        }));


        Set<Position> positions = WP.possibleMoves(new Position(2, 2), start);
        assertEquals("should have three options to capture and one to move",
                new HashSet<>(Arrays.asList(new Position(4, 4),
                                            new Position(1, 3),
                                            new Position(6, 6),
                                            new Position(2, 6)
                        )),
                positions);



    }

    private DraughtsPiece[][] reverse(DraughtsPiece[][] pieces) {
        for (int i = 0; i < pieces.length / 2; i++) {
            DraughtsPiece[] swap = pieces[i];
            pieces[i] = pieces[pieces.length - i - 1];
            pieces[pieces.length - i - 1] = swap;
        }
        return pieces;
    }
}
