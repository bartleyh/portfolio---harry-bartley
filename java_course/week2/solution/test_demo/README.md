# Java testing example project
## Setting up dependencies
### Prerequisites
Ensure you have Java SDK 11 installed from [here](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)

Download Maven from [here](https://maven.apache.org/install.html)

Copy the settings.xml file (inside the test_demo folder) into ~/.m2 (this folder will appear after installing maven)

### Adding the trust store
Copy cacerts-plus-artifactory.jks (inside the test_demo folder) to ~/.m2

Add the following line to ~/.bashrc

export MAVEN_OPTS="-Djavax.net.ssl.trustStore=~/.m2/cacerts-plus-artifactory.jks -Djavax.net.ssl.trustStorePassword=changeit -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true"

### Running the tests
Run the command mvn test inside the folder containing pom.xml

Inspect the results in target/surefire-reports

You can also run the tests in your IDE