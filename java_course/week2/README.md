## Accelerator 2018-2019 Harry Bartley

### Week 2

- OO week
- Visibility modifiers
- Interfaces, abstract classes
- Packages
- Inheritance
- Logical Short circuiting
- Abstract Classes, Interfaces, overriding and overloading
- Polymorphism, composition, aggregation
- junit Testing
