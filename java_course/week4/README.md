## Accelerator 2018-2019 Harry Bartley

### Week 4

- Java streams and method references
- Testing -- frameworks and designing for testability
- Generics; type variables
- Mockito
