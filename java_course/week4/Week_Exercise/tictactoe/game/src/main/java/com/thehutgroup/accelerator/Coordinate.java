package com.thehutgroup.accelerator;

public class Coordinate {
  private int x;
  private int y;

  public Coordinate(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public void setX(int x) {
    this.x = x;
  }

  public void setY(int y) {
    this.y = y;
  }

  public int getY() {
    return y;
  }

  public int getX () {
    return x;
  }

  public int[] returnCoords() {
    int[] coords = new int[2];
    coords[0] = getX();
    coords[1] = getY();
    return coords;
  }

  public String toString() {
    return "[" + getX() + "," + getY() + "]";
  }

}
