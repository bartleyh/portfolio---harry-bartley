package com.thehutgroup.accelerator.AiMoves;

import com.thehutgroup.accelerator.Board;
import com.thehutgroup.accelerator.Coordinate;
import com.thehutgroup.accelerator.Piece;

public class EasyAi extends IndividualStrategy implements Strategies   {

  public EasyAi(Board board) {
    super(board);
  }

  public Coordinate makeMove(Piece opponentsPiece) {
    Coordinate coords = new Coordinate(0, 0);
    Coordinate stopWinning = twoInRow(opponentsPiece);
    if (coords.getX() == stopWinning.getX() && coords.getY() == stopWinning.getY()) {
      Coordinate random = randomMove();
      coords.setX(random.getX());
      coords.setY(random.getY());
    } else {
      coords.setX(stopWinning.getX());
      coords.setY(stopWinning.getY());
    }
    while (checkIfOccupied(coords)) {
      Coordinate random = randomMove();
      coords.setX(random.getX());
      coords.setY(random.getY());
    }
    return coords;
  }}
