package com.thehutgroup.accelerator;

import com.thehutgroup.accelerator.AiMoves.EasyAi;
import com.thehutgroup.accelerator.AiMoves.HardAi;
import com.thehutgroup.accelerator.AiMoves.MediumAi;

import java.util.Scanner;

public class gameRunner {


  public void playGame() {
    System.out.println("-----------------------");
    System.out.println("Welcome to tic-tac-toe!");
    System.out.println("-----------------------");
    Board board = new Board();
    int random = (int) (Math.random() * 2);
    Piece piece = Piece.values()[random];
    if (multiplayerOrSingleplayer()) {
      String difficulty = askDifficulty();
      System.out.println("You are player: " + piece);
      System.out.println(" ");
      while (true) {
        System.out.println("Its your turn!");
        System.out.println(" ");
        playerTurn(board, piece);
        if (board.gameOver(piece)) {
          break;
        }
        aiTurn(board, Piece.getPiece(piece), piece, difficulty);
        if (board.gameOver(Piece.getPiece(piece))) {
          break;
        }
        System.out.println(board.getOccupied());
      }
    } else {
      System.out.println("Player One is : " + piece + "s");
      System.out.println(" ");
      while (true) {
        System.out.println("It is player " + piece + "s" + " turn");
        System.out.println(" ");
        playerTurn(board, piece);
        if (board.gameOver(piece)) {
          break;
        }
        piece = Piece.getPiece(piece);
      }
    }
    if (playAgain()) {
      playGame();
    }
  }

  public boolean multiplayerOrSingleplayer() {
    System.out.println();
    Scanner sc = new Scanner(System.in);
    System.out.println("For singleplayer enter s and for multiplayer enter m");
    return (sc.next().equals("s"));
  }

  public String askDifficulty() {
    System.out.println();
    Scanner sc = new Scanner(System.in);
    System.out.println("For easy/medium/hard difficulty type: e/m/h");
    String answer = sc.next();
    if(answer.equals("e") || answer.equals("m") || answer.equals("h")) {
      return (sc.next());
    } else {
      askDifficulty();
    }
    return (sc.next());
  }

  public void playerTurn(Board board, Piece piece) {
    Player player = new Player(board);
    board.printBoard();
    board.insertPiece(player.chooseMove(), piece);
    board.printBoard();
    System.out.println(" ");
  }

  public boolean playAgain() {
    System.out.println(" ");
    System.out.println("Game Over");
    System.out.println(" ");
    Scanner sc = new Scanner(System.in);
    System.out.println("Would you like to play again? y/n :");
    return sc.next().equals("y");
  }


  public void aiTurn(Board board, Piece piece, Piece opponentsPiece, String difficulty) {
      if(difficulty.equals("e")) {
        EasyAi ai = new EasyAi(board);
        board.insertPiece(ai.makeMove(opponentsPiece), piece);
      } else if (difficulty.equals("m")) {
        MediumAi ai = new MediumAi(board);
        board.insertPiece(ai.makeMove(opponentsPiece), piece);
      } else {
        HardAi ai = new HardAi(board);
        board.insertPiece(ai.makeMove(opponentsPiece), piece);
      }
      System.out.println(" ");
  }
}
