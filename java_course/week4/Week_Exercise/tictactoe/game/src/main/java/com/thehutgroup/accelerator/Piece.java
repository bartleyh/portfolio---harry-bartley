package com.thehutgroup.accelerator;

public enum Piece {
  X,
  O;

  public static Piece getPiece(Piece piece) {
    switch (piece) {
      case X:
        return O;
      case O:
        return X;
      default:
        throw new RuntimeException("Invalid player");
    }
  }

}


