package com.thehutgroup.accelerator.AiMoves;

import com.thehutgroup.accelerator.Board;
import com.thehutgroup.accelerator.Coordinate;
import com.thehutgroup.accelerator.Piece;

public class MediumAi extends IndividualStrategy implements Strategies  {

  public MediumAi(Board board) {
    super(board);
  }

  public Coordinate makeMove(Piece opponentsPiece) {
    Coordinate coords = new Coordinate(0, 0);
    Piece aiPiece = Piece.getPiece(opponentsPiece);
    Coordinate stopWinning = twoInRow(opponentsPiece);
    Coordinate tryWinning = twoInRow(aiPiece);
    if (coords.getX() == stopWinning.getX() && coords.getY() == stopWinning.getY()) {
      if (coords.getX() == tryWinning.getX() && coords.getY() == tryWinning.getY()) {
        Coordinate random = randomMove();
        coords.setX(random.getX());
        coords.setY(random.getY());
        if (board.getOccupied().size() < 2) {
          Coordinate firstMove = firstMoves(opponentsPiece, aiPiece);
          coords.setX(firstMove.getX());
          coords.setY(firstMove.getY());
        } else if (board.getOccupied().size() < 4) {
          Coordinate secondMove = firstMoves(opponentsPiece, aiPiece);
          coords.setX(secondMove.getX());
          coords.setY(secondMove.getY());
        }
      } else {
        coords.setX(tryWinning.getX());
        coords.setY(tryWinning.getY());
      }
    } else {
      coords.setX(stopWinning.getX());
      coords.setY(stopWinning.getY());
    }
    while (checkIfOccupied(coords)) {
      makeMove(opponentsPiece);
    }
    return coords;
  }
}
