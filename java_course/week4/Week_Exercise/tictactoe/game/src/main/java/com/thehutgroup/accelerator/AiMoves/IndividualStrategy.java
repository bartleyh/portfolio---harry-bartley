package com.thehutgroup.accelerator.AiMoves;

import com.thehutgroup.accelerator.Board;
import com.thehutgroup.accelerator.Coordinate;
import com.thehutgroup.accelerator.Piece;

public abstract class IndividualStrategy {
  public Board board;

  public IndividualStrategy(Board board) {
    this.board = board;
  }

  public boolean checkIfOccupied(Coordinate coords) {
    String newCoords = coords.toString();
    if (board.isOccupied(newCoords)) {
      return true;
    }
    return false;
  }

  public Coordinate randomMove() {
    Coordinate coords = new Coordinate(0, 0);
    int randomNine = (int) (Math.random() * 9);
    int[][] selection = new int[][] {{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}};
    int[] newSelection = selection[randomNine];
    coords.setX(newSelection[0] + 1);
    coords.setY(newSelection[1] + 1);
    return coords;
  }

  public Coordinate firstMoves(Piece opponentPiece, Piece aiPiece) {
    Coordinate coords = new Coordinate(0, 0);
    if (board.getBoard()[1][1] == opponentPiece || board.getBoard()[1][1] == aiPiece) {
      int randomFour = (int) (Math.random() * 4);
      int[][] selection = new int[][] {{0, 0}, {0, 2}, {2, 0}, {2, 2}};
      int[] newSelection = selection[randomFour];
      coords.setX(newSelection[0] + 1);
      coords.setY(newSelection[1] + 1);
    } else {
      coords.setX(2);
      coords.setY(2);
    }
    return coords;
  }

  public Coordinate twoInRow(Piece piece) {
    Coordinate coords = new Coordinate(0, 0);
    if (board.getBoard()[0][0] == piece && board.getBoard()[0][1] == piece) {
      if (board.getBoard()[0][2] != Piece.getPiece(piece)) {
        coords.setX(1);
        coords.setY(3);
      }
    } else if (board.getBoard()[0][1] == piece && board.getBoard()[0][2] == piece) {
      if (board.getBoard()[0][0] != Piece.getPiece(piece)) {
        coords.setX(1);
        coords.setY(1);
      }
    } else if (board.getBoard()[1][0] == piece && board.getBoard()[1][1] == piece) {
      if (board.getBoard()[1][2] != Piece.getPiece(piece)) {
        coords.setX(2);
        coords.setY(3);
      }
    } else if (board.getBoard()[1][1] == piece && board.getBoard()[1][2] == piece) {
      if (board.getBoard()[1][0] != Piece.getPiece(piece)) {
        coords.setX(2);
        coords.setY(1);
      }
    } else if (board.getBoard()[2][0] == piece && board.getBoard()[2][1] == piece) {
      if (board.getBoard()[2][2] != Piece.getPiece(piece)) {
        coords.setX(3);
        coords.setY(3);
      }
    } else if (board.getBoard()[2][1] == piece && board.getBoard()[2][2] == piece) {
      if (board.getBoard()[2][0] != Piece.getPiece(piece)) {
        coords.setX(3);
        coords.setY(1);
      }
    } else if (board.getBoard()[0][0] == piece && board.getBoard()[1][0] == piece) {
      if (board.getBoard()[2][0] != Piece.getPiece(piece)) {
        coords.setX(3);
        coords.setY(1);
      }
    } else if (board.getBoard()[0][1] == piece && board.getBoard()[1][1] == piece) {
      if (board.getBoard()[2][1] != Piece.getPiece(piece)) {
        coords.setX(3);
        coords.setY(2);
      }
    } else if (board.getBoard()[0][2] == piece && board.getBoard()[1][2] == piece) {
      if (board.getBoard()[2][2] != Piece.getPiece(piece)) {
        coords.setX(3);
        coords.setY(3);
      }
    } else if (board.getBoard()[1][0] == piece && board.getBoard()[2][0] == piece) {
      if (board.getBoard()[0][0] != Piece.getPiece(piece)) {
        coords.setX(1);
        coords.setY(1);
      }
    } else if (board.getBoard()[1][1] == piece && board.getBoard()[2][1] == piece) {
      if (board.getBoard()[0][1] != Piece.getPiece(piece)) {
        coords.setX(1);
        coords.setY(2);
      }
    } else if (board.getBoard()[1][2] == piece && board.getBoard()[2][2] == piece) {
      if (board.getBoard()[0][2] != Piece.getPiece(piece)) {
        coords.setX(1);
        coords.setY(3);
      }
    } else if (board.getBoard()[0][0] == piece && board.getBoard()[1][1] == piece) {
      if (board.getBoard()[2][2] != Piece.getPiece(piece)) {
        coords.setX(3);
        coords.setY(3);
      }
    } else if (board.getBoard()[2][2] == piece && board.getBoard()[1][1] == piece) {
      if (board.getBoard()[0][0] != Piece.getPiece(piece)) {
        coords.setX(1);
        coords.setY(1);
      }
    } else if (board.getBoard()[0][2] == piece && board.getBoard()[1][1] == piece) {
      if (board.getBoard()[2][0] != Piece.getPiece(piece)) {
        coords.setX(3);
        coords.setY(1);
      }
    } else if (board.getBoard()[2][0] == piece && board.getBoard()[1][1] == piece) {
      if (board.getBoard()[0][2] != Piece.getPiece(piece)) {
        coords.setX(1);
        coords.setY(3);
      }
    } else {
      coords.setX(0);
      coords.setY(0);
    }
    System.out.println(coords.getX());
    System.out.println(coords.getY());
    return coords;
  }

}
