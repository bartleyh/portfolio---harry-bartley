package com.thehutgroup.accelerator;

import java.util.Scanner;

public class Player {
  private Board board;

  public Player(Board board) {
    this.board = board;
  }

  public Coordinate chooseMove() {
    Scanner sc = new Scanner(System.in);
    System.out.println();
    System.out.println("Enter the co-ordinates of Horizontal (1-3), in form 1: ");
    int coordinateX = sc.nextInt();
    System.out.println("Enter the co-ordinates of Verticle (1-3), in form 1: ");
    int coordinateY = sc.nextInt();
    System.out.println(" ");
    Coordinate coords = new Coordinate(coordinateX, coordinateY);
    if (validInput(coordinateX, coordinateY)) {
      while(board.isOccupied(coords.toString())) {
        chooseMove();
      }
    } else {
      System.out.println("Not a valid cell, please try again");
      chooseMove();
    }
    return coords;
  }

  public boolean validInput(int cordX, int cordY) {
    return (cordX > 0 && cordX <= 3 && cordY > 0 && cordY <= 3);
  }
}
