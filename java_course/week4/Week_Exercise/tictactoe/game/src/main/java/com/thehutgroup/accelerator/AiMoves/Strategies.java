package com.thehutgroup.accelerator.AiMoves;

import com.thehutgroup.accelerator.Coordinate;
import com.thehutgroup.accelerator.Piece;

public interface Strategies {

  public Coordinate makeMove(Piece piece);

  }