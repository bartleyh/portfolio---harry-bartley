package com.thehutgroup.accelerator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Student student1 = new Student("test1",new String[]{"Manchester","M1 123"},3.6,2.1);
        Student student2 = new Student("test2",new String[]{"Manchester","M1 456"},2.8,2.6);
        Student student3 = new Student("test3",new String[]{"Manchester","M1 789"},3.4,1.7);
        sortByCumulativeGPA cumulativeGPA = new sortByCumulativeGPA(5);
        cumulativeGPA.priorityinsertValue(student1);
        cumulativeGPA.priorityinsertValue(student2);
        cumulativeGPA.priorityinsertValue(student3);
        System.out.println(" ");
        System.out.println("Student with greatest cumulative GPA");
        cumulativeGPA.poll();

        System.out.println(" ");

        sortByCurrentYearGPA currentYearGPA = new sortByCurrentYearGPA(5);
        currentYearGPA.priorityinsertValue(student1);
        currentYearGPA.priorityinsertValue(student2);
        currentYearGPA.priorityinsertValue(student3);
        System.out.println("Student with greatest current year average GPA");
        currentYearGPA.poll();
    }
}
