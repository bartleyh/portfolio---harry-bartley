package com.thehutgroup.accelerator;

public class sortByCumulativeGPA extends Queue {

  public sortByCumulativeGPA(int size) {
    super(size);
  }

  public void priorityinsertValue(Student input) {
    Double InputcumulativeGPA = input.getCumulative_GPA();
    int i;
    if (numberOfItems == 0) {
      insertValue(input);
    } else {
      for (i = numberOfItems - 1; i >= 0; i--) {
        if (InputcumulativeGPA > contents[i].getCumulative_GPA()) {
          contents[i+1] = contents[i];
        } else break;
      }
      contents[i+1] = input;
      back++;
      numberOfItems++;
    }
  }
}