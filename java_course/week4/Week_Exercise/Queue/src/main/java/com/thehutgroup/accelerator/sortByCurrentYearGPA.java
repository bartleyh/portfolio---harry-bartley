package com.thehutgroup.accelerator;

public class sortByCurrentYearGPA extends Queue {

  public sortByCurrentYearGPA(int size) {
    super(size);
  }

  public void priorityinsertValue(Student input) {
    Double InputCurrentYearGPA = input.getGPA_For_Current_Year();
    int i;
    if (numberOfItems == 0) {
      insertValue(input);
    } else {
      for (i = numberOfItems - 1; i >= 0; i--) {
        if (InputCurrentYearGPA > contents[i].getGPA_For_Current_Year()) {
          contents[i+1] = contents[i];
        } else break;
      }
      contents[i+1] = input;
      back++;
      numberOfItems++;
    }
  }
}