package com.thehutgroup.accelerator;


import java.util.ArrayList;
import java.util.List;

public abstract class Queue<T> {
  public int queueSize;
  public List<T> contents;
  public int front, numberOfItems, back = 0;

  public Queue(int size) {
    queueSize = size;
    contents = new ArrayList<>(queueSize);
  }

  public void poll() {
    if (numberOfItems > 0) {
      System.out.println(contents.get(front).toString());
      contents.set(front, null);
      numberOfItems--;
      for (int i = 1; i < queueSize; i++) {
        contents.set(i - 1, contents.get(i));
      }
    }
  }

  public void insertValue(T input) {
    if (numberOfItems + 1 <= queueSize) {
      contents.set(back, input);
      back++;
      numberOfItems++;
    }
  }

  public abstract void PriorityInsertValue(Student input);

}
