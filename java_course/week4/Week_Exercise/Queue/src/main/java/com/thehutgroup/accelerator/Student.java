package com.thehutgroup.accelerator;

import java.util.Objects;

public class Student {
  public String Name;
  public String[] Address;
  public Double Cumulative_GPA;
  public Double GPA_For_Current_Year;

  public Student(String name, String[] address, Double cumulative_GPA, Double GPA_For_Current_Year) {
    this.Name = name;
    this.Address = address;
    this.Cumulative_GPA = cumulative_GPA;
    this.GPA_For_Current_Year = GPA_For_Current_Year;
  }

  public String getName() {
    return Name;
  }

  public void setName(String name) {
    Name = name;
  }

  public String[] getAddress() {
    return Address;
  }

  public void setAddress(String[] address) {
    Address = address;
  }

  public Double getCumulative_GPA() {
    return Cumulative_GPA;
  }

  public void setCumulative_GPA(Double cumulative_GPA) {
    Cumulative_GPA = cumulative_GPA;
  }

  public Double getGPA_For_Current_Year() {
    return GPA_For_Current_Year;
  }

  public void setGPA_For_Current_Year(Double GPA_For_Current_Year) {
    this.GPA_For_Current_Year = GPA_For_Current_Year;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof Student)) {
      return false;
    }
    Student student = (Student) o;
    return Objects.equals(Name, student.Name) &&
        Objects.equals(Address, student.Address) &&
        Objects.equals(Cumulative_GPA, student.Cumulative_GPA) &&
        Objects.equals(GPA_For_Current_Year, student.GPA_For_Current_Year);
  }

  @Override
  public int hashCode() {
    return Objects.hash(Name, Address, Cumulative_GPA, GPA_For_Current_Year);
  }

  @Override
  public String toString() {
    return "Student{" +
        "Name='" + Name + '\'' +
        ", Address='" + Address + '\'' +
        ", Cumulative_GPA=" + Cumulative_GPA +
        ", GPA_For_Current_Year=" + GPA_For_Current_Year +
        '}';
  }
}
