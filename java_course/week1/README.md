## Accelerator 2018-2019 Harry Bartley

### Week 1

- How to learn new languages -- classifying different aspects of programming languages, course objectives. Compiler overview.
- Java philosophy
- Java build/execution model -- with and without an IDE -- bytecode theory and practice, JVM, JDK vs SDK etc. Interpreting compiler errors.
  Basic syntax -- classes, methods, objects, main

- Practical involving using javac/java without IDE; inspecting bytecode
- Types -- primitives, classes and overview of memory areas
- Control statements -- for, while, switch, if (logical short-circuit)
- Using your IDE effectively -- modules, navigation, debugging, import refactoring
- Basic Debugging
