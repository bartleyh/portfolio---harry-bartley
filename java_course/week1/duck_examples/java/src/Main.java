public class Main {
    public static void main(String[] args) {
        quack(new Duck());
        quack(new DabblingDuck());
        quack(new DivingDuck());
        quack(new ImposterDuck());
    }

    public static void quack(Duck duck) {
        duck.quack();
    }
}
