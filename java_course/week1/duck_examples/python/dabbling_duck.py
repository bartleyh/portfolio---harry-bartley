from duck import Duck

class DabblingDuck(Duck):
    def dabble(self):
        print("Dabble")