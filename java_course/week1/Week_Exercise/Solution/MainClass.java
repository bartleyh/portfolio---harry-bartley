import java.io.BufferedReader;
import java.io.IOException;
import java.io.*;

public class MainClass {


    //program to find out if an iteger is greater, equal, not equal or less than another integer     
    public static void main(String[] args)throws IOException {

        Calculator myObject= new Calculator();
        
        int firstNumber = 0;
        int secondNumber = 0;
        String operation = "";
        
        BufferedReader myReader= new BufferedReader(new InputStreamReader(System.in));
        
        if(args.length == 3) {
            try {
            firstNumber = Integer.parseInt(args[0]);
            secondNumber = Integer.parseInt(args[2]);
            operation = args[1];
            } catch (NumberFormatException e) {
            System.err.println("Invalid Input: Please enter numbers.");
        }
        }

        if (operation.equals("+"))
        {
            System.out.println(myObject.add(firstNumber,secondNumber));
        }
        else  if (operation.equals("-"))
        {
            System.out.println(myObject.subtract(firstNumber,secondNumber));
        }
        else  if (operation.equals("/"))
        {
            System.out.println(myObject.divide(firstNumber,secondNumber));
        }
        else  if (operation.equals("*"))
        {
            System.out.println(myObject.multiply(firstNumber,secondNumber));
        } else {
            System.out.println(operation);
        }
        
    }
}

