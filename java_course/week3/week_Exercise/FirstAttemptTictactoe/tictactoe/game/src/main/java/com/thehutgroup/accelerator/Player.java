package com.thehutgroup.accelerator;

import java.util.Scanner;

public class Player {
  private Board board;

  public Player(Board board) {
    this.board = board;
  }

  public int[] chooseMove() {
      int[] coords = new int[2];
      Scanner sc = new Scanner(System.in);
      System.out.println();
      System.out.println("Enter the co-ordinates of Horizontal (1-3), in form 1: ");
      int coordinateX = sc.nextInt();
      System.out.println("Enter the co-ordinates of Verticle (1-3), in form 1: ");
      int coordinateY = sc.nextInt();
    if(validInput(coordinateX, coordinateY)) {
      coords[0] = coordinateX;
      coords[1] = coordinateY;
    } else {
      System.out.println("Not a valid cell, please try again");
      chooseMove();
    }
    return coords;
  }

  public int[] aiMove(Piece opponentsPiece) {
    Piece playerPiece = Piece.getPiece(opponentsPiece);
    int[] coords = new int[2];
    int random = (int) (Math.random() * 2);
//    if(winAttempt(opponentsPiece) != coords) {
//      return winAttempt(opponentsPiece);
//    } else if (winAttempt(playerPiece) != coords) {
//      return winAttempt(playerPiece);
//    }
    if(board.getBoard()[1][1] != null ) {
      if(board.getBoard()[0][0] == opponentsPiece ) {
        if(board.getBoard()[0][1] == null || board.getBoard()[1][0] == null) {
          int[][] selection = new int[][] {{0, 1}, {1, 0}};
          int[] newSelection = selection[random];
          coords[0] = newSelection[0] + 1;
          coords[1] = newSelection[1] + 1;
        }
      } else if (board.getBoard()[0][2] == opponentsPiece ) {
        int[][] selection = new int[][] {{0, 1}, {1, 2}};
        int[] newSelection = selection[random];
        coords[0] = newSelection[0]+ 1;
        coords[1] = newSelection[1]+ 1;
      } else if (board.getBoard()[2][0] == opponentsPiece ) {
        int[][] selection = new int[][] {{1, 0}, {2, 1}};
        int[] newSelection = selection[random];
        coords[0] = newSelection[0]+ 1;
        coords[1] = newSelection[1]+ 1;
      } else if (board.getBoard()[2][2] == opponentsPiece ) {
        int[][] selection = new int[][] {{1, 2}, {2, 1}};
        int[] newSelection = selection[random];
        coords[0] = newSelection[0]+ 1;
        coords[1] = newSelection[1]+ 1;
      } else {
        int randomFour = (int) (Math.random() * 4);
        int[][] selection = new int[][] {{0, 1}, {1, 0}, {1, 2}, {2, 1}};
        int[] newSelection = selection[randomFour];
        coords[0] = newSelection[0] + 1;
        coords[1] = newSelection[1] + 1;
      }
    } else {
      coords[0] = 2;
      coords[1] = 2;
    }
      System.out.println(coords[0]);
      System.out.println(coords[1]);

      if(coords[0] == 0 && coords[1] ==0) {
        int randomNine = (int) (Math.random() * 9);
        int[][] selection = new int[][] {{0, 0}, {0, 1}, {0, 2}, {1, 0}, {1, 1}, {1, 2}, {2, 0}, {2, 1}, {2, 2}};
        int[] newSelection = selection[randomNine];
        coords[0] = newSelection[0] + 1;
        coords[1] = newSelection[1] + 1;
      }

    return coords;
  }

  public int[] winAttempt(Piece piece){
    int[] coords = new int[2];
    if (board.getBoard()[0][0] == piece && board.getBoard()[0][1] == piece) {
      coords[0] = 1;
      coords[1] = 3;
    } else if (board.getBoard()[0][1] == piece && board.getBoard()[0][2] == piece) {
      coords[0] = 1;
      coords[1] = 1;
    } else if (board.getBoard()[1][0] == piece && board.getBoard()[1][1] == piece) {
      coords[0] = 2;
      coords[1] = 3;
    } else if (board.getBoard()[1][1] == piece && board.getBoard()[1][2] == piece) {
      coords[0] = 2;
      coords[1] = 1;
    } else if (board.getBoard()[2][0] == piece && board.getBoard()[2][1] == piece) {
      coords[0] = 3;
      coords[1] = 3;
    } else if (board.getBoard()[2][1] == piece && board.getBoard()[2][2] == piece) {
      coords[0] = 3;
      coords[1] = 1;
    }
    return coords;
  }

  public boolean validInput(int cordX, int cordY) {
    return (cordX > 0 && cordX <= 3 && cordY > 0 && cordY <= 3);
  }
}
