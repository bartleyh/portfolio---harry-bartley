package com.thehutgroup.accelerator;

import java.util.HashSet;
import java.util.Set;

public class Board {
  public Set<String> occupied = new HashSet<>();
  public Piece[][] board = new Piece[3][3];


  public Set<String> getOccupied() {
    return occupied;
  }

  public Boolean isOccupied(String coords) {
    return getOccupied().contains(coords);
  }

  public Piece[][] getBoard() {
    return board;
  }

  public String toString(int X, int Y) {
    return "[" + X + "," + Y + "]";
  }

  public void insertPiece(int[] coords, Piece piece, Player player, Boolean ai) {
    String newCoords = toString(coords[0], coords[1]);
    if(isOccupied(newCoords)) {
      if(ai) {
        insertPiece(player.aiMove(Piece.getPiece(piece)), piece, player, true);
      } else {
        System.out.println("Not a valid cell, please try again");
        insertPiece(player.chooseMove(), piece, player, false);
      }

    } else {
      board[coords[0]-1][coords[1]-1] = piece;
      occupied.add(newCoords);
    }
  }

  public boolean gameOver(Piece piece) {
    if(getOccupied().size() == 8) {
      System.out.println("The game was a draw!");
      return true;
    } else if (getBoard()[0][0] == piece && getBoard()[0][1] == piece && getBoard()[0][2] == piece
            || getBoard()[1][0] == piece && getBoard()[1][1] == piece && getBoard()[1][2] == piece
            || getBoard()[2][0] == piece && getBoard()[2][1] == piece && getBoard()[2][2] == piece
            || getBoard()[0][0] == piece && getBoard()[1][0] == piece && getBoard()[2][0] == piece
            || getBoard()[0][1] == piece && getBoard()[1][1] == piece && getBoard()[2][1] == piece
            || getBoard()[0][2] == piece && getBoard()[1][2] == piece && getBoard()[2][2] == piece
            || getBoard()[0][0] == piece && getBoard()[1][1] == piece && getBoard()[2][2] == piece
            || getBoard()[0][2] == piece && getBoard()[1][1] == piece && getBoard()[2][0] == piece) {
      System.out.println("Player " + piece + " won!");
      return true;
    } else return false;
  }

  public void printBoard() {
    for (int i = 0; i < 3; i++) {
      if (i != 0) {
        System.out.println("-----------");
      }
      for (int j = 0; j < 3; j++) {
        if (j == 0) {
          if(getBoard()[i][j] == null) {
            System.out.print(" " + "-" + " | ");
          } else {
            System.out.print(" " + getBoard()[i][j] + " | ");
          }
        }
        else if (j == 1) {
          if(getBoard()[i][j] == null) {
            System.out.print("-" + " | ");
          } else {
            System.out.print(getBoard()[i][j] + " | ");
          }
        } else {
          if(getBoard()[i][j] == null) {
            System.out.print("-" + " ");
          } else {
            System.out.print(getBoard()[i][j] + " ");
          }
        }
      }
      System.out.println();
    }
  }
}
