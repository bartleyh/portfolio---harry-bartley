package com.thehutgroup.accelerator;

import java.util.Scanner;

public class Run {


  public void playGame() {
    System.out.println("-----------------------");
    System.out.println("Welcome to tic-tac-toe!");
    System.out.println("-----------------------");
    Board board = new Board();
    int random = (int) (Math.random() * 2);
    Piece piece = Piece.values()[random];
    if (multiplayerOrSingleplayer()) {
      System.out.println("You are player: " + piece);
      System.out.println(" ");
      while (true) {
        System.out.println("Its your turn!");
        System.out.println(" ");
        playerTurn(board, piece);
        if (board.gameOver(piece)) {
          break;
        }
        aiTurn(board, Piece.getPiece(piece), piece);
        if (board.gameOver(piece)) {
          break;
        }
      }
    } else {
      System.out.println("Player One is : " + piece + "s");
      System.out.println(" ");
      while (true) {
        System.out.println("It is player " + piece + "s" + " turn");
        System.out.println(" ");
        playerTurn(board, piece);
        if (board.gameOver(piece)) {
          break;
        }
        piece = Piece.getPiece(piece);
      }
    }
    if (playAgain()) {
      playGame();
    }
  }

  public boolean multiplayerOrSingleplayer() {
    System.out.println();
    Scanner sc = new Scanner(System.in);
    System.out.println("For singleplayer enter s and for multiplayer enter m");
    return (sc.next().equals("s"));
  }

  public void playerTurn(Board board, Piece piece) {
    Player player = new Player(board);
    board.printBoard();
    board.insertPiece(player.chooseMove(), piece, player, false);
    board.printBoard();
    System.out.println(" ");
  }

  public boolean playAgain() {
    System.out.println(" ");
    System.out.println("Game Over");
    System.out.println(" ");
    Scanner sc = new Scanner(System.in);
    System.out.println("Would you like to play again? y/n :");
    return sc.next().equals("y");
  }


  public void aiTurn(Board board, Piece piece, Piece opponentsPiece) {
    Player player = new Player(board);
    board.insertPiece(player.aiMove(opponentsPiece), piece, player, true);
    System.out.println(" ");
  }

}
