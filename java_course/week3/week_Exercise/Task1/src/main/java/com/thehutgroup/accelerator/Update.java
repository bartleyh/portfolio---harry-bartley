package com.thehutgroup.accelerator;

import java.util.HashSet;
import java.util.Set;

public class Update extends Status {
  public Set<Coordinate> occupied = new HashSet<>();


  public Update(Player player, Coordinate currentMove, Board board) {
    super(player, currentMove, board);
  }


  public Set<Coordinate> getOccupied() {
    return occupied;
  }

  public Boolean isOccupied(Coordinate currentMove) {
    return getOccupied().contains(currentMove);
  }

  public Boolean validSpace(Coordinate currentMove) {
    return 0 < currentMove.getX() && currentMove.getX() < 4
        && 0 < currentMove.getY() && currentMove.getY() < 4;
  }

  @Override
  public void updateBoard(Coordinate currentMove, Player player, Board board) {
    System.out.println(getOccupied());
    if (validSpace(currentMove) && !isOccupied(currentMove)) {
      getOccupied().add(currentMove);
      System.out.println(getOccupied());
      board.newBoard(currentMove, player);
    } else {
      System.out.println("Not a valid cell, please try again");
//      Move move = new Move(board);
//      move.placePiece(move.showBoard(player));
    }
  }
}
