package com.thehutgroup.accelerator;

abstract class Status {
  public Player player;
  private Coordinate currentMove;
  private Board board;


  public Status(Player player, Coordinate currentMove, Board board) {
    this.player = player;
    this.currentMove = currentMove;
    this.board = board;

  }

  public Player getPlayer() {
    return this.player;
  }

  public abstract void updateBoard(Coordinate currentMove, Player player, Board board);
}
