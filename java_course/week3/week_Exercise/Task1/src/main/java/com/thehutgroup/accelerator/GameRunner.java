package com.thehutgroup.accelerator;

import java.util.Scanner;

public class GameRunner {

  public GameRunner() {
  }

  public Board getNewBoard() {
    Board board = new Board();
    return board;
  }

  public Player start() {
    System.out.println("-----------------------");
    System.out.println("Welcome to tic-tac-toe!");
    System.out.println("-----------------------");
    int random = (int) (Math.random() * 2);
    Player currentPlayer = Player.values()[random];
    System.out.println("You are player: " + currentPlayer);
    System.out.println(" ");
    return currentPlayer;
  }

  public Player newPlayer(Player currentPlayer) {
    return Player.getPlayer(currentPlayer);
  }

  public void run(Player currentPlayer, boolean newGame) {
    Move move = new Move(getNewBoard());
    move.placePiece(move.showBoard(currentPlayer, newGame));
    rerun(newPlayer(currentPlayer), getNewBoard(), move);
  }

  public void rerun(Player currentPlayer, Board board, Move move) {
    Boolean newGame = false;
    move.placePiece(move.showBoard(currentPlayer, newGame));
    if (!board.winGame(currentPlayer)) {
      rerun(newPlayer(currentPlayer), board, move);
    } else {
      board.buildBoard();
      System.out.println("Player " + currentPlayer + " won!");
      Scanner sc = new Scanner(System.in);
      System.out.println();
      System.out.println("Would you like to play again? Type in y or n: ");
      if (sc.next().equals("Y") || sc.next().equals("y")) {
        run(start(), true);
      } else {
        System.out.println("GAME OVER");
      }
    }
  }
}
