package com.thehutgroup.accelerator;

import java.lang.reflect.Array;
import java.util.*;


public class Move {
  private Player currentPlayer;
  private Board board;
  private Update update = new Update(currentPlayer, null, board);

  public Move(Board board) {
    this.board = board;
  }

  public void setBoard(Board board) {
    this.board = board;
  }

  public Board getBoard() {
    return board;
  }


  public Player showBoard(Player currentPlayer, Boolean newGame) {
    System.out.println("It is player: " + currentPlayer + " turn");
    System.out.println();
    if (newGame) {
      Board board = new Board();
      setBoard(board);
    }
    getBoard().buildBoard();
    return currentPlayer;
  }

  public Boolean gameOver() {
    return false;
  }

  public void placePiece(Player currentPlayer) {
    Scanner sc = new Scanner(System.in);
    System.out.println();
    System.out.println("Enter the co-ordinates of Horizontal (1-3), in form 1: ");
    int coordinateX = sc.nextInt();
    System.out.println("Enter the co-ordinates of Verticle (1-3), in form 1: ");
    int coordinateY = sc.nextInt();
    Coordinate coordinate = new Coordinate(coordinateX, coordinateY);
    System.out.println("Confirm move is " + (coordinate) + " Y/N: ");
    if (sc.next().equals("N") || sc.next().equals("n")  ) {
      placePiece(showBoard(currentPlayer, false));
    } else {
      update.updateBoard(coordinate, currentPlayer, getBoard());
    }
  }


}
