package com.thehutgroup.accelerator;

public enum Player {
  X,
  O;

  public static Player getPlayer(Player player) {
    switch (player) {
      case X:
        return O;
      case O:
        return X;
      default:
        throw new RuntimeException("Invalid player");
    }
  }

}