package com.thehutgroup.accelerator;

public class Board {
  private String[][] board = new String[][] {{" -", "-", "-"}, {" -", "-", "-"}, {" -", "-", "-"}};

  public String[][] newBoard(Coordinate currentMove, Player player) {
    int coordinateX = currentMove.getX();
    int coordinateY = currentMove.getY();
    if (coordinateY == 1) {
      board[coordinateX - 1][coordinateY - 1] = " " + player.toString();
    } else {
      board[coordinateX - 1][coordinateY - 1] = player.toString();
    }
    return board;
  }

  public String[][] blankBoard() {
    board = new String[][] {{" -", "-", "-"}, {" -", "-", "-"}, {" -", "-", "-"}};
    return board;
  }

  public boolean winGame(Player player) {
    if (board[0][0].equals(" " + player.toString()) && board[1][0].equals(" " + player.toString()) && board[2][0].equals(" " + player.toString())
        || board[0][1].equals(player.toString()) && board[1][1].equals(player.toString()) && board[2][1].equals(player.toString())
        || board[0][2].equals(player.toString()) && board[1][2].equals(player.toString()) && board[2][2].equals(player.toString())
        || board[0][0].equals(" " + player.toString()) && board[0][1].equals(player.toString()) && board[0][2].equals(player.toString())
        || board[1][0].equals(" " + player.toString()) && board[1][1].equals(player.toString()) && board[1][2].equals(player.toString())
        || board[2][0].equals(" " + player.toString()) && board[2][1].equals(player.toString()) && board[2][2].equals(player.toString())
        || board[0][0].equals(" " + player.toString()) && board[1][1].equals(player.toString()) && board[2][2].equals(player.toString())
        || board[0][2].equals(player.toString()) && board[1][1].equals(player.toString()) && board[2][0].equals(" " + player.toString())
    ) {
      System.out.println("match");
      return true;
    } else {
      return false;
    }

  }

  public String[][] buildBoard() {
    for (int i = 0; i < board.length; i++) {
      if (i != 0) {
        System.out.println("-----------");
      }

      for (int j = 0; j < board[i].length; j++) {
        if (j != 2) {
          System.out.print(board[i][j] + " | ");
        } else {
          System.out.print(board[i][j] + "  ");
        }
      }
      System.out.println();
    }
    return board;
  }
}
