## Accelerator 2018-2019 Harry Bartley

### Week 3

- Data Structures: Lists, ArrayLists, Hashing, Queues, Maps
- GRASP and design principles for metrices like cohesion and coupling, open close principle, controller, MVC etc.
- Most used creational, structural and behavioural patterns and their Implementation (builder, factory, singleton etc)
