public class Triangle implements ComplexShapes {

    private double base;
    private double perpend;
    private double hypo;

    public Triangle(double a, double b, double c) {
        this.base = a;
        this.perpend = b;
        this.hypo = c;

    }

    @Override
    public double area() {
        return (base * perpend) / 2;
    }

    @Override
    public double perimeter() {
        return base + perpend + hypo;
    }

    @Override
    public void drawShape() {
        System.out.println("Triangle with area " + area() + " and perimeter " + perimeter());
    }
}
