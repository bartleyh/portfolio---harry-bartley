## Accelerator 2018-2019 Harry Bartley

### Week 5

- Dependencies, build (gradle, maven, artifactory)
- Dependency injection
- Practical -- app with manual DI, then with a few sample frameworks
