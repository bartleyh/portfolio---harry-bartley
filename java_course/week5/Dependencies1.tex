

\documentclass{beamer}
\makeatletter
\setlength\beamer@paperwidth{16.00cm}%
\setlength\beamer@paperheight{9.00cm}%
\geometry{papersize={\beamer@paperwidth,\beamer@paperheight}}
\makeatother
 
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{color}

\usepackage{graphicx}
\graphicspath{ {./images/} }

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ 
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
	basicstyle=\footnotesize,        % the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{mygreen},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,	                   % adds a frame around the code
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{blue},       % keyword style
	language=Octave,                 % the language of the code
	morekeywords={*,...},            % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
	rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=false,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{mymauve},     % string literal style
	tabsize=2,	                   % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}
 
%Information to be included in the title page:
\title{Java Course}
\subtitle{Dependencies, Builds and Software Workflows}
\author{Shaun Hall}
\institute{THG Accelerator}
\date{2019}
 
 
 
\begin{document}
 \lstset{language=Java}
\frame{\titlepage}
 
\begin{frame}
\frametitle{Definition of terms}
\begin{itemize}
	\item \textbf{Dependency}: logical reliance 
	\begin{itemize}
		\item \textbf{Dependency management}: controlling metadata describing code you depend on. Implementing the retrieval of dependencies.
		\item \textbf{Dependency injection}: a software engineering design pattern where the dependencies of an object are passed by the client.
	\end{itemize}
\item \textbf{Build}: a standalone artifact that can be executed.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Dependency Management in Java}
\includegraphics[scale=0.8]{maven}
\includegraphics[scale=0.5]{gradle}
\end{frame}

\begin{frame}
\frametitle{Maven Theory}
\begin{itemize}
	\item Simple, opinionated, inflexible build processes $\implies$ consistent use across all projects
	\item \textbf{Lifecycles} consist of \textbf{phases} which consist of \textbf{goals}
	\item The file \textbf{pom.xml} defines the build configuration.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Maven Command Line Syntax}
\begin{lstlisting}[language=bash]
mvn (phase|goal)+
\end{lstlisting}
e.g.
\begin{lstlisting}[language=bash]
mvn clean install
\end{lstlisting}
\end{frame}

\begin{frame}
\frametitle{The \emph{default} Maven Lifecycle}
The following phases, in this order
\begin{itemize}
	\item validate - validate the project is correct and all necessary information is available
	\item compile - compile the source code of the project
	\item test - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
	\item package - take the compiled code and package it in its distributable format, such as a JAR.
	\item verify - run any checks on results of integration tests to ensure quality criteria are met
	\item install - install the package into the local repository, for use as a dependency in other projects locally
	\item deploy - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Maven Phase Execution}
When you ask for a phase to be executed, all the previous phases in the lifecycle are executed first:
\begin{lstlisting}[language=bash]
mvn install
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]
\frametitle{Sections in a pom.xml}
\begin{lstlisting}[language=xml, basicstyle=\tiny]
<project xmlns=...>
	<modelVersion>4.0.0</modelVersion>
	<packaging>jar</packaging>

	<groupId>com.thehutgroup.accelerator</groupId>
	<artifactId>my-project</artifactId>
	<version>v</version>
	
	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>27.0-jre</version>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin>
				<plugin config>
			</plugin>
		</plugins>
	</build>
</project>
\end{lstlisting}

\end{frame}

\begin{frame}
\frametitle{Maven Practical}
\end{frame}

\begin{frame}
\frametitle{Gradle Theory}
\begin{itemize}
	\item Gradle is a general purpose build tool (not just for Java)
	\item The unit of execution is tasks; tasks can depend on other tasks
	\item It is highly flexible -- you can define your own tasks easily
	\item Build config is written in the Groovy language (or Kotlin)
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Gradle Practical}
Follow the instructions in \href{https://gitlab.io.thehut.local/Accelerator18/course-material/java-course/blob/master/week5/gradle-demo/README.md}{java-course/week5/gradle-demo/README.md} 
\end{frame}

\begin{frame}
\frametitle{Advanced Maven and Software Workflows}
Here is an example of a software workflow -- we will show how this can be implemented with maven.
\end{frame}

	
\end{document}

\end{document}
