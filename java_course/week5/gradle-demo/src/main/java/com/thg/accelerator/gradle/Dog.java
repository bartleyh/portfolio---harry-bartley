package com.thg.accelerator.gradle;

import java.util.Objects;

public class Dog {
    private String breed;

    public Dog(String breed) {
        this.breed = breed;
    }

    public void bark() {
        System.out.println("woof from breed " + breed);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return Objects.equals(breed, dog.breed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(breed);
    }
}
