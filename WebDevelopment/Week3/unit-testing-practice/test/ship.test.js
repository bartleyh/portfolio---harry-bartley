const ship = require("../main/ship.js");

describe("Ship factory function", () => {
  test("should be a function", () => {
    let testship = ship("ship");
    expect(testship).toBeDefined();
  });
  test("should have a name and length", () => {
    let testship = ship("ship", 4);
    expect(testship.shipName()).toEqual("ship");
    expect(testship.shipLength()).toEqual(4);
  });
  test("should be able to hit the ship", () => {
    let testship = ship("ship", 4);
    expect(testship.hit(1)).toEqual(true);
    expect(testship.hit(4)).toEqual(true);
    expect(testship.hit(5)).toEqual(false);
    expect(testship.hit(1)).toEqual(false);
  });
  test("should be able to sink the ship", () => {
    let testship = ship("ship", 4);
    expect(testship.isSunk()).toEqual(false);
    expect(testship.hit(1)).toEqual(true);
    expect(testship.hit(2)).toEqual(true);
    expect(testship.hit(3)).toEqual(true);
    expect(testship.hit(4)).toEqual(true);
    expect(testship.isSunk()).toEqual(true);
  });
});
