const player = require("../main/player.js");

describe("player factory function ", () => {
  test("should be a function", () => {
    let testPlayer = player("1");
    expect(testPlayer).toBeDefined();
  });
  test("should be able to attack coordinates", () => {
    let testPlayer = player("1");
    let testPlayer2 = player("2");
    expect(testPlayer.attack([2, 3])).toEqual("23");
    expect(testPlayer.attack([2, 3])).toEqual(false);
    expect(testPlayer.attack([2, 6])).toEqual("26");
    expect(testPlayer.attack([2, 6])).toEqual(false);
    expect(testPlayer.attack([10, 6])).toEqual(false);
    expect(testPlayer2.attack([2, 3])).toEqual("23");
  });
  test("should be able to attack coordinates using random coordinates", () => {
    let testPlayer = player("1");
    let positionX = Math.floor(Math.random() * 10);
    let positionY = Math.floor(Math.random() * 10);
    let coord = [positionX, positionY];
    expect(testPlayer.attack(coord)).toEqual(coord[0] + "" + coord[1]);
  });
});
