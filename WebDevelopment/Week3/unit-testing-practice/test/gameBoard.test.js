const ship = require("../main/ship.js");
const gameBoard = require("../main/gameBoard.js");

describe("gameBoard factory function", () => {
  test("should be a function", () => {
    let testBoard = gameBoard();
    expect(testBoard).toBeDefined();
  });
  test("should be able to place a ship", () => {
    let testShip1 = ship("test1", 4);
    let testShip2 = ship("test2", 6);
    let testShip3 = ship("test3", 4);
    let testBoard = gameBoard();
    expect(testBoard.placeShip(23, testShip1, false)).toEqual(true);
    expect(testBoard.placeShip(27, testShip2, false)).toEqual(true);
    expect(testBoard.placeShip(53, testShip3, false)).toEqual(false);
  });
  test("should be able to recieve an attack", () => {
    let testShip1 = ship("test1", 4);
    let testShip2 = ship("test2", 6);
    let testShip3 = ship("test3", 4);
    let testBoard = gameBoard();
    expect(testBoard.placeShip(23, testShip1, false)).toEqual(true);
    expect(testBoard.placeShip(92, testShip2, true)).toEqual(true);
    expect(testBoard.receiveAttack(23)).toEqual(true);
    expect(testBoard.receiveAttack(11)).toEqual(false);
    expect(testShip1.hits[0]).toEqual(true);
  });

  test("should be able to check if all ships have sunk", () => {
    let testShip1 = ship("test1", 2);
    let testBoard = gameBoard();
    expect(testBoard.placeShip(23, testShip1, false)).toEqual(true);
    expect(testBoard.receiveAttack(22)).toEqual(false);
    expect(testBoard.receiveAttack(23)).toEqual(true);
    expect(testBoard.receiveAttack(33)).toEqual(true);
    expect(testBoard.checkAllSunk()).toEqual(true);
  });
});
