// const domControl = require("./domControl.js");

// let drawGrid = domControl();
// drawGrid.createGrid();
const rows = 10;
const col = 10;
let canvas = document.getElementById("grid");
let ctx = canvas.getContext("2d");
for (let r = 0; r < rows; r++) {
  for (let c = 0; c < col; c++) {
    ctx.fillStyle = "red";
    ctx.strokeStyle = "red";
    ctx.fillRect(10 * rows, 10 * col, 10, 10);
  }
}
