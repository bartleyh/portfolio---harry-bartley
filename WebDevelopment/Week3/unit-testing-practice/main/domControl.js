const gameBoard = require("./gameBoard.js");

const domControl = () => {
  const rows = 10;
  const col = 10;
  const createGrid = () => {
    var canvas = document.getElementById("grid");
    var ctx = canvas.getContext("2d");
    ctx.canvas.width = w;
    ctx.canvas.height = h;
    for (let r = 0; r < rows; r++) {
      for (let c = 0; c < col; c++) {
        ctx.fillStyle = "red";
        ctx.strokeStyle = "red";
        ctx.fillRect(10 * rows, 10 * col, 10, 10);
      }
    }
  };

  return { createGrid };
};
module.exports = domControl;
