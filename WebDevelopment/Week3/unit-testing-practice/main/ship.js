const ship = (name, length) => {
  const shipName = () => name;
  const shipLength = () => length;

  const hits = new Array(length).fill(false);

  const hit = num => {
    num = num - 1;
    if (num >= 0 && num < length) {
      if (hits[num] == false) {
        hits[num] = true;
        return true;
      } else return false;
    } else return false;
  };

  const isSunk = () => {
    for (let i = 0; i < length; i++) {
      if (hits[i] == false) {
        return false;
      }
    }
    return true;
  };
  return { shipName, shipLength, hit, isSunk, hits };
};

module.exports = ship;
