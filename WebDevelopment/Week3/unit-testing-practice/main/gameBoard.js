const ship = require("./ship.js");

const gameBoard = () => {
  const boardDimensions = 10;
  let shipList = [];
  let filledCells = [];
  let missedShots = [];

  const checkAllSunk = () => {
    if (shipList.length == 0) {
      return true;
    } else return false;
  };

  const checkValid = (positionX, positionY) => {
    if (
      positionX - 1 >= 0 &&
      positionX < boardDimensions &&
      positionY - 1 >= 0 &&
      positionY < boardDimensions
    ) {
      return true;
    } else return false;
  };

  // false is horizontal, true is verticle
  const placeShip = (coord, ship, rotate) => {
    positionX = Math.floor(coord / 10);
    positionY = coord % 10;

    tempCoordsList = [];

    for (let i = 0; i < ship.shipLength(); i++) {
      if (checkValid(positionX, positionY)) {
        if (filledCells.includes(positionX + "" + positionY)) {
          return false;
        } else {
          tempCoordsList.push(positionX + "" + positionY);
          if (rotate === false) {
            positionX = positionX + 1;
          } else {
            positionY = positionY + 1;
          }
        }
      } else return false;
    }
    for (let j = 0; j < tempCoordsList.length; j++) {
      filledCells.push(tempCoordsList[j]);
    }
    const newShipAdded = {
      object: ship,
      ListofPos: tempCoordsList
    };

    shipList.push(newShipAdded);
    return true;
  };

  const receiveAttack = coord => {
    positionX = Math.floor(coord / 10);
    positionY = coord % 10;
    if (checkValid(positionX, positionY)) {
      if (filledCells.includes(positionX + "" + positionY)) {
        for (let i = 0; i < shipList.length; i++) {
          if (shipList[i].ListofPos.includes(positionX + "" + positionY)) {
            shipList[i].object.hit(
              shipList[i].ListofPos.indexOf(positionX + "" + positionY) + 1
            );
          }
          if (shipList[i].object.isSunk()) {
            let index = shipList.indexOf(shipList[i]);
            shipList = shipList.splice(index, shipList[i]);
          }
        }
        checkAllSunk();
        return true;
      } else {
        missedShots.push(positionX + "" + positionY);
        return false;
      }
    } else return false;
  };
  return { placeShip, receiveAttack, checkAllSunk };
};
module.exports = gameBoard;
