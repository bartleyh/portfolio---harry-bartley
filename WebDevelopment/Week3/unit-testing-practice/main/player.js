const player = name => {
  const moves = [];

  const attack = coord => {
    newCoord = coord[0] + "" + coord[1];
    if (moves.includes(newCoord)) {
      return false;
    }
    if (checkValid(coord[0], coord[1])) {
      moves.push(newCoord);
      return newCoord;
    }
    return false;
  };
  const randomMove = () => {
    let positionX = Math.floor(Math.random() * 10);
    let positionY = Math.floor(Math.random() * 10);
    let coord = [positionX, positionY];
    if (moves.includes(coord[0] + "" + coord[1])) {
      randomMove();
    }
    return attack(coord);
  };
  const checkValid = (positionX, positionY) => {
    if (positionX >= 0 && positionX < 10 && positionY >= 0 && positionY < 10) {
      return true;
    } else return false;
  };
  return { name, attack, randomMove };
};

module.exports = player;
