(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
const gameBoard = require("./gameBoard.js");

const domControl = () => {
  const rows = 10;
  const col = 10;
  const createGrid = () => {
    var canvas = document.getElementById("grid");
    var ctx = canvas.getContext("2d");
    ctx.canvas.width = w;
    ctx.canvas.height = h;
    for (let r = 0; r < rows; r++) {
      for (let c = 0; c < col; c++) {
        ctx.fillStyle = "red";
        ctx.strokeStyle = "red";
        ctx.fillRect(10 * rows, 10 * col, 10, 10);
      }
    }
  };

  return { createGrid };
};
module.exports = domControl;

},{"./gameBoard.js":2}],2:[function(require,module,exports){
const ship = require("./ship.js");

const gameBoard = () => {
  const boardDimensions = 10;
  let shipList = [];
  let filledCells = [];
  let missedShots = [];

  const checkAllSunk = () => {
    if (shipList.length == 0) {
      return true;
    } else return false;
  };

  const checkValid = (positionX, positionY) => {
    if (
      positionX - 1 >= 0 &&
      positionX < boardDimensions &&
      positionY - 1 >= 0 &&
      positionY < boardDimensions
    ) {
      return true;
    } else return false;
  };

  // false is horizontal, true is verticle
  const placeShip = (coord, ship, rotate) => {
    positionX = Math.floor(coord / 10);
    positionY = coord % 10;

    tempCoordsList = [];

    for (let i = 0; i < ship.shipLength(); i++) {
      if (checkValid(positionX, positionY)) {
        if (filledCells.includes(positionX + "" + positionY)) {
          return false;
        } else {
          tempCoordsList.push(positionX + "" + positionY);
          if (rotate === false) {
            positionX = positionX + 1;
          } else {
            positionY = positionY + 1;
          }
        }
      } else return false;
    }
    for (let j = 0; j < tempCoordsList.length; j++) {
      filledCells.push(tempCoordsList[j]);
    }
    const newShipAdded = {
      object: ship,
      ListofPos: tempCoordsList
    };

    shipList.push(newShipAdded);
    return true;
  };

  const receiveAttack = coord => {
    positionX = Math.floor(coord / 10);
    positionY = coord % 10;
    if (checkValid(positionX, positionY)) {
      if (filledCells.includes(positionX + "" + positionY)) {
        for (let i = 0; i < shipList.length; i++) {
          if (shipList[i].ListofPos.includes(positionX + "" + positionY)) {
            shipList[i].object.hit(
              shipList[i].ListofPos.indexOf(positionX + "" + positionY) + 1
            );
          }
          if (shipList[i].object.isSunk()) {
            let index = shipList.indexOf(shipList[i]);
            shipList = shipList.splice(index, shipList[i]);
          }
        }
        checkAllSunk();
        return true;
      } else {
        missedShots.push(positionX + "" + positionY);
        return false;
      }
    } else return false;
  };
  return { placeShip, receiveAttack, checkAllSunk };
};
module.exports = gameBoard;

},{"./ship.js":3}],3:[function(require,module,exports){
const ship = (name, length) => {
  const shipName = () => name;
  const shipLength = () => length;

  const hits = new Array(length).fill(false);

  const hit = num => {
    num = num - 1;
    if (num >= 0 && num < length) {
      if (hits[num] == false) {
        hits[num] = true;
        return true;
      } else return false;
    } else return false;
  };

  const isSunk = () => {
    for (let i = 0; i < length; i++) {
      if (hits[i] == false) {
        return false;
      }
    }
    return true;
  };
  return { shipName, shipLength, hit, isSunk, hits };
};

module.exports = ship;

},{}]},{},[1]);
