const stringFunctions = require("../main/stringFunctions.js");

describe("stringFunctions", () => {
  beforeEach(() => {
    let string = new stringFunctions("hello");
    return string;
  });

  test("should capitilise first letter of string", () => {
    let string = new stringFunctions("hello");
    expect(string.capitalise()).toEqual("Hello");
  });

  test("should capitilise first letter of string", () => {
    let string = new stringFunctions("hello");
    expect(string.reverseString()).toEqual("olleh");
  });
});
