class stringFunctions {
  constructor(word) {
    this.word = word;
  }

  capitalise() {
    let capitalised = this.word;
    return capitalised.charAt(0).toUpperCase() + capitalised.slice(1);
  }

  reverseString() {
    let string = [];
    for (let i = this.word.length; i >= 0; i--) {
      string.push(this.word.charAt(i));
    }
    return string.join("");
  }
}

module.exports = stringFunctions;
