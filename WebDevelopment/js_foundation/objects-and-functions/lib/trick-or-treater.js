class TrickOrTreater {
  constructor(costume) {
    this.dressedUpAs = costume.style;
    this.bag = [];
    this.hasCandy = false;
    this.countCandies = 0;
  }

  putCandyInBag(candy) {
    this.hasCandy = true;
    this.bag.push(candy.type);
    this.countCandies += 1;
  }

  eat() {
    this.countCandies -= 1;
    this.bag.pop(this.bag[0]);
  }
}

module.exports = TrickOrTreater;
