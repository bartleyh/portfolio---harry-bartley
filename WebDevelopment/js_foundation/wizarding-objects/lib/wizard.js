class Wizard {
  constructor(name) {
    this.name = name;
    this.petsCount = 0;
    this.pets = [];
    this.tempList = [];
  }

  givePet(pet) {
    this.petsCount += 1;
    this.pets.push(pet);
  }

  petList() {
    for (let i = 0; i < this.pets.length; i++) {
      this.tempList.push(this.pets[i].name + " a " + this.pets[i].type);
    }
    return this.tempList;
  }
}
module.exports = Wizard;
