class Hobbit {
  constructor(name) {
    this.name = name;
    this.disposition = "homebody";
    this.age = 0;
    this.adult = false;
    this.isShort = true;
    this.old = false;
    this.hasRing = false;
    this.hasRing = true ? this.name == "Frodo" : (this.hasRing = false);
  }

  celebrateBirthday() {
    this.age++;
    this.age <= 32 ? (this.adult = false) : (this.adult = true);
    this.age >= 101 ? (this.old = true) : (this.old = false);
  }
}

module.exports = Hobbit;
