class Dragon {
  constructor(name, rider, color) {
    this.name = name;
    this.rider = rider;
    this.color = color;
    this.hungry = true;
    this.count = 0;
  }

  eat() {
    this.count++;
    return this.count % 3 == 0 ? (this.hungry = false) : (this.hungry = true);
  }
}

module.exports = Dragon;
