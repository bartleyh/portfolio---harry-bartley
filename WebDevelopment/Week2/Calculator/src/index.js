let inputScreen = [];
let operatorSymbol = "";
let result = 0;

function standard() {
  inputScreen = [];
  operatorSymbol = "";
  document.getElementById("screen").innerHTML = 0;
}

function input(int) {
  if (inputScreen.length >= 1 && operatorSymbol != "") {
    if (
      operatorSymbol == "+" ||
      operatorSymbol == "X" ||
      operatorSymbol == "-" ||
      operatorSymbol == "/"
    ) {
      if (inputScreen.length == 2) {
        number = inputScreen[1];
        inputScreen.pop();
        int = number.toString() + int.toString();
        int = parseInt(int);
        inputScreen.push(int);
        document.getElementById("screen").innerHTML = int;
      } else {
        inputScreen.push(int);
        document.getElementById("screen").innerHTML = int;
      }
    } else {
      inputScreen.shift();
      inputScreen.push(int);
      document.getElementById("screen").innerHTML = int;
    }
  } else {
    operatorSymbol = "";
    if (inputScreen.length != 0) {
      number = inputScreen[0];
      inputScreen.shift();
      int = number.toString() + int.toString();
      int = parseInt(int);
      inputScreen.push(int);
    } else {
      inputScreen.push(int);
    }
    document.getElementById("screen").innerHTML = int;
  }
}

function Operator() {
  if (inputScreen == []) {
    standard();
  }
  num1 = inputScreen[0];
  num2 = inputScreen[1];
  if (operatorSymbol == "+") {
    let operate = new Add(num1, num2);
    document.getElementById("screen").innerHTML = operate.run();
    result = operate.run();
  } else if (operatorSymbol == "-") {
    let operate = new Subtract(num1, num2);
    document.getElementById("screen").innerHTML = operate.run();
    result = operate.run();
  } else if (operatorSymbol == "/") {
    let operate = new Divide(num1, num2);
    document.getElementById("screen").innerHTML = operate.run();
    result = operate.run();
  } else if (operatorSymbol == "X") {
    let operate = new Multiply(num1, num2);
    document.getElementById("screen").innerHTML = operate.run();
    result = operate.run();
  }
  inputScreen = [];
}

class Add {
  constructor(num1, num2) {
    this.num1 = num1;
    this.num2 = num2;
  }
  run() {
    return this.num1 + this.num2;
  }
}

class Subtract {
  constructor(num1, num2) {
    this.num1 = num1;
    this.num2 = num2;
  }
  run() {
    return this.num1 - this.num2;
  }
}

class Divide {
  constructor(num1, num2) {
    this.num1 = num1;
    this.num2 = num2;
  }
  run() {
    return this.num1 / this.num2;
  }
}

class Multiply {
  constructor(num1, num2) {
    this.num1 = num1;
    this.num2 = num2;
  }
  run() {
    return this.num1 * this.num2;
  }
}
