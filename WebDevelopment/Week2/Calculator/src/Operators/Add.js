class Add {
  constructor(num1, num2) {
    this.num1 = num1;
    this.num2 = num2;
  }
  run() {
    return this.num1 + this.num2;
  }
}

module.exports = Add;
