class Bag {
  constructor() {
    this.empty = true;
    this.count = 0;
    this.candies = [];
  }

  push(candy) {
    this.candies.push(candy);
    this.count += 1;
  }

  contains(sweets) {
    for (let i = 0; i < this.candies.length; i++) {
      if (this.candies[i].type == sweets) {
        return true;
      }
    }
    return false;
  }
}

module.exports = Bag;
