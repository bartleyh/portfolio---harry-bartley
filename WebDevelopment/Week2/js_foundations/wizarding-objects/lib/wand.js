class Wand {
  constructor(type, size, core) {
    this.type = type;
    this.size = size;
    this.core = core;
  }

  cast(string) {
    return "Casting " + string + "!";
  }
}

module.exports = Wand;
