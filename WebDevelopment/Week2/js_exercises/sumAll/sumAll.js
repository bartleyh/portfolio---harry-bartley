const sumAll = function(min, max) {
  let total = 0;
  if (
    min !== NaN &&
    min >= 0 &&
    max !== NaN &&
    max >= 0 &&
    typeof max !== "string" &&
    typeof min !== "string"
  ) {
    if (min > max) {
      for (i = max; i <= min; i++) {
        total += i;
      }
    } else {
      for (i = min; i <= max; i++) {
        total += i;
      }
    }
    return total;
  } else {
    return "ERROR";
  }
};

module.exports = sumAll;
