## Accelerator 2018-2019 Harry Bartley

### Basic js exercises and testing

• Calculator makes use of basic DOM manipulation with some css styling

• js_exercises includes using numerous js methods to produce some classic beginner programs

• js_foundations uses jest to test a number of classes running basic functions
