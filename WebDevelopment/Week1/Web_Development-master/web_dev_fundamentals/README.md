# Web Development Core Curriculum 

**Workshops:**

[An Introduction to the Frontend](wd1.md)

**Lecture Slides:**

[Introduction to HTML and CSS](1_HTML_CSS.pdf)

[Advanced CSS I](2_CSS.pdf)

[Advanced CSS II](3_selectors_CSS.pdf)

[Introduction to JS](Introduction_to_JS.pdf)

[Advanced JS](Advanced_JS.pdf)

**Projects:**

*Below are the projects from Term One with a web development focus.*

[Build the Google Home Page](https://gitlab.com/thgaccelerator/team-projects/blob/master/Challenge_Day_2/flask.md)

[Build your own Blog](https://gitlab.com/thgaccelerator/team-projects/tree/master/Challenge_4)

**Recomended Independent Learning Resources:**

[The Modern Javascript Tutorial](https://javascript.info)

[Introdcution to JS](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript)




