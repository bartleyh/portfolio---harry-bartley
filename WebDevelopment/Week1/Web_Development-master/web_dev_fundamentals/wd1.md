# Web Development: An Introduction

An Introduction to the Front-End | HTML and CSS Foundations | Developer Tools

## An Introduction to the Front-end


### Introduction 

The “front-end languages" live in the browser. After you type an address into the address bar at the top and hit the enter/return key, your browser will receive at least an HTML file from the web server. That file will likely tell the browser to request a CSS file and a JavaScript file as well (probably many more than one, but we’ll keep it simple).

Each of these languages performs a separate but very important function and they work harmoniously together to determine how the web page is STRUCTURED (HTML), how it LOOKS (CSS), and how it FUNCTIONS (JavaScript). And keep in mind that your browser handles figuring out how to make these files into a functioning web page (not the server).

### Learning Outcomes

Take a look at the points below, you should use these to test yourself after having completed all the learning tasks: 

- What is the role of HTML in a web page? 
- What is the role of CSS in a web page? 
- What is the role of JavaScript in a web page? 

### Tasks

**1.  Let's get a high level overview of how HTML, CSS and JavaScript work together using MDN's 'Getting Started' guide. You should (as a minimum) read:** 
    - [Dealing with files](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/Dealing_with_files)
    - [HTML basics](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics)
    - [CSS basics](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/CSS_basics)
    - [JavaScript basics](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/CSS_basics)

_There is no need to implement the website; simply read about the process at this point._


**2. Familiarize yourself with these documentation pages: [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML/Element), [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference#Keyword_index), [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference), and [jQuery](http://api.jquery.com/).**

_You will not understand most of what is there just yet, but bookmark the pages for later reference._


**Additional Reading:**

- Get an  introduction to front-end development from this [interview](https://generalassemb.ly/blog/what-is-front-end-web-development/)




-------------

## HTML and CSS foundations

### Introduction

**HTML** is the markup that contains all the actual content that a web page has. Go to a website of your choice, right click on any of the elements on the page and select "Inspect" to open your browser's Developer Tools and it will reveal the structure of the page. 

**CSS** tells the browser if you want to display any of those tags a particular way, for instance, turning its background blue and pushing it a little to the left. In your Developer Tools, you can see the CSS styles in another panel, usually showing which specific properties were inherited from which lines of CSS.

_You can read a little more about CSS [here](https://skillcrush.com/2012/04/03/css/)_

### Learning Outcomes
Refer back to these as you complete the learning tasks:

- Why do we separate HTML and CSS?
- What are classes and IDs (and how are they different)?
- What are elements?
- What are tags?
- What are attributes?
- What are forms?
- What is a div?
- What are selectors?
- What are properties?
- What are values?
- What is the “query string” in a URL and what does it do?
- What is the difference between “pixels” and “ems”?
- How do CSS styles for a particular element get inherited? i.e. how does an element get its “default” styles?
- What are two CSS attributes you can change to push an element around on the page?
- What are the three different ways to include CSS in your project or use CSS to style a particular element?
- What is the “default stylesheet” or “user agent stylesheet”?
- Why use a CSS reset file?

**Suggested Resources and tasks:**

- Do the [Codeacademy HTML Course](https://www.codecademy.com/learn/learn-html) and the [Codeacademy CSS course](https://www.codecademy.com/learn/learn-css)

- Consolidate this knowledge and fill in the gaps with the [HTML Dog beginner HTML tutorial](http://www.htmldog.com/guides/html/beginner/), the [HTML Dog beginner CSS tutorial](http://www.htmldog.com/guides/css/beginner/) and the [HTML Dog Intermediate CSS tutorial](http://www.htmldog.com/guides/css/intermediate/)

- Learn about basic forms from this video from [Treehouse](https://teamtreehouse.com/library/html/forms/inputs). You can also use [this page](https://www.w3schools.com/html/html_forms.asp) from W3 as a reference


------------

## Developer Tools

### Introduction

All of the most popular modern web browsers come with a suite of tools designed to assist with the development of websites. You may see these referred to as the web inspector. These tools will enable you to "inspect" the HTML, CSS and JavaScript of any website you visit (or build). 

Knowing how to use your browser's developer tools effciently is an indispensable web development skill. 

### Learning Outcomes 

Make sure you can do the following before moving on: 

- You can open developer tools in your browser. 
- You know how to select a specific HTML element on you page using your browser's developer tools. 
- You can use developer tools to change the CSS on specific elements of a web page. 

### Tasks

Skim through the official documentation for [Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/?utm_source=dcc&utm_medium=redirect&utm_campaign=2018Q2). The most useful sections will probably be: 

-  [Overview](https://developers.google.com/web/tools/chrome-devtools/)
- [Getting started with viewing and changing CSS](https://developers.google.com/web/tools/chrome-devtools/css/)
- [CSS Reference](https://developers.google.com/web/tools/chrome-devtools/css/reference)
- [View and change HTML](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/)


-----------
# Project

For this project you will be utilising everything you have just learned to deconstruct an existing website and rebuild it. 

Build the page out in a `.html` file and open it in your browser. 

Before starting, you may wish to make sure you know how to do the following as it will make it easier for you once you start to build: 

1. Two ways to move a `div` around on the page
2. Identify the background colour of an existing webpage
3. Take the URL of an image from an existing webpage
4. Center an element horizontally
5. Understand how to use classes and ids to target CSS at specific elements on the page. 
6. Build a very basic form (it doesn't need to "go" anywhere)

### The Assignment:

**Build the Google homepage**

1. Start with putting the main elements on the page (ie. the logo image and search form), then position them appropriately within the page. 

2. You can either download the Google logo or link directly to it's URL on the web in your `<img>` tag. 

3. Next you should implement the Navbar across the top. 

4. Once you are happy with your likeness, ensure your project is pushed to Github and share it. 

----------------

### Optional (Advanced) version:

**Build the Google search results page**

This project should enable you to resuse much of your code from the homepage project. 

Don't worry about the fact that your links and forms don't "go" anywhere, just concentrate on the order of the items on the page. 

*You will notice that all the classes and ids that you inspect on Google's homepage are nonsensical strings. This is because the code has been 'minified'. This removes or shortens unnecessary characters and names to help the page load faster* 


