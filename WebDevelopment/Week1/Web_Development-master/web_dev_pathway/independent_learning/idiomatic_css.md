# Idiomatic CSS

Your guide to CSS etiquette and maintaining a degree of sanity whilst writing large volumes of CSS.

# Generally…

* Establish a consistent style for the code-base and reinforce it on the team. Any code-base should look like a single-person typed it, even when there are multiple contributers.
* Choose existing, common style patterns first, employing deviations only when necessary and for good reason.

# Whitespace…

* Use whitespace to improve readability.
* Choose between soft indents (spaces) or real tabs. Stick to your choice without fail. (Preference: spaces)
* Choose the number of characters used per indentation level. (Preference: 2 spaces)
* Configure your editor to “show invisibles” or to automatically remove end-of-line whitespace.

# Format…

* Choose a code format that is easy to read; easy to clearly comment; minimizes the chance of accidentally introducing errors; and results in useful diffs and blames.
* Use one discrete selector per line in multi-selector rulesets.
* Include a single space before the opening brace of a ruleset.
* Include one declaration per line in a declaration block.
* Use one level of indentation for each declaration.
* Include a single space after the colon of a declaration.
* Use lowercase and shorthand hex values, e.g., #aaa.
* Use single or double quotes consistently. Preference is for double quotes, e.g., content: "".
* Quote attribute values in selectors, e.g., input[type="checkbox"].
* Where allowed, avoid specifying units for zero-values, e.g., margin: 0.
* Include a space after each comma in comma-separated property or function values.
* Include a semi-colon at the end of the last declaration in a declaration block.
* Place the closing brace of a ruleset in the same column as the first character of the ruleset.
* Separate each ruleset by a blank line.

```css
.selector-1,
.selector-2,
.selector-3[type="text"] {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    display: block;
    font-family: helvetica, arial, sans-serif;
    color: #333;
    background: #fff;
    background: linear-gradient(#fff, rgba(0, 0, 0, 0.8));
}

.selector-a,
.selector-b {
    padding: 10px;
}

```
# Declaration Order…

* Order declarations in accordance with a single, simple principle.
* Examples include: order alphabetically, and/or cluster related properties (e.g. positioning and box-model)

# Comments…
* Create a comment style that is simple and consistent.
* Place comments on a new line above their subject.
* Use comments when needed, not as a crutch to explain everything you are doing. Your code should largely speak for itself.