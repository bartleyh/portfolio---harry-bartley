# Independent Learning :mortar_board:

[CSS Performance and Optimization](performance.md)

[Image Handling](image_handling.md)

### Resources

[exercism.io](exercism.md)
_You can use exercism.io in 'Mentored Mode' or 'Independent Mode'. Both will be valuable in developing and consolidating your JavaScript Knowledge_



