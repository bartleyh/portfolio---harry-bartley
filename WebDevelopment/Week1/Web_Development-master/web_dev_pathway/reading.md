# Reading List :books:

_Some recommended reading material to supplement the Web Development Pathway._


- [HTML and CSS: Design and Build Websites](https://www.amazon.co.uk/HTML-CSS-Design-Build-Websites/dp/1118008189/ref=as_li_ss_tl?ie=UTF8&qid=1459879147&sr=8-3&keywords=duckett&linkCode=sl1&tag=stekineduteca-20&linkId=8a59a11365e1b70fb01e9114d135c479)
- [Scalable and Modern Architecture for CSS](https://smacss.com/)
- [JavaScript and JQuery: Interactive Front-end Web Development](https://www.amazon.co.uk/JavaScript-JQuery-Interactive-Front-End-Development/dp/1118531647/ref=as_li_ss_tl?s=books&ie=UTF8&qid=1457549440&sr=1-3&linkCode=sl1&tag=stekineduteca-20&linkId=d2d2d78cb207ec06b1f10f5f2d06fd4f)
- [Eloquent JavaScript](https://eloquentjavascript.net/)
- [Don't Make me Think](https://www.amazon.co.uk/Dont-Make-Me-Think-Usability/dp/0321344758)
- [Field Guide to Human Centred Design](http://www.designkit.org/resources/1)
- [Pro React](http://www.pro-react.com/)