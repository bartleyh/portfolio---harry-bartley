# Web Development Pathway

Below is a high level breadown of the Web Development content for Term 2 of the Accelerator. 

It's possible that we may deviate from the structure and content as the course progresses. The scheduling and the content will be flexible and determined by the speed at which those on the pathway progress. 


[Week One](./core_curriculum/week_1)

* HTML: Structure, Semantics and FPO
* CSS: Structure, the Box Model, Positioning
* Introduction to Responsive Layouts
* Reset v Normalize CSS Files
* Introduction to Flexbox
* Introduction to Grid
* CSS Transitions, Transformations and Animations

[Week Two](./core_curriculum/week_2)

JavaScript Recap: 

* Data Types, Variables, Conditionals and Functions
* Scope, Arrays and For Loops
* Introduction to DOM manipulation
* Objects, 'this' and constructor functions
* localStorage and JSON
* Debugging JavaScript
* Event Bubbling and Delegation

[Week Three](./core_curriculum/week_3)

* Introduction to Unit Testing JavaScript 
* Introduction to Array Prototype Methods 
* Array Prototype Iteration Methods 
* What is `this`?

[Week Four](./core_curriculum/week_4)

**CSS:** 

* Introduction to Sass 

 **JavaScript:**  

* Data Structures and Types in JavaScript 
* Object Oriented Programming in JavaScript 
* More on the keyword `this` 
* Scope and Closures

[Week Five](./core_curriculum/week_5)

* Test Driven Development with Jest
* ES6 Fundamentals  
* Promises 
* Async/Await 
* ES6 Generators  
* ES6 Spread and Rest Operators 
* ES6 Destructuring

[Week Six](./core_curriculum/week_7)

* Introduction to Babel
* Introduction to React 
* Introduction to React Stateful Components 
* React Component Communication

[Week Seven](./core_curriculum/week_7)

* Advanced React and Redux
    * React lifecycle methods and Proptypes
    * Unit testing react with Create React App, Jest and Enzyme
    * Testing Async methods and API calls with React
* Accessibility
* Client-side Web APIs

[Week Eight](./core_curriculum/week_8)

* Advanced React and Redux Continued
    * Starting with Redux
    * Testing Redux
    * React Router 4
* Building a Boiler Plate
* MVC Architecture

[Week Nine](./core_curriculum/week_9)

* Server-Side JavaScript and APIs
    * Introduction to Node.js
    * Express Fundamentals
    * Server-side Testing
    * Deploying a Node App to Heroku
    * Express Middleware
* Introduction to OAUth

[Week Ten](./core_curriculum/week_10) 

* Build Tools and Workflow
    - Connecting the Backend to the Frontend
    - Continuous Integration

[Week Eleven]()

_Being kept free for Final Projects._

