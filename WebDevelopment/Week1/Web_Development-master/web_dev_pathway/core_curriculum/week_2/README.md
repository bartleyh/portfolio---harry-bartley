# Week Two 

_Again, some of the content from the pages outlined below will already be familiar to you. You are free to run through the content at your own speed but you must demonstrate your knowledge and understanding by completing the requisite exercises._


JavaScript Recap: 

* Data Types, Variables, Conditionals and Functions
* Scope, Arrays and For Loops
* Introduction to DOM manipulation
* Objects, 'this' and constructor functions
* localStorage and JSON
* Debugging JavaScript
* Event Bubbling and Delegation

As you work through the above tutorials, you should also look to complete the following tasks: 

- [JavaScript Foundations](https://gitlab.com/thgaccelerator/js_foundations)

You can take your time to go through the tutorials before demonstrating your knowledge through the exercises **or** you can jump straight to the challenges and use the resources if and when you need them. 

