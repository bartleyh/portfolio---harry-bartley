# WebDev: Week One

## Contents: 

_Some of the content from the pages outlined below will already be familiar to you. You are free to run through the content at your own speed but you must demonstrate your knowledge and understanding by completing the requisite exercises. It is recommended that you progress through the content in the order in which it has been listed._

* [HTML: Structure, Semantics and FPO](html.md)
    - [Recap](html_recap.md)
    - [HTML Challenge](challenges/html_challenge.md)

* [CSS: Structure, the Box Model, Positioning](css.md)
    - [CSS Recap](css-recap.md)
    - [CSS Challenge](challenges/css_challenge.md)

* [Introduction to Responsive Layouts](responsive_layouts.md)
    - [Responsive Layout Tutorial](responsive_layouts)
    - [Responsive Layouts Challenge](challenges/responsive_layouts.md) 

* [Reset v Normalize CSS Files](reset.md)

* [Introduction to Flexbox](challenges/flexbox)

* [Introduction to Grid](grid.md)

* [Cloning Netflix](challenges/netflix_clone)

* [CSS Transitions, Transformations and Animations](transitions.md)

**Weekly Challenge:**

To keep you ticking over until next week - and to unleash the power of your new CSS powers - take the [CSS Art Attack Challenge](challenges/art_attack.md)

## Week One Task Checklist

The following tasks are expected to be completed (at the latest) by Thursday morning of week 2: 

- [ ] [HTML Challenge](challenges/html_challenge.md)  
- [ ] [CSS Challenge](challenges/css_challenge.md)  
- [ ] [Responsive Layout Challenge](challenges/responsive_layouts.md)  
- [ ] [Netflix Clone](challenges/netflix_clone)  
- [ ] [CSS Animations Tutorial and Challenges](transitions.md)

- [ ] [Art Attack Challenge](challenges/art_attack.md)  



