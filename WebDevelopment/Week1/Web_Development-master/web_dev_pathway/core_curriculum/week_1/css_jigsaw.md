# CSS Jigsaw

### Learning Goals

* Understand the differences between reset and normalize css files
* Learn how to write consistent and idiomatic CSS with an understanding of why it is important
* Explore and understand how to implement a variety of image optimisation methods
* Understand the key issues in crafting CSS that can impact performance
* Explore current industry patterns for organizing CSS files, understanding their differences and pros/cons
* Learn the difference in CSS transitions, transformations, animations and how to implement various interactions utilizing all of them

# What is a 'Jigsaw Lesson'?

Today, you will be starting as we mean to go on, by creating your own learning. We will be dividing you up into "learning groups" for the session. Within these groups, you will collaborate on some research of some assigned subjects and then you will share your findings with the rest of the group. 

The structure for the process is as follows: 

### Wave One :ocean: : Research (30 mins)

In this session your goal is to *“build expertise”*. Gather into your “Research Groups” specified in the outline. During this time your group is collaborating to put together a collection of notes / key understandings such as:

* What is this thing?
* Why is it useful?
* How do you put it into action?
* Put together a short demo (less than 3 minutes)
* How can it affect your development process?
* Are there any downsides to using it?
* Where can a person go to learn more about it? Any resources seem better than others?

Some additional requirements to note:

_There are specific questions per content area below to guide the group’s research. The expecation is that your presentations for session 3 cover the key questions provided per content area._
_Each person from the “Research Group” needs to finish session 1 with their own set of notes and their own demo ready to go._

### Break (5 mins)

### Wave Two :ocean: : "Expert" Presentations (10 mins each)

Get back together in your “Learning Groups” specified in the outline. 

A representative from each of the research groups will take turns sharing their findings from the respective content areas as follows:

* Clear, concise definition(s) of the subject matter and its importance
* Concrete example(s) of when to use, employ, consider
* Any concerns and/or things to watch out for
* Create a short demo-able code example to share
* Optional/additional resources for independent study


**While they’re sharing, the rest of the group should:**

* Take their own notes
* Ask clarifying questions
* Brainstorm/record any depth questions the researcher is not able to answer

### Wave Three :ocean: : 

It's time to feedback and highlight anything that remains unclear so that we can make sure to circle back around to them during a later session. 


## Pointers for Research Groups: 

### Reset v. Normalize CSS Files & Idiomatic CSS

* Start with the [Reset and Normalize](reset.md) lesson
* What are the differences between Reset v Normalize files and when and why would they matter to FE Devs? 
* Who are the creators of normalize (and why should we care about this?)
* Anything else of interest to note here? (_Think about output of the CSS, bug issues, performance, documentation, maintainability, legacy and future support, etc._)
* Check out the [idiomatic CSS](../../independent_learning/idiomatic_css.md) content
* What is idiomatic CSS and why is it important? 
* What are some options in the organisation of properties per CSS declaration? 
* Take a peek at [this](https://www.wired.com/2012/06/write-better-css-with-idiomatic-css/) article and see if there is anything else of interest you should add to your list. 

### Image Optimisation

* Start with the [Introduction to Image Handling](../../independent_learning/image_handling.md) lesson from the Front End Curriculum independent study section. 
* What are the different image types and what are they best used for? Can you provide us with some visual examples? 
* What is the lowest hanging fruit available to you as a dev to optimise image file size? How does it work? Can you brave a demonstration to the group?
* What is a sprite? How does it work?
* Create a code demo with images to implement and explain html's `srcset` and `sizes` attributes available on the image tag. 
* Anything else of interest to note? 

### Performance and Organisation
* Start with the [Performance and Organization](../../independent_learning/performance.md) page of the Independent Study guide. 
* Revisit the concept of [specificity](css.md)
* How does the browser handle a conflicting CSS rule? 
* When writing CSS, what sorts of things does a front end dev need to consider for performance (remember - always want things to read/render/perform FASTER)
* Are there any good “rules of thumb” to remember as you construct your CSS?
* Someday… you will have LOT’s of CSS to organize. Multiple files even! What are some organization patterns that exist in the industry? Give a high-level overview of each, focusing on basic principles, how they compare, pros/cons. (Hint: BEM, SMACSS, Object Oriented CSS, Atomic CSS)
* Why is a system of organization important at all?
* Anything else of interest to note? 

### Transitions, Transformations and Animations
* Start with the [CSS Transitions, Transformations and Animations lesson](transitions.md)
* Articulate the differences between transitions, transformations, and animations.
* Are there accessibility concerns with CSS animations? What are they and what can we do about it?
* Create your own code pen examples of various transitions, transformations, and at least one multi-stage animation (goal: 5 code pens, different from the lesson provided above)
* Anything else of interest to note? 






