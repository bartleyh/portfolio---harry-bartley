# HTML: A Recap

#### Some Useful Vocabulary
* `HTML` HyperText Markup Language
* `CSS`Cascading Style Sheet
* `CSS` Property The name of a display property of an HTML element (e.g., color, border)
* `HTML Element` A building block that makes up the structure of a web page
* `HTML Tag` Used to create HTML elements. Some elements have an opening and closing tag, others only have an opening tag.
* `Id / Class` Ways to identify HTML elements
* `Attribute` Additional values that configure HTML elements and adjust their behavior
* `Hyperlink` A reference to an external resource
* `Block` A block-level element occupies the entire width of its parent element (container), thereby creating a “block.”
* `Inline` An inline-level element only occupies the space bounded by the tags defining the element, instead of breaking the flow of the content.

### What is HTML?
* HTML is used to create electronic documents (pages) that are displayed on the Web
* Each page contains a series of connections to other pages called hyperlinks
* HTML ensures the proper formatting of content (text, images, video) so that your internet browser can display them as intended.
* HTML is made up of many Elements
* Elements are used to hold our content and define how the browser must format and display the content.
* Elements are created with either one or two tags.
* Tags are created with angle brackets <> and are used to create elements
* Most elements consist of an opening and closing tag which wraps content (like text)
* Markup = the set of tags used to structure a page

## 1. Anatomy of a Tag

### Elements

Elements are created with one or more tags. They are used to describe and hold our content.

Elements which are created with only one tag are called empty elements and cannot have any child elements. Examples of this are `<img> and <input>`.

Elements which can contain child elements are created with an opening and closing tag which surround the child elements and/or text content. `<h1>Text Content</h1>`

### Example

Let’s say that we had some text and we wanted to denote that this text was a paragraph.

```This is an example paragraph. We should probably place this inside of a tag. If we place it in a tag it will be easier to access and style.```

We’d wrap the text in paragraph tags.

```<p>This is an example paragraph. We should probably place this inside of a tag. If we place it in a tag it will be easier to access and style.</p>```

We use `<p>` to signal to the browser that everything that’s about to follow is part of a paragraph and `</p>` to let the browser know that this paragraph is done. When a user visits our application, the browser loads up the HTML and parses it into the elements that will eventually make up our user interface.

### Check your understanding

Before moving on, make sure you can answer the following: 

* What is an HTML tag?  
* What is an element?  
* What makes an HTML element different that an HTML tag?  
* What is the difference between a “regular” element and an self-closing, or empty, element?  


## 2. Required Structure of an HTML page

Every page that is built with HTML needs to have the four same elements to start:

* `<!doctype html>` declaration: The doctype declaration is not an HTML tag, but rather tells the browser which version of HTML the page is written in.
    * [W3C DTD Docs](https://www.w3.org/QA/2002/04/valid-dtd-list.html)
* `<html></html>` tag wraps the entire document
* `<head></head>` tag wraps elements that shouldn’t be rendered: information about the page and how to process it
* `<body></body>` tag wraps elements that should be displayed: the actual content

Take a look at [this codepen](https://codepen.io/AnnaHollandSmith/pen/XoZJxB) to see an example of the structure outlined above.


### Check your understanding

* What are the required elements in an HTML page?
* Why do we need to include the `<!doctype html>` declaration?
* What type of content goes in the `<head>` tag?
* What type of content goes in the `<body>` tag?

## 3. Containing Elements, Semantics & Text

HTML5 has a variety of semantic tags, or HTML tags that provide additional meaning through descriptive naming, available for us to use. These tags are an easy way to not only make our code more understandable and clear to other developers (and our future selves), but they are also a great way to incorporate basic accessibility into your HTML for users who may need to access your website in non-traditional ways.

Experiment with the following semantic tags in codepen:

* `header`
* `footer`
* `h1 - h6`
* `section`
* `article`
* `p`
* `ul and ol`
* `div`

## 4. Block and Inline elements

You may have noticed that some tags behave a little differently in a layout than others. Some tags make content stack, while others let content sit next to each other. What’s that all about?

**This is an important distinction and is worth some investigation:**

* Block elements stack on top of each other. Each one starts and ends on its own line.
* Inline elements can be used to mark up a few words inside of a block element.

Most elements are block elements. Some common inline tags you might see in the wild:

* `<em>` is used to denote that you’d like to emphasize some text.
* `<strong>` is used to denote that this text is important.

We use `<em>` and `<strong>` to denote the semantic meaning of the content.

Take a look at this [example on Codepen](https://codepen.io/AnnaHollandSmith/pen/XoZbKO)

You may notice that the `<em>` tags are italicized and the `<strong>` tags are displayed in bold. The browser does this by default. That said, you should still only use these tags to convey meaning. We can change the way stuff looks later with CSS.

#### "span" and "div"

All of the tags discussed above have some kind of semantic meaning. Assistive technology devices will use them to help people with visual impairments understand the page. Search engines will use them to figure out the structure of your page. You should use semantic HTML tags whenever possible and appropriate.

Sometimes, however, you don’t want a tag to have any meaning. Typically, this is when you just want to target a certain portion of the page with CSS or JavaScript and none of the semantic tags really apply.

**Disclaimer:** There are many, many more semantic tags than the ones we've discussed here.
Think of `<span>` and `<div>` as the base layer of HTML tags, they don’t have any meaning in and of themselves and they typically don’t come with any built-in styling from the browser.

There is just one important difference between the two.

* `<div>` is a block element.
* `<span>` is an inline element.

We’ll discuss these in more detail over in the CSS recap.

## 5. Images and Attributes
Now that we better understand why elements decide to sit where they do on a page, let’s talk about two types of inline HTML elements that make our pages more interactive and beautiful!

We use HTML tags to mark up text to show its semantic meaning. The browser uses these tags to structure the document. As we talked about earlier in this lesson, most tags have an opening and closing tag, but a few do not. For example, images—defined using the `<img>` tag do not have a closing tag.

**Note:** Elements which do not have closing tags and cannot have child elements are called empty elements.

Consider the following example:

```<img src="https://www.thehive-network.com/wp-content/uploads/2018/10/the-hut-group-thg-logo-vector.png">```

Our browser is more than happy to load up an image, but we need to tell it where that image is located. Our `<img>` tag needs extra information to know which image to display. That’s where the `src` attribute comes in.

## 6. Hyperlinks

Another important tag is the `<a>` tag. These are the tags we use for creating hyperlinks. You might have noticed that the `<a>` tag behaves a little differently than the `<h1>`, `<h2>`, and `<p>` tags. We can use the `<a>` tag to mark up a few words, while the other tags denote a big section-let’s call it a “block”—of our page.

Consider the following example:

```<p>
  Welcome to <a href="https://www.thgaccelerator.com/">THG Accelerator</a>
</p>
```
In this case, the `<a>` tag needs to know which url it should be linked to. We use the href attribute to set the links destination. href is an abbreviation for “hypertext reference.”

# A challenge 

We've covered a lot of ground so far, but there is something we haven't discussed above and that's `<table>`

And – surprise! – we’re not going to do it together: This time it's all on you -you’re going to test out your Googling skills!

You will learn by building the table below: 

## Create this table

![simple table](./challenges/images/flags.jpg)

You can make use of codepen to create your own html file or set it up as your own project locally, it's up to you. 

**Helpful Hint:** Start by researching the [table element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table) in HTML. 

# Some useful Docs: 

[MDN HTML Overview](https://developer.mozilla.org/en-US/docs/Web/HTML)  
[MDN HTML Reference](https://developer.mozilla.org/en-US/docs/Web/HTML/Reference)

## 8. Forms 

So far, we’ve done an excellent job of displaying information to the user, but we haven’t really asked them for their input. HTML also includes a set of elements for building forms. Forms are an important part of a website. They allow users to send data to the web site. Most of the time that data is sent to the web server, but the web page can also intercept it to use it on its own.

**Docs**
HTML Guide and form structure

[MDN HTML Forms Guide](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms)  
[MDN How to Structure an HTML Form](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/How_to_structure_an_HTML_form)

### Form basics and buttons

There is a lot to forms that we’ll go more into depth with in a moment, but to start we’ll focus on two elements:

* `<input>` creates an input field. `<input>` is like `<img>` in that it does not require or support a closing tag. It can take an optional type attribute that helps validate user input in some browsers.
* `<button>` creates a button. `<button>` on the other hand does support a closing tag.

Take a look at an example in [codepen](https://codepen.io/AnnaHollandSmith/pen/REQWKM)

### Forms: Next Level
Basic input and button elements are a great starting point, but to build a truly usable form we need to use the following base elements:

* form  
* label  
* input  
* submit  

Take a look at this [codepen](https://codepen.io/AnnaHollandSmith/pen/aPajXX) for an example. 

# A  Challenge 

```html
<form action="/my-handling-form-page" method="post">
    <div>
        <label for="name">Name:</label>
        <input type="text" id="name" name="user_name" />
    </div>
    <div>
        <label for="mail">E-mail:</label>
        <input id="mail" name="user_mail" />
    </div>
    <div>
        <label for="msg">Message:</label>
        <textarea id="msg" name="user_message"></textarea>
    </div>
    
    <div class="button">
        <button type="submit">Send your message</button>
    </div>
</form>
```

Copy the code from above into your own Pen and refactor to make the following changes: 

* Validate for email type
* Add a set of radio buttons with at least three options - only allowing one to be selected at a time
* Include placeholders for name, email, and message
* Add a drop down for favorite color with at least three options
* Use an input for submit instead of a button
