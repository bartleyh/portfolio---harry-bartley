# An Introduction to CSS Animations and Transitions

You will remember that the three building blocks of front end development include **structure (HTML)**, **presentation (CSS)** and **behaviour (JavaScript)**. However, it turns out that CSS has some pretty fancy "behaviour tricks" available to us. Through CSS transitions, transformations, and animations you can create quite a bit of movement on your page without the need for any substantial JavaScript.

Take a look at some examples of these in the sites below: 

* [Type Terms](https://www.supremo.co.uk/typeterms/)

* [Movie Posters](http://demo.marcofolio.net/3d_animation_css3/)

* [Coke Can](http://www.romancortes.com/ficheros/css-coke.html)

#### Some Definitions: 

* **CSS Transitions:** CSS transitions allow property changes in CSS values to occur smoothly over a specified duration of time.
* **CSS Transformations:** By modifying the coordinate space, CSS transforms change the shape and position of the affected content without disrupting the normal document flow.
* **CSS Animations:** For more involved movement, we can use CSS animations rather than transitions and transformations. What’s the difference? A transition is typically a state change that goes from A to B (like a :hover state), while an animation may have many stages from A to B to C to D.

#### The Docs

- [MDN - CSS Transitions](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Transitions/Using_CSS_transitions)
- [Animatable CSS Properties](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_animated_properties)
- [CSS Tricks - Transform](https://css-tricks.com/almanac/properties/t/transform/)
- [Using CSS Animations](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations)
- [CSS Tricks - Animation](https://css-tricks.com/almanac/properties/a/animation/)

## Practice

It's time to put it all into practice. You are welcome to pair programme throughout these challenges. 

Once again, we will be using Codepen, but feel free to keep and save a copy of your code to your Gitlab profile. 

## Codepen One

Go ahead and set up a div with the following style: 

```css
 background-color: yellow;
  border-radius: 30px;
  height: 300px;
  width: 300px;
  margin: 0 auto;
```

1. First, create a [pseudo class](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes) of `hover`. On hover, the div should transition background color on a 2s delay. (Hint: `transition-property` & `transition-duration` may be helful to you here)
2. What happens with the transition when you move your cursor off the div?
3. Move the transition properties out of the hover state and onto the base div. What is different now when you move your cursor off the block?
4. You can chain properties together that you want to transition. Change the `background-color` and `border-radius` at the same time, but set them to different duration speeds. The border of the box should transition from 30px to 0.
5. Assuming you have successfully completed steps 1-4, now delete the transition-property line of code. Does your animation still work? Why?

Just this once, here's a [completed Codepen](https://codepen.io/AnnaHollandSmith/pen/REJXPg) to check your work.

## Codepen Two

Setup:

```html
<div class="container">
  <div class="box"></div>
</div>
```

```css
.container {
  border: 1px solid grey;
  height: 400px;
  width: 90%;
  margin: 0 auto;
  padding: 15px;
}

.box {
  background-color: yellow;
  border-radius: 30px;
  height: 300px;
  width: 25%;
}
```

* Create a hover state for the CONTAINER whereby the background color of the BOX changes to green.
* Make the box move on hover by adding `margin-left: 75%.`
* It’s a stark transition. Smooth it out with a `1s` transition that works when the user moves their cursor onto the container, and also works when they move their cursor off of the container.
* Experiment with the following three values with the transition-timing-function on the container:
    - `transition-timing-function: ease;`
    - `transition-timing-function: steps(4, end);`
    - `transition-timing-function: cubic-bezier(1, -1, 1, .5);`

# Codepen Three

Setup: 

```html
<div class="container">
  <div class="box"></div>
</div>
```

```css
.container {
  border: 1px solid grey;
  height: 400px;
  width: 90%;
  margin: 0 auto;
  padding: 15px;
}

.box {
  background-color: yellow;
  border-top: 5px solid red;
  height: 95px;
  width: 100px;
  -webkit-transform: rotate(45deg);
}
```
* Rotate the box counter-clockwise 45 degrees
* What happens when you pass rotate an argument of .5turn or .75turn or 2turn?
You can control the origin from which the transformation initiates. Add `-webkit-transform-origin: 100% 100%;` to the hover state of your container and then experiment with changing the x and y values of the property. What do you notice by using 0 for both x and y, versus using 50% for both x and y?
* Implement scale and skew transformations

# Codepen Four

Setup: 

```html
<div></div>
```

```css
div {
  background: yellow;
  height: 200px;
  width: 200px;
  margin: 100px auto 0;
}

@keyframes testAnimation {
  0% {
    transform: rotate(360deg);
    border-radius: 50% 0 50% 0;
  }
  100% {
    background-color: aqua;
    border-radius: 0 50% 0 50%;
  }
}
```

* What do you think the testAnimation will do?
* Connect the defined keyframes to the div
* Create a 3s duration for the animation
* Create a zero animation delay
* Create an infinite iteration count
* Create a linear timing function
* Set the animation direction to alternate