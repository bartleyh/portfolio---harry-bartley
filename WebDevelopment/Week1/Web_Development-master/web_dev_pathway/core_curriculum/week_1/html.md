# HTML: Structure, Semantics and FPO

Some of this content may be little more than a recap to you at this point. Feel free to run through it quickly but be sure to complete the [HTML challenges](challenges/html_challenge.md) and read the section on FPO before moving on. Some of you may want to check your understanding from term 1 and run through the [recap](html_recap.md) content too. 

# Overview

The front-end of the web is based on three major technologies:

**HTML** aka **“STRUCTURE”**: HyperText Markup Language (HTML) defines the structure and semantics of web pages on the web.  
**CSS aka “PRESENTATION”**: Cascading Style Sheets (CSS) sets the look and style of a web page. CSS provides style to the structure provided by HTML.  
**JavaScript** aka **“BEHAVIOUR”**: JavaScript allows us to define interaction in our pages.


#### Some Useful Vocabulary
* `HTML` HyperText Markup Language
* `CSS`Cascading Style Sheet
* `CSS` Property The name of a display property of an HTML element (e.g., color, border)
* `HTML Element` A building block that makes up the structure of a web page
* `HTML Tag` Used to create HTML elements. Some elements have an opening and closing tag, others only have an opening tag.
* `Id / Class` Ways to identify HTML elements
* `Attribute` Additional values that configure HTML elements and adjust their behavior
* `Hyperlink` A reference to an external resource
* `Block` A block-level element occupies the entire width of its parent element (container), thereby creating a “block.”
* `Inline` An inline-level element only occupies the space bounded by the tags defining the element, instead of breaking the flow of the content.

### What is HTML?
* HTML is used to create electronic documents (pages) that are displayed on the Web
* Each page contains a series of connections to other pages called hyperlinks
* HTML ensures the proper formatting of content (text, images, video) so that your internet browser can display them as intended.
* HTML is made up of many Elements
* Elements are used to hold our content and define how the browser must format and display the content.
* Elements are created with either one or two tags.
* Tags are created with angle brackets <> and are used to create elements
* Most elements consist of an opening and closing tag which wraps content (like text)
* Markup = the set of tags used to structure a page

You should have covered HTML structure during term one. In need of a refresher? Head over to the [HTML recap](html_recap.md) page to cover everything you need to know.

If you are happy enough with your knowledge of HTML, it's time to **show what you know** with a couple of [HTML challenges](challenges/html_challenge.md)

# FPO

FPO stands for "For Placement Only". FPO options act as a placeholder for content used in designing layouts. Throughout this course, and your career, you may be required to build interfaces before you have or know the content. In such cases, you can use FPO content. There are a number of options available for you to use including text copy, images, and video. Here are a few common sources to get you started: 

[Meet the Ipsums](https://meettheipsums.com/)  
[FPOImg](http://fpoimg.com/)  
[Lorem Pixel](http://lorempixel.com/)  
[Unsplash](https://unsplash.com/)  
[SampleVideos](https://www.sample-videos.com/)  

### Some useful additional resources

[W3C Markup Validator](https://validator.w3.org/#validate_by_uri): _allows you to check HTML and XHTML documents for well-formed markup. Markup validation is an important step towards ensuring the technical quality of web pages._


