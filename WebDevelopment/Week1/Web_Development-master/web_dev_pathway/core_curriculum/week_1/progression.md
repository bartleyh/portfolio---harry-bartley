# Skill Progression: HTML and CSS

## HTML

#### Accessibility

Level | Expectation
--- | ---
Novice | 	Knows what a semantic tag is and leverages the HTML5 structural tags to organize the content of their markup.
Advanced Beginner |	Leverages more precise semantic tags when applicable, and employs basic ARIA roles attributes for added clarity in structure, descriptive image alt attributes, title attributes for applicable anchor tags.
Proficient |  Employs detailed accessibility practices throughout markup, especially in forms and can speak to decisions made in accessibility choices as it relates to specific accessibility concerns.
Exceptional	 |  Can run markup through a variety of online accessibility tools and score well for content, color, screen readers, etc.

#### Style

Level | Expectation
--- | ---
Novice | Crafts markup with proper indentation and opening/closing tags.
Advanced Beginner |	Crafts markup with proper and logical nesting.
Proficient | Crafts lean, efficient markup that is easy to read and follow across naming conventions, structure, and solid formatting that follows industry best practices.
Exceptional	| Crafts lean, efficient markup and can speak directly to choices made to improve performance, including but not limited to, page load times, css/js optimizations, image optimizations.


## CSS

#### Structure of Code

Level | Expectation
---|---
Novice	| Can effectively target DOM elements via tag, class, and/or id and write CSS rules around each element to create the desired style.
Advanced Beginner |	Can cleanly and logically organize CSS rules according to similar categories (i.e. typography, layout, components), and then logically organize the remaining CSS rules based on flow of the markup. Organizes properties within rules alphabetically.
Proficient |	Leverages cascading styles and CSS specificity rules to create more complex targeting of elements in order to reduce, reuse, share styles across elements. Organizes properties within rules based upon industry standard principles of writing consistent, idiomatic CSS.
Exceptional |	Understands the performance implications surrounding cascading/specificity and crafts CSS that is mindful of reducing complexity and increasing performance.


#### Implementation

Level | Expectation
---|---
Novice | Can articulate how the CSS box model works and apply it appropriately in a static layout.
Advanced Beginner |	Can articulate the differences between the approaches of absolute/relative positioning, flex-box, floats, and can appropriately apply the approaches to solve a variety of layout problems.
Proficient | Develops layouts that work cross-browser, are responsive, and can logically defend the choices made in implementation approach for layout.
Exceptional	| Can articulate rationale for all parts of the CSS implementation (each line of code/CSS rule) specifically in regards to the balance of: structure of code, design integrity, performance.
