# CSS: Structure, Box Model and Positioning

This segment is all about the presentation layer of the front-end web. We will take some time to accomplish the following: 

* Consoidate our knowledge and understanding of the basics of CSS, and experiment with some of the major properties for both aestetic and layout purposes. 
* Build (another) small static site and style our markup using some fancy CSS. 

But first, take a look at [CSS Zen Garden](http://www.csszengarden.com/) to appreciate the power of CSS. As you click through the pages, remember that they all have the same markup (HTML) - they have had their different personalities injected through the use of CSS alone. 

### Some (quick) vocab revision: 

* `CSS` Cascading Style Sheets
* `CSS` property The name of a display property of an HTML element (e.g., color, border)
* `HTML` element A building block that makes up the structure of a web page
* `tag` Usually used interchangeably with ‘HTML element’
* `id / class` Ways to identify HTML elements

# What is CSS? 

It’s a “style sheet language” that lets you style the HTML elements on your page. CSS works with HTML, but isn’t HTML. CSS controls the positioning, sizing, colors, and specific fonts on your page. There is a class and id attribute available to use on every html element. In addition to the plain old element names themselves, these attributes allow you to create “targets” for both your css and javascript. They are hooks so that you can manipulate the look and behavior of your HTML elements.

You should be familiar with the foundations of CSS, but if you fancy a recap, we've got you covered. Head over to the [CSS recap pages](css-recap.md) and fill your boots!

Once you are feeling ready to show what you know, its time to complete and submit your [CSS challenges](challenges/css_challenge.md). 