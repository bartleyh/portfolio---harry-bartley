# Reset v Normalize

Browsers will give plain HTML a set of default styles, but these styling choices won't necessarily have any consistency across browsers. These default styles can also (frustratingly) interfere with our layout in unexpected ways as we write and introduce our own styles. It's good and common practice to clear out any default styles and start with a blank slate before you behin working on your CSS> 

_If you are interested in the default styles that are applied in your browser, you can view these links:_

* [Chrome](https://trac.webkit.org/browser/trunk/Source/WebCore/css/html.css)
* [Firefox](https://dxr.mozilla.org/mozilla-central/source/layout/style/res/html.css)

There are two approaches used to reconstruct our styles: *resetting the default styles* and *normalizing the default styles*.

Both of these techniques aim to accomplish the same thing – getting the default styles out of our way as we work – but they go about doing that in slightly different ways.

**Reset**  
A reset file is a set of CSS rules that resets the styling of all HTML elements to a consistent baseline. It is a ‘slash and burn’ approach that wipes out all the default styles and lets you start from scratch. If you want to truly start from scratch, a reset file will let you do that.

For more information on the thinking behind reset files, check out [this article](http://meyerweb.com/eric/thoughts/2007/04/18/reset-reasoning/).

A great base for a reset file is Eric Meyer’s [Reset CSS](https://meyerweb.com/eric/tools/css/reset/).

**Normalize**  
A normalize file is a less extreme way to make the browser defaults consistent. It provides cross-browser consistency without completely losing the default styles and working with modern CSS standards.

A great base normalize file is Nicolas Gallagher’s [Normalize.css](https://necolas.github.io/normalize.css/).

For more information on normalize.css and how it works, check out this [article](http://nicolasgallagher.com/about-normalize-css/).

### Additional resources

[A helpful tutorial on Normalize.css from Treehouse](https://teamtreehouse.com/library/applying-normalizecss-browser-reset-css) (it's only 3 mins long)

