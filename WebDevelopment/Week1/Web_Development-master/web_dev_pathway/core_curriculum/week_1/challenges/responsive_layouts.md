# The Responsive Layouts Challenge


By the end of this challenge, you will:

* Have an understanding of how to refactor an unresponsive layout
* Have the skills and understanding to implement media queries and add breakpoints effectively and as needed
* Feel confident that you understand how to control how your site looks on every screen size.


## Overview of The Challenge
Now that you have a handle on media queries and breakpoints, take a look at the cat.html and cat.css files over in [this repo](../responsive_walkthrough). Even though it's been built using the same responsive site we just made as a template, this site is **definitely not responsive!** 

Your challenge is to ensure our users have access to all this highly critical cat-related information! Consider how you can approach structuring this layout on medium and small screens, and, using media queries, see if you can make the page contents work nicely as you resize the screen.


# Getting Started
Make a copy of [this repo](../responsive_walkthrough), open up cat.html in your browser, and try resizing the screen. Make it small, make it big, play around with it. What happens to the layout? Take some time to think about how you might want the site the behave on various screen sizes, and implement some layout adjustments to achieve it.

