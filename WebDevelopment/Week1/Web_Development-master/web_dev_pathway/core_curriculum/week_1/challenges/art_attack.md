# Art Attack

![Art Attack Logo](./images/art-attack.gif)

Your challenge this week is to use your new-found CSS wizadry to create art(!)

For some inspiration, take a look at this image of [Grace Hopper](https://codepen.io/techxastrish/pen/eEyYNP) created using a single div, the [Mona Lisa in CSS](https://codepen.io/jaysalvat/pen/HaqBf) or this extraordinary image created using only [CSS and HTML](https://github.com/cyanharlow/purecss-francine).

It does not matter what you choose to create, but you **will** be asked to share your artistic creation with us all next week. 


# Getting started

You may want to start with [Coding Artist's video tutorial](https://coding-artist.teachable.com/). Or you can check out his [written tutorial](https://medium.com/coding-artist/a-beginners-guide-to-pure-css-images-ef9a5d069dd2) if you prefer. 

If you enjoy more of a "trial and error" approach to learning, why not start with his [CSS Koala](https://codepen.io/mikemang/pen/oYMePj) and refactor it to produce a new animal or design? 

![CSS Koala](./images/koala.png)

# Optional and Advanced

If you would like to stretch yourself even further, why not look into building a pure CSS animation. Take a look at some examples from [Codepen](https://webdesign.tutsplus.com/articles/pure-css-animation-inspiration-on-codepen--cms-30875) before getting started. 

#### If you liked this challenge...

If you enjoyed this challenge, why not continue by practicing a little every day. [Daily CSS Images](http://dailycssimages.com/) will send you a new challenge to your email every day for 50 days!


