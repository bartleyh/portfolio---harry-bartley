# CSS challenge

## Challenge One: The Dev Tools Challenge

Developer Tools, or Dev Tools, are available to use in every browser (the example below shows the Chrome Dev Tools but use whatever browser you are most comfortable working in). They help us debug, experiment, and test assumptions in our HTML, CSS, and Javascript quickly and at low-risk in the browser. They’re your friend when it comes to understanding how CSS works (or untangling why it isn’t working) – get in the habit of using them early and often! 

### Editing CSS 

To the right of the HTML pane, there’s a small sidebar that gives us styling information for the currently selected element. Similar to the HTML pane, we can add or remove styles and adjust CSS property values from this pane. You can click on any style property associated with the selected element and change its value. You can also use the blue checkbox to toggle the style on or off.

### Your Dev Tools Challenge
Directly from the css pane, we can edit the css and see the changes reflected immediately.

Let's work on the following edits for [thg.com](https://www.thg.com/)

* Change the background colour of the header to `chartreuse`
* Change the text to `magenta`
* Hide the elements that contain the homepage image. 

When all is said and done, you should have something that looks a bit like this:

![example with changes](./images/example.png)

Now go ahead and find some other sites to explore and manipulate. 

## Challenge Two: Quick-fire CSS Layout

### CSS Layout Challenge

As a developer working on the front end, you will often be handed designs and specifications from a designer that you then have to build. You will need to be able to take a graphic file and recreate it with code. This means that you'll have to be able to think through how you're going to get all the elements in that design where they need to be, and that requires a solid understanding of how to position HTML elements with CSS.

#### Getting Started

In this exercise, we'll take a series of simple comps and recreate each in [CodePen](http://codepen.io/). You will make the appropriate number of `<div>` tags with the div numbers indicated in the comps, give them a `<height>`, `<width>`, and `background-color` and then make them match the layout in the comp. Here's an example of what this might look like in your HTML and CSS:

Your HTML:

```html
<div class="div1">
  div1
</div>

<div class="div2">
  div2
</div>
```

Your CSS:

```css
div {
  width: 100px;
  height: 100px;
  background-color: black;
  color: white;
}
.div1 {
  float: right;
}

.div2 {
  float: left;
}
```

#### Tips:
* Remember, we've talked about several ways to position elements on the page. These include but are not necessarily limited to `display: block` and `display: inline-block`, `position: relative` and `position: absolute`, `margin`, `padding`, and the infamous `float`.
* If the layout dictates, some of the divs may be nested within other divs. Use your best judgement!
* See how many different ways you can achieve the same result and consider the pros/cons of solution!

##### Challenge 1:

![challenge one](./images/layout1.png)

##### Challenge 2:

![challenge two](./images/layout2.png)

##### Challenge 3:

![challenge three](./images/layout3.png)

##### Challenge 4: 

![challenge four](./images/layout4.png)






