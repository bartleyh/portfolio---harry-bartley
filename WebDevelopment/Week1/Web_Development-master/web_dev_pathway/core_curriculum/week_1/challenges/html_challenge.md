# HTML Challenge: 

To get started, sign yourself up for [codepen](https://codepen.io/), we will be using this to experiment and share our code for some of these early exercises. 

Once you are signed up, work your way through the challenges below. You are welcome to complete in pairs or as individuals. You will however need to share your work before moving on. 



# Challenge One: 

Create this table: 

![simple table](./images/flags.jpg)

You can make use of codepen to create your own html file or set it up as your own project locally, it's up to you. 

**Helpful Hint:** Start by researching the [table element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/table) in HTML. 


# Challenge Two


Copy the form code from below into your own pen: 

```html
 <form action="/my-handling-form-page" method="post">
    <div>
        <label for="name">Name:</label>
        <input type="text" id="name" name="user_name" />
    </div>
    <div>
        <label for="mail">E-mail:</label>
        <input id="mail" name="user_mail" />
    </div>
    <div>
        <label for="msg">Message:</label>
        <textarea id="msg" name="user_message"></textarea>
    </div>
    
    <div class="button">
        <button type="submit">Send your message</button>
    </div>
</form>
```

You should then perform a refactor that addresseses the following: 

* Validate for email type
* Add a set of radio buttons with at least three options - only allowing one to be selected at a time
* Include placeholders for name, email, and message
* Add a drop down for favorite color with at least three options
* Use an input for submit instead of a button
