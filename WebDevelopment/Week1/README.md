## Accelerator 2018-2019 Harry Bartley

### Basic CSS and HTML exercises

• CSS challenge and picture involves basic manipulation of raw css

• Netflix Challenge involves replicating main Netflix homepage using only html, css and js (no frameworks)

• Responsive Walkthrough is practise changing page size (or to mobile view) so the containers stack neatly
