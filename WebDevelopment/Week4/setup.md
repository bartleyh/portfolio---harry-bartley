# Setting up your JavaScript projects

#### Configuring Your Projects
Adding dependencies, bundling your JavaScript files with Webpack, adding style loaders, writing npm scripts, linting your code … This lesson will begin to draw back the veil on these mysteries!

#### Vocabulary

* `Dependency`: Something your program needs in order to do it’s job that is provided by an external source
* `NPM`: Node Package Manager is a repository of JavaScript libraries
* `Webpack`: Broadly, a thing that takes your many files and combines them into just a few files a web browser can understand
* `Linter`: A tool that checks your code for any obvious errors


# npm
The **node package manager** is a command line tool that gives you access to a gigantic repository of plugins, libraries and tools. If you have done our Fundamentals course, you will probably have encountered it when you installed the Jasmine testing framework to do our exercises.

* Take a couple minutes to [watch this video](https://docs.npmjs.com/about-npm/index.html) - a great introduction to npm.
* [This video teaches you how to install packages with npm](https://docs.npmjs.com/downloading-and-installing-packages-locally).
* [This tutorial](https://docs.npmjs.com/creating-a-package-json-file) covers the `package.json` file, which you can use to manage your project’s dependencies
* If you run into trouble at any point you can [check out the official docs page](https://docs.npmjs.com/) for more tutorials and documentation.

# Webpack
Webpack is simply a tool for bundling modules. There is a lot of talk across the net about how difficult and complex it is to set up and use, but at the moment our needs are few and the setup is simple enough. In fact, you can see an example of getting it up and running on the front page of [their website](https://webpack.js.org/).

Webpack is a very powerful tool, and with that power comes a decent amount of complexity - just look at the sample config file on [this page](https://webpack.js.org/configuration/) 😱. Don’t let it scare you off! The basic configuration is not difficult to master.

To get us started we are going to refer to the official documentation.

* Code along with the [first four steps of this tutorial](https://webpack.js.org/guides/getting-started/) (“Basic Setup” through “Using a Configuration”)

Let’s discuss what’s going on there. After installing webpack using npm we set up a simple project that required an external library (lodash - check it out here if it’s new to you) using a simple script tag. The site lists a few reasons why this is probably not ideal and then steps through using webpack to accomplish the same thing.

There are a couple of key concepts to understanding how webpack works - entry and output. In this example, we rearranged the files into a src and dist folder. Technically we could have called those folders anything, but those names are typical. src is our source directory. In other words, src is where we write all of the code that webpack is going to bundle up for us. When webpack runs, it goes through all of our files looking for any import statements and then compiles all of the code we need to run our site into a single file inside of the `dist` folder (short for distrubution). Our entry file, then is the main application file that links (either directly or indirectly) to all of the other modules in our project. In this example, it is `/src/index.js`. The output file is the compiled version - `dist/bundle.js.`

* Browse [this document](https://webpack.js.org/concepts/) for more details. We’ll talk plugins and loaders in another lesson.



