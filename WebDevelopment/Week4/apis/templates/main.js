let image = document.querySelector("img");

fetch(
  "https://api.giphy.com/v1/gifs/translate?api_key=veM4aoFpmdeE6ggeE2uc7QEFX0FnKMJm&s=cats",
  { mode: "cors" }
)
  .then(response => response.json())
  .then(response => (image.src = response.data.images.original.url));
