let img = document.querySelector('img')

const gifPromise = fetch('https://api.giphy.com/v1/gifs/translate?api_key=[INSERT_YOUR_API_KEY]=cats', { mode:'cors' })

gifPromise
    .then(response => response.json())
    .then(response => img.src = response.data.images.original.url)
    .catch((error) => {
        console.log(error)
    })