# Webpack 

Webpack is simply a tool for bundling modules. There is a lot of talk across the net about how difficult and complex it is to set up and use, but at the moment our needs are few and the setup is simple enough. In fact, you can see an example of getting it up and running on the front page of their website.

Webpack is a very powerful tool, and with that power comes a decent amount of complexity - just look at the sample config file on this page 😱. Don’t let it scare you off! The basic configuration is not difficult and proficiency with webpack looks amazing on resumes.

There are several other popular tools for achieving the same goals, including Gulp.js and Grunt.js. These tools are known as task runners. A task runner is exactly what it sounds like: a tool to run tasks such as concatenation and minification. webpack can do the same things, but it does so by loading assets such as plugins.

In the past, our curriculum used Gulp.js. However, webpack is quickly gaining marketshare and popularity and it's commonly used with both Angular and React. For that reason, we've switched over to webpack. If you're interested, you can explore Gulp.js on your own. You're not expected to know the fine points of the differences between module bundlers and task runners while you're at Epicodus, but you're encouraged to do some additional reading on your own.

### Introduction to Webpack
So how does webpack work and why is it so useful?

Webpack uses a dependency graph to recursively manage an application's assets. That sounds complicated, but the good news is that webpack will do most of the heavy lifting for us. Let's take a look at an example.

Imagine that we're building an application that makes very complex peanut butter and jelly sandwiches. As a result, we have multiple JavaScript files for managing the creation of these sandwiches: peanut-butter.js, jelly.js and bread.js. In addition to these files, which contain our business logic, we also have an entry point for our application called main.js where we include our interface logic. Think of an entry point as a door leading into our application. webpack needs this entry point in order to recursively gather all the other files the application needs. A bigger application may have multiple entry points but we'll only be working with one.

Here's what the first few lines of main.js might look like:
```javascript.js
main.js
import { PeanutButter } from './peanut-butter.js'
import { Jelly } from './jelly.js'
import { Bread } from './bread.js'
import '../css/styles.css'

...
```
You haven't learned about import statements just yet, but we'll cover them in depth soon. For now, just be aware that an import statement is exactly what it sounds like: a way to import a piece of code from one file into another.

When we tell webpack to load main.js, webpack will recursively load and concatenate all the code from main.js as well as any required code from other files such as peanut-butter.js. If jelly.js imports code from yet another file called blueberry.js, webpack would gather that code, too. This code would all be gathered into a single file with a name such as main.bundle.js.

And just like that, our code is bundled. With a task runner, we'd need to write a task concatenating our code to achieve the same thing.

Webpack will load not just JavaScript files but also other assets such as CSS files and images. In fact, as long as we have the right loaders and plugins (which we'll discuss shortly), we can import many types of assets.

All this information might be a little overwhelming at first. However, as long as you correctly set up your webpack configuration file and use import statements, webpack will take care of the rest for you.

Code along with the [first four steps of this tutorial](https://webpack.js.org/guides/getting-started/) (“Basic Setup” through “Using a Configuration”)

Let’s discuss what’s going on there. After installing webpack using npm we set up a simple project that required an external library (lodash - check it out here if it’s new to you) using a simple script tag. The site lists a few reasons why this is probably not ideal and then steps through using webpack to accomplish the same thing.

There are a couple of key concepts to understanding how webpack works - entry and output. In this example, we rearranged the files into a src and dist folder. Technically we could have called those folders anything, but those names are typical. src is our source directory. In other words, src is where we write all of the code that webpack is going to bundle up for us. When webpack runs, it goes through all of our files looking for any import statements and then compiles all of the code we need to run our site into a single file inside of the dist folder (short for distrubution). Our entry file, then is the main application file that links (either directly or indirectly) to all of the other modules in our project. In this example, it is /src/index.js. The output file is the compiled version - dist/bundle.js.

Browse [this document](https://webpack.js.org/concepts/) for more details. We’ll talk plugins and loaders in another lesson.
