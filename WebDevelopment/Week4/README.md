# Week Four

_The majority of the content covered this week will be new to you, take your time and feel free to search out your own resources for supplementary learning too._


* [JSON](json.md)
* [Async](async.md)
* [Working with APIs](./apis)
* [Async and Await](async_and_await.md)
* [Promises](promises.md)

Once you are happy with the fundamentals of async JavaScript, read, code along and understand the below: 
* [ES6 Modules](es6_modules.md)
* [Webpack](webpack1.md) (_much of this will have been covered in [ES6 Modules](es6_modules.md)_)
* [More Webpack](webpack2.md)


* [Weather Challenge](./challenge)


# Week Four Checklist

* [The Extension Exercises for the API tutorial](./apis)
* [Weather Challenge](./challenge)

