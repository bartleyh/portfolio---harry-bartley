# JSON

There isn't much for you to learn here, and you should already be familiar with the main JavaScript methods you will need when dealing with JSON. 

**JSON (JavaScript Object Notation)** is a standardized format for structuring data. It is heavily based on the syntax for JavaScript objects. You will often encounter JSON formatted data when working with external servers or APIs - it is essentially the universal format for transmitting data on the web.

Spend 10-15 minutes going through the following resources and you’ll be good to go: 

* This [MDN tutorial](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON) is probably all you need…
* Read about the JavaScript methods that you’ll most often be using when dealing with JSON - [JSON.parse()](https://www.w3schools.com/js/js_json_parse.asp) and [JSON.stringify()](https://www.w3schools.com/js/js_json_stringify.asp).
* Mis-formatted JSON is a common cause of errors. [This JSON formatter](https://jsonformatter.curiousconcept.com/) lets you paste in JSON code and will search it for formatting errors.