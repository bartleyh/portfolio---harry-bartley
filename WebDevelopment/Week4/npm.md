# NPM

The node package manager is a command line tool that gives you access to a gigantic repository of plugins, libraries and tools. If you have done our Fundamentals course, you will probably have encountered it when you installed the Jasmine testing framework to do our exercises.

* Take a couple minutes to watch [this video](https://docs.npmjs.com/about-npm/index.html) - a great introduction to npm.
* [This video](https://docs.npmjs.com/downloading-and-installing-packages-locally) teaches you how to install packages with npm.
* [This tutorial](https://docs.npmjs.com/creating-a-package-json-file) covers the package.json file, which you can use to manage your project’s dependencies
* If you run into trouble at any point you can check out the official docs page [for more tutorials](https://docs.npmjs.com/) and documentation.

## Yarn?
At some point, you will probably run into [Yarn](https://yarnpkg.com/en/) - a replacement for the default npm. For the most part, it does the same things though it does have a few more features. Recent versions of npm have incorporated some of the best features of Yarn, so using it won’t offer you any real advantages at this point in your career. It is a fine project, however, and may be worth your consideration in the future.
