let searchButton = document.getElementById("searchBtn");
let searchInput = document.getElementById("searchTxt");
let apiKey = "&APPID=53dea6af452831a979bea9bd68a53601";
let url = "http://api.openweathermap.org/data/2.5/weather?units=metric&q=";
let tempResults = document.getElementById("temp");
let tempMinMax = document.getElementById("min&max");
let pressure = document.getElementById("pressure");
let humidity = document.getElementById("humidity");
let title = document.getElementById("title");

searchButton.addEventListener("click", cityInputFunction);
searchInput.addEventListener("keyup", enterPressed);

function enterPressed(event) {
  console.log("here");
  if (event.key === "Enter") {
    cityInputFunction();
  }
}

async function cityInputFunction() {
  let cityInput = searchInput.value;
  let mainURL = url.concat(cityInput, apiKey);
  try {
    const response = await fetch(mainURL, { mode: "cors" });
    const weatherData = await response.json();
    title.innerHTML = "The Current Weather for " + cityInput;
    tempResults.innerHTML = "Current temperature: " + weatherData.main.temp;
    tempMinMax.innerHTML =
      "Minimum to maximum temperature: " +
      weatherData.main.temp_min +
      " -> " +
      weatherData.main.temp_max;
    pressure.innerHTML = "Pressure: " + weatherData.main.pressure;
    humidity.innerHTML = "Humidity: " + weatherData.main.humidity;
  } catch (error) {
    console.log(error);
  }
}
