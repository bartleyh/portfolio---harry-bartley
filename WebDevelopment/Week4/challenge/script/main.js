let searchButton = document.getElementById("searchBtn");
let searchInput = document.getElementById("searchTxt");
let apiKey = "&APPID=53dea6af452831a979bea9bd68a53601";
let url = "http://api.openweathermap.org/data/2.5/weather?units=metric&q=";
let tempResults = document.getElementById("temp");
let tempMinMax = document.getElementById("min&max");
let pressure = document.getElementById("pressure");
let humidity = document.getElementById("humidity");
let title = document.getElementById("title");
let searchBox = document.getElementById("search");

searchButton.addEventListener("click", cityInputFunction);
searchInput.addEventListener("keyup", enterPressed);

function enterPressed(event) {
  console.log("here");
  if (event.key === "Enter") {
    cityInputFunction();
  }
}

async function cityInputFunction() {
  let cityInput = searchInput.value;
  let mainURL = url.concat(cityInput, apiKey);
  try {
    const response = await fetch(mainURL, { mode: "cors" });
    const weatherData = await response.json();
    if (
      weatherData.weather[0].main == "Rain" ||
      weatherData.weather[0].main == "Drizzle"
    ) {
      searchBox.style.backgroundImage = "url('/style/rain.jpg')";
    } else if (weatherData.weather[0].main == "Clouds") {
      searchBox.style.backgroundImage = "url('/style/clouds.jpg')";
    } else if (weatherData.weather[0].main == "Clear") {
      searchBox.style.backgroundImage = "url('/style/sun.jpeg')";
    } else if (weatherData.weather[0].main == "Snow") {
      searchBox.style.backgroundImage = "url('/style/snow.jpg')";
    } else {
      searchBox.style.backgroundImage = "url('/style/cloud.jpg')";
    }
    searchBox.style.padding = "10px";
    title.innerHTML = "The Current Weather for " + cityInput;
    tempResults.innerHTML =
      "Current temperature: " + weatherData.main.temp + "°" + "C";
    tempMinMax.innerHTML =
      "Minimum to maximum temperature: " +
      weatherData.main.temp_min +
      "°" +
      "C" +
      " -> " +
      weatherData.main.temp_max +
      "°" +
      "C";
    pressure.innerHTML = "Pressure: " + weatherData.main.pressure + "mb";
    humidity.innerHTML = "Humidity: " + weatherData.main.humidity + "%";
    console.log(weatherData);
  } catch (error) {
    console.log(error);
  }
}
// WEATHER.0.MAIN

// Mist

// Snow
