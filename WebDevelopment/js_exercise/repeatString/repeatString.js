const repeatString = function(word, repeats) {
  string = "";
  if (repeats >= 0) {
    for (i = 0; i < repeats; i++) {
      string += word;
    }
    return string;
  } else {
    return "ERROR";
  }
};

module.exports = repeatString;
