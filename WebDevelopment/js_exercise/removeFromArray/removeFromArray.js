const removeFromArray = function(array, remove1, remove2, remove3, remove4) {
  let newarray = [];
  for (i = 0; i < array.length; i++) {
    if (
      remove1 == array[i] ||
      remove2 == array[i] ||
      remove3 == array[i] ||
      remove4 == array[i]
    ) {
      continue;
    } else newarray.push(array[i]);
  }
  return newarray;
};

module.exports = removeFromArray;
