class Medusa {
  constructor(name) {
    this.name = name;
    this.statues = [];
  }

  stare(victim) {
    this.statues.length < 3
      ? this.statues.push(victim)
      : (this.statues[0].stoned =
          false && this.statues.splice(this.statues[0]));
    victim.stoned = true;
  }
}
module.exports = Medusa;
