class Wizard {
  constructor(description) {
    this.name = description.name;
    this.isRested = true;
    this.bearded = true
      ? description.bearded == undefined
      : description.bearded == description.bearded;
    this.times = 0;
  }
  incantation(string) {
    return string.toUpperCase();
  }

  cast() {
    this.times++;
    this.times % 3 == 0 ? (this.isRested = false) : (this.isRested = true);
    return this.isRested ? "Magic Bullet!" : "I SHALL NOT CAST!";
  }
}

module.exports = Wizard;
