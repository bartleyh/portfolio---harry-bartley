USE company_lab;
INSERT INTO `employee` (`NI`, `bdate`, `fname`, `lname`, `gender`, `hire_date`, `salary`,`Dno`,`superNI`,`job`) VALUES
(1, '1985-10-11','rogers','sanders','M','2016-08-02', '20000.00', 2, 4, 'developer'),
(2, '1985-10-04', 'mike','miller','M', '2016-11-02',NULL , 4, 4, 'not assinged'),
(3, '1950-10-11','david','brown','M','1999-10-11', '40000.00',1, 7, 'team leader'),
(4, '1942-10-11','ali','morgan','M','1975-10-11','70000.00',2, 8, 'manager'),
(5, '1986-10-08','john','wright','M', '2016-08-02','20000.00',4, 6, 'developer'),
(6, '1980-10-11','alex','john','F','2010-10-11', '45000.00',3, 9, 'project manager'),
(7,'1981-05-12', 'james','andrews','M','2011-10-05','60000.00',3, 9, 'manager'),
(8, '1940-08-07','jenny','ross', 'F','1980-10-11', '80000.00',1, 8, 'executive'),
(9, '1950-12-01','paul','bell','M', '1990-10-11','50000.00',2, 7, 'data analyst');

INSERT INTO `department` (`dnum`, `dname`, `mgrNI`, `mgrstartdate`) VALUES
(1, 'research',8, '2000-10-11'),
(2, 'hr',7, '2017-10-05'),
(3, 'technology',9, '2001-10-11'),
(4, 'payroll',4, '2000-03-11');
