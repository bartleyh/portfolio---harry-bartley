# SQL - from code academy

## Intro to SQL

### What is a relational database?

>A relational database is a database that organises information into one or more tables

Table
>A table is a collection of data organised into rows and columns, tables are sometimes referred to as relations

All data stored in a relational database is of a certian type, some of the most common are:

• INTEGER, a postive or negative whole number
• TEXT, a text string
• DATE, the data formatte as YYYY-MM-DD for the year, month and day
• REAL, a decimal value

## Statements

Statement
>A statement is text that the database recognizes as a valid command, they always end in semi-colons,

1. CREATE TABLE, is a clause. Clauses perform specific tasks in SQL. By convention, clauses are written in capital letters. Clauses can also be referred to as commands
2. table_name, Refers to the name of the table that the command is applied to
3. (column_1 data_type, column_2 data_type, column_3 data_type) is a parameter. A parameter is a list of columns, data types, or values that are passed to a clause as an argument. Here, the parameter is a list of column names and the associated data type.

## Commands/Clauses

• INSERT INTO table_name (parameters1, parameters2)
  VALUES (1, "");

• SELECT * FROM table_name to viewa row just created

• UPDATE table_name
  SET parameter = ..
  WHERE id = ..

• ALTER TABLE table_name
  ADD COLUMN blah TEXT;

• DELETE FROM table_name
  WHERE parameter IS NULL;

• AS, renaming with an alies,
  SELECT parameter AS "blah"
  FROM table_name;

### Constraints

Constraints that add information about how a column can be used are invoked after specifying the data type for a column. They can be used to tell the database to reject inserted data that does not adhere to a certain restriction

• PRIMARY KEY columns can be used to uniquely identify the row. Attempts to insert a row with identical value to a row already in the table will result in a constraint violation
• UNIQUE columns have a different value for every row. This is similar to PRIMARY KEY except a table can have many different UNIQUE columns
• NOT NULL columns must have a value. Attempts to insert a row without a vlaue for a NOT NULL column will result in a constraint violation
• DEFAULT columns take an additional arguement that will be the assumed value for an inserted row if the new row does not specify a value for that column

```SQL
CREATE TABLE celebs (
       id INTEGER PRIMARY KEY,
       name TEXT UNIQUE,
       date_of_birth TEXT NOT NULL,
       date_of_death TEXT DEFAULT "Not Applicable",
);
```

### Distinct

DISTINCT is used to return unique values in the output, it filters out all duplicate values in the specified columns, for instance

```SQL
SELECT DISTINCT parameter
FROM table_name;
```

### Where

Restrict query results using the WHERE clause in order to obtain only the information we want 
1. WHERE filters the result set to only include rows where the following conditions are true

### LIKE

1.
Returns a parameter that is like another, for example if two film titles, "seven" and "se7en" are return from WHERE name LIKE "se_en"

2.
A second version of like can be used where % is a wildcard, for example "A%" matches all beggining with A or for any containing a word "%word%"

### Between

The BETWEEN operator can be used in a WHERE clause to filter the result set within a certain range, these values can be numbers, text or dates

```SQL
SELECT *
FROM table_name
WHERE name BETWEEN "A" AND "J" ;
```

#### Be aware that AND and OR exist as part of WHERE and its operators

### ORDER BY

Sorts the results, this can either be alphabetically or numerically

• DESC is the keyword used in ORDER BY for descending order
• ASC is for ascending


### LIMIT

When dealing with a table with hundreds and thousands of rows it is important to cap the number of rows appearing, if we set LIMIT 10, self explanatory 


### CASE

A CASE statement allows us to create different outputs (usually in the SELECT statement) 

```SQL
SELECT name,
 CASE
  WHEN imdb_rating > 8 THEN 'Fantastic'
  WHEN imdb_rating > 6 THEN 'Poorly Received'
  ELSE 'Avoid at All Costs'
 END
FROM movies;
```

## Aggregate Functions


### Count

The fastest way to calculate how many rows are in a table is to use the count function, COUNT() takes the name of the column as an arguement and counts the number of non-empty values in that column


#### SUM()
In a similar way SUM() is used to add together all the values in a particular column

#### MAX/MIN

• MAX() takes the name of a column as an argument and returns the largest value in that column. Here, we returned the largest value in the downloads column.

• MIN() works the same way but it does the exact opposite; it returns the smallest value.


#### Avg() and Round() 

•The AVG() function works by taking a column name as an argument and returns the average value for that column.
•The ROUND() function takes two arguements into its parenthesis: a column name and an integer, with this it can give an everage for example, to a select number of decimal places

### Group By

This comes in two parts;

• We can use GROUP BY() after a 




### Having

•In addition to being able to group data by GROUP BY, SQL also allowd you to filter which groups to include and which to exclude